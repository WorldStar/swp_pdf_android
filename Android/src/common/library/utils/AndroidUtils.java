package common.library.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;

public class AndroidUtils {
    public static String getVersionNumber(Context context)
    {
    	if( context == null )
    		return "";
    	
		try {
			PackageInfo pinfo;
			pinfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			String versionName = pinfo.versionName;
			return versionName;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";		
    }
}
