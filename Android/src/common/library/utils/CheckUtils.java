package common.library.utils;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Map;

public class CheckUtils {
	public static boolean isEmpty(Object obj)
    {
        if(obj == null)
            return true;
        if(obj.toString().trim().length() == 0)
            return true;
        if((obj instanceof Collection) && ((Collection)obj).size() == 0)
            return true;
        if((obj instanceof Map) && ((Map)obj).size() == 0)
            return true;
        try
        {
            if((obj instanceof Object[]) || obj.getClass().getName().startsWith("["))
            {
                int length = Array.getLength(obj);
                if(length == 0)
                    return true;
            }
        }
        catch(Exception e) { }
        return false;
    }
    
    public static boolean isNotEmpty( Object obj )
    {
    	return !isEmpty(obj);
    }
    
    public static boolean hasEmpty(Object objects[])
    {
        if(objects == null || objects.length == 0)
            return true;
        Object arr$[] = objects;
        int len$ = arr$.length;
        for(int i$ = 0; i$ < len$; i$++)
        {
            Object obj = arr$[i$];
            if(isEmpty(obj))
                return true;
        }

        return false;
    }    
}
