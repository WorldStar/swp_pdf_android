package common.pdf.utils;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


public class PDFMaker {
	public static PdfPCell makePDFTabelCell(String content, FontFamily textFont, float textSize )
	{
		return makePDFTabelCell(content, textFont, textSize, BaseColor.BLACK, BaseColor.WHITE);
	}
	
	public static PdfPCell makePDFTabelCell(String content, FontFamily textFont, float textSize, BaseColor textColor, BaseColor backColor )
	{
		Chunk chunk = new Chunk(content);
		Paragraph para = new Paragraph();
		Font font = new Font(textFont, textSize);
		font.setColor(textColor);
		chunk.setFont(font);
		para.add(chunk);
		PdfPCell cell = new PdfPCell(para);
		cell.setBackgroundColor(backColor);
		
		return cell;	
	}
	
	public static PdfPCell makePDFTabelCell(String content, FontFamily textFont, float textSize, BaseColor textColor, BaseColor backColor, float borderWidth, BaseColor borderColor )
	{
		Chunk chunk = new Chunk(content);
		Paragraph para = new Paragraph();
		para.setAlignment(Element.ALIGN_JUSTIFIED);
		Font font = new Font(textFont, textSize);
		font.setColor(textColor);
		chunk.setFont(font);
		para.add(chunk);
		PdfPCell cell = new PdfPCell(para);
		cell.setBackgroundColor(backColor);
		cell.setBorderColor(borderColor);
		cell.setBorderWidth(borderWidth);
		
		return cell;
	}
	
	public static PdfPCell makePDFTabelCell(String[] content, FontFamily[] textFont, float[] textSize )
	{
		BaseColor[] textColor = {BaseColor.BLACK, BaseColor.BLACK};
		return makePDFTabelCell(content, textFont, textSize, textColor, BaseColor.WHITE);
	}
	
	public static PdfPCell makePDFTabelCell(String[] content, FontFamily[] textFont, float[] textSize, BaseColor[] textColor, BaseColor backColor )
	{
		Paragraph para = new Paragraph(textSize[0] * 3);
		for(int i = 0; i < content.length; i++ )
		{
			String buff = content[i];
			if( i > 0 )
				buff = "\r\n" + content[i];
			Chunk chunk = new Chunk(buff);
			Font font = new Font(textFont[i], textSize[i]);
			font.setColor(textColor[i]);
			chunk.setFont(font);
			para.add(chunk);
		}

		PdfPCell cell = new PdfPCell(para);
		cell.setBackgroundColor(backColor);
		
		return cell;	
	}
	
	public static PdfPCell makePDFTabelCell(String[] content, FontFamily[] textFont, float[] textSize, BaseColor[] textColor, BaseColor backColor, float borderWidth, BaseColor borderColor )
	{	
		PdfPCell cell = makePDFTabelCell(content,  textFont, textSize, textColor, backColor );
		cell.setBorderColor(borderColor);
		cell.setBorderWidth(borderWidth);
		
		return cell;
	}
	
	public static PdfPCell makePDFTabelCell(String[] content, FontFamily textFont, float textSize )
	{
		return makePDFTabelCell( content, textFont, textSize, BaseColor.BLACK, BaseColor.WHITE);
	}
	
	public static PdfPCell makePDFTabelCell(String[] content, FontFamily textFont, float textSize, BaseColor textColor, BaseColor backColor )
	{
		List list = new List(false);
		list.setListSymbol("");
		
		for( int i = 0; i < content.length; i++ )
		{
			Chunk chunk = new Chunk(content[i]);
			Font font = new Font(textFont, textSize);
			font.setColor(textColor);
			chunk.setFont(font);
			
			ListItem item = new ListItem(chunk);
			item.setSpacingAfter(textSize / 2);
			list.add(item);
		}

		PdfPCell cell = new PdfPCell();
			cell.addElement(list);
		cell.setBackgroundColor(backColor);
		
		return cell;	
	}
	
	
	public static void layoutPDFTabelCell(PdfPCell cell, float padding, int verticalAligment, float height )
	{
		layoutPDFTabelCell(cell, padding, verticalAligment, Element.ALIGN_LEFT, height); 
	}
	
	public static void layoutPDFTabelCell(PdfPCell cell, float padding, int verticalAligment, int horizontalAligment, float height )
	{
		if( cell == null )
			return;
		
		cell.setVerticalAlignment(verticalAligment);
		cell.setHorizontalAlignment(horizontalAligment);
		cell.setPadding(padding);
		
		if( height > 0 )
			cell.setFixedHeight(height);
	}
	
	public static void layoutPaddingPDFTableCell(PdfPCell cell, float leftPadding, float topPadding, float rightPadding, float bottomPadding)
	{
		if( cell == null )
			return;
		
		if( leftPadding > 0 )
			cell.setPaddingLeft(leftPadding);
		if( rightPadding > 0 )
			cell.setPaddingRight(rightPadding);
		if( topPadding > 0 )
			cell.setPaddingTop(topPadding);
		if( bottomPadding > 0 )
			cell.setPaddingBottom(bottomPadding);
	}
	
	public static PdfPCell makeStringOnlyCell(String content, FontFamily textFont, float textSize, BaseColor textColor )
	{
		PdfPCell cell = makePDFTabelCell(content, textFont, textSize, textColor, BaseColor.WHITE);
		layoutPDFTabelCell(cell, 0, Element.ALIGN_BOTTOM, 0);
		layoutPaddingPDFTableCell(cell, 5.0f, 0, 0, 2.5f);

		cell.setBorder(Rectangle.NO_BORDER);
		
		return cell;
	}
	
	public static PdfPCell makeStringUnderlineCell(String content, FontFamily textFont, float textSize, BaseColor textColor, BaseColor underlineColor )
	{
		PdfPCell cell = makePDFTabelCell(content, textFont, textSize, textColor, BaseColor.WHITE);
		layoutPDFTabelCell(cell, 0, Element.ALIGN_BOTTOM, 0);
		layoutPaddingPDFTableCell(cell, 5.0f, 0, 0, 1.5f);
		
		cell.setBorderColorBottom(underlineColor);
		cell.setBorderWidthLeft(0);
		cell.setBorderWidthRight(0);
		cell.setBorderWidthTop(0);
		
		return cell;
	}
	
	public static void addParagraph(Document document, String content, float beforeSpace, float afterspace, FontFamily textFont, float textSize, BaseColor textColor )
	{
		if( document == null )
			return;
		
		Font font = new Font(textFont, textSize);
		font.setColor(textColor);

		Paragraph	label = new Paragraph(content, font);
		label.setSpacingBefore(beforeSpace);
		label.setSpacingAfter(0);
		
//		Chunk title = new Chunk(content, font);
//		label.add(title);
		
		try {
			document.add(label);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void addParagraphUnderline(Document document, String content, float beforeSpace, float afterspace, FontFamily textFont, float textSize, BaseColor textColor, int lineCount )
	{
		PdfPTable table = new PdfPTable(1); 
		table.setWidthPercentage(95.0f);
		table.setSpacingBefore(beforeSpace);
		table.setSpacingAfter(afterspace);
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		float height = 20;
		int start = 0, end = 0;
		int unitLength = 40;
//		String [] para = content.split("\n");
		String realData = "";		
		
		try {
			start = 0;
			for(int i = 0; i < lineCount; i++ )
			{
				end = start + unitLength;
				if( content.length() < end )
					end = content.length();
				
				if(start >= content.length())
					realData = "";
				else
					realData = content.substring(start, end);
				
				PdfPCell cell= makeStringUnderlineCell(realData, textFont, textSize, textColor, BaseColor.BLACK );
				cell.setFixedHeight(height);
				table.addCell(cell);				
				
				start += unitLength;
			}
			document.add(table);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}	
	}
	
	public static void drawLines(PdfWriter writer, float sx, float sy, float ex, float ey, float width, BaseColor color  )
	{
		if( writer == null )
			return;
		
        PdfContentByte cb = writer.getDirectContent(); 

        cb.setLineWidth(width);	 // Make a bit thicker than 1.0 default 
        cb.setGrayStroke(0.95f); // 1 = black, 0 = white 
        cb.moveTo(sx, sy); 
        cb.lineTo(ex, ey); 
        cb.setColorStroke(color);
        cb.stroke();
	}
	
	public static void addFillParagraph(Document document, String []content, float []width, boolean []flag, float textSize, float percent, float space, int aligment)
	{		
		if( content.length != width.length )
			return;
		if( content.length < 1 )
			return;
		
		PdfPTable table = new PdfPTable(content.length); 
		table.setWidthPercentage(percent);
		table.setSpacingBefore(space);
		table.setHorizontalAlignment(aligment);
		try {
			table.setWidths(width);
			
			for(int i = 0; i < content.length; i++ )
			{
				PdfPCell cell = null;
				if( flag[i] == false )
					cell = makeStringOnlyCell(content[i], FontFamily.UNDEFINED, textSize, BaseColor.BLACK);
				else
					cell = makeStringUnderlineCell(content[i], FontFamily.UNDEFINED, textSize, BaseColor.BLACK, BaseColor.BLACK);
				
				table.addCell(cell);
			}
			
			document.add(table);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}	

	}
}
