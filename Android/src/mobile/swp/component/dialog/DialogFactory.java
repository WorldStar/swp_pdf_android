package mobile.swp.component.dialog;

import mobile.swp.R;
import mobile.swp.choice.ChoiceManager;
import mobile.swp.dialogs.SelectChoiceDialog;
import mobile.swp.dialogs.SelectListDialog;
import mobile.swp.dialogs.SelectMenuDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.view.Gravity;
import common.design.layout.ScreenAdapter;
import common.list.adapter.ItemCallBack;
import common.list.adapter.ItemResult;


public class DialogFactory {
	public static final String	DIALOG_TITLE = "dialog_title";
	public static final String SEL_NUM	= "sel_num";
	public static final String ITEMS	= "items";

	public static final int		SELECT_LEVEL_DIALOG_ID	= 0;
	public static final int		SELECT_LIST_DIALOG_ID	= 1;
	public static final int		SELECT_MENU_DIALOG_ID	= 2;
	private volatile static DialogFactory instance = null;
	
	public CommonDialogInterface customDialog = null;
	

	private DialogFactory() {
	}
	
	public static final DialogFactory getInstance() {
		if (instance == null) {
			synchronized (DialogFactory.class) {
				if(instance == null)
					instance = new DialogFactory();
			}
			
		}
		return instance;
	}
	
	public CommonDialogInterface createDialog(Context context, int dialogID, JSONObject data, ItemCallBack callback)
	{
		JSONObject param = new JSONObject();
		int layoutID = 0;
		if( dialogID == SELECT_LEVEL_DIALOG_ID )
		{
			try {
				param.put(CommonDialog.DIALOG_DATA, data);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			layoutID = R.layout.dialog_select_choice;
			customDialog = new SelectChoiceDialog(context, layoutID, param, callback);		
		}
		
		if( dialogID == SELECT_LIST_DIALOG_ID )
		{
			try {
				param.put(CommonDialog.DIALOG_DATA, data);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			layoutID = R.layout.dialog_select_choice;
			customDialog = new SelectListDialog(context, layoutID, param, callback);		
		}
		
		if( dialogID == SELECT_MENU_DIALOG_ID )
		{
			try {
				param.put(CommonDialog.DIALOG_DATA, data);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			layoutID = R.layout.dialog_select_choice;
			customDialog = new SelectMenuDialog(context, layoutID, param, callback);		
		}
		
		customDialog.showDialog();
//		customDialog.setCanceledOnTouchOutside(true);

		
		return customDialog;
	}
	public void hideDialog()
	{
//		if( customDialog != null && customDialog.getDialog() != null )
//			customDialog.getDialog().dismiss();
//		
//		customDialog = null;

	}
	
	public void showMenu(Context context, ItemCallBack callback)
	{
		JSONObject choice = ChoiceManager.getInstance().getChoiceInfo();
		JSONArray list = choice.optJSONArray("menu_list");

		int height = list.length() * 100 + 55;
		JSONObject data = new JSONObject();
		try {
			data.put(CommonDialog.DIALOG_WIDTH, ScreenAdapter.computeWidth(600));
			data.put(CommonDialog.DIALOG_HEIGHT, ScreenAdapter.computeHeight(height));
			data.put(CommonDialog.DIALOG_POS_X, 0);
			data.put(CommonDialog.DIALOG_POS_Y, ScreenAdapter.computeHeight(164));
			data.put(CommonDialog.DIALOG_GRAVITY, Gravity.LEFT|Gravity.TOP);
			data.put(DialogFactory.DIALOG_TITLE, "Select Country");
			data.put(DialogFactory.SEL_NUM, -1);
			data.put(DialogFactory.ITEMS, list);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		createDialog(context, DialogFactory.SELECT_MENU_DIALOG_ID, data, callback);
	}
}
