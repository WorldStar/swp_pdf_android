package mobile.swp.component.dialog;

public interface CommonDialogInterface {
	public void showDialog();
	public void hideDialog();
	public void setCanceledOnTouchOutside(boolean cancel );
}
