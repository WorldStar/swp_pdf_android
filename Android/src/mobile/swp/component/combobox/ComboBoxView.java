package mobile.swp.component.combobox;

import mobile.swp.R;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import common.design.layout.LayoutUtils;

public class ComboBoxView  extends LinearLayout {
	TextView m_txtContent;
	ImageView m_imgArrow;
	
	public ComboBoxView(Context context) {
		this(context, null);
	}

	public ComboBoxView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		initSubComponents();
	}
	
	private void initSubComponents()
	{
		setOrientation(LinearLayout.HORIZONTAL);
		setGravity(Gravity.CENTER_VERTICAL);
				
		m_txtContent = new TextView(getContext());
		
		LayoutParams paramText = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);		
		paramText.gravity = Gravity.CENTER_VERTICAL;		
		m_txtContent.setHint("Please select Alige... ." );
		m_txtContent.setHintTextColor(Color.rgb(207, 199, 199));
		m_txtContent.setTextColor(Color.WHITE);
		addView(m_txtContent, paramText);
				
		m_imgArrow = new ImageView(getContext());
		LayoutParams paramArrow = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);		
		m_imgArrow.setBackgroundResource(R.drawable.combox_arrow);
		addView(m_imgArrow, paramArrow);		
	}
	
	public String getText()
	{
		return m_txtContent.getText().toString();
	}
	public void setText(String text)
	{
		m_txtContent.setText(text);
	}
	
	public void setHint()
	{
		m_txtContent.setHint(R.string.environmental_fill_hint);
	}
	
	public void setHint(String hint)
	{
		m_txtContent.setHint(hint);
	}
	
	public void setHintTextColor(int color)
	{
		m_txtContent.setHintTextColor(color);
	}
	
	public void setTextSize(float size)
	{
		m_txtContent.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
	}
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);
		
		int totalWidth = r - l;
		int totalHeight = b - t;
		
		int height = totalHeight / 2;
		int width = height * 68 / 54;
		LayoutUtils.setSize(m_imgArrow, width, height, false);
		LayoutUtils.setMargin(m_imgArrow, 0, 0, totalHeight / 3, 0, false);
		
		width = totalWidth - width - totalHeight;	
		
		LayoutUtils.setSize(m_txtContent, width, LayoutParams.WRAP_CONTENT, false);
		LayoutUtils.setMargin(m_txtContent, totalHeight / 3, 0, totalHeight / 3, 0, false);
		
	}
	
}
