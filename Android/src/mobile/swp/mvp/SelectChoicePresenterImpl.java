package mobile.swp.mvp;

import mobile.swp.choice.ChoiceManager;
import mobile.swp.component.dialog.DialogFactory;
import mobile.swp.constant.Const;
import android.content.Context;

import common.library.utils.MessageUtils;
import common.list.adapter.ItemCallBack;
import common.list.adapter.ItemResult;
import common.manager.activity.ActivityManager;


public class SelectChoicePresenterImpl implements SelectChoicePresenter, NavigateBarPresenter, BasePresenter {
	SelectChoiceView		view;
	int						m_nSelectedNum = -1;
	
	public SelectChoicePresenterImpl(SelectChoiceView view)
	{
		this.view = view;
	}

	@Override
	public void selectChoice(int num) {
		view.showSelectedButton(num);
		
		m_nSelectedNum = num;			
	}

	@Override
	public boolean gotoMenuPage() {		
		DialogFactory.getInstance().showMenu((Context)view, new ItemCallBack() {
			
			@Override
			public void doClick(ItemResult result) {
				view.gotoHomePage(result.mItemNumber);				
			}
		});
		return true;
	}
	
	@Override
	public boolean gotoPrevPage() {		
		ChoiceManager.getInstance().setChoiceLevel(m_nSelectedNum);
		
		if( ChoiceManager.getInstance().prevStage() == false)
		{
			ActivityManager.getInstance().popAllActivity();
		}
		else
			view.changeSelfPage();
		
		return true;
	}

	@Override
	public boolean gotoNextPage() {
		if( m_nSelectedNum < 0 )
		{
			MessageUtils.showMessageDialog((Context)view, Const.ERROR_SELECT_CONTENT);
			return false;
		}
		ChoiceManager.getInstance().setChoiceLevel(m_nSelectedNum);
		
		if( ChoiceManager.getInstance().nextStage() == false)
			view.gotoHomePage(0);
		else
			view.changeSelfPage();
		
		return true;
	}

	@Override
	public void initData() {
		String title = ChoiceManager.getInstance().getChoiceTitle();
		String description = ChoiceManager.getInstance().getChoiceDescription();
		int level = ChoiceManager.getInstance().getChoiceLevel();
		
		if( level < 0 || level > 4 )
			level = 2;	// Default Value
				
		m_nSelectedNum = level;
		
		boolean flag = ChoiceManager.getInstance().isFirstStage();
		view.showChoiceInfo(title, description, level, flag);
	}
}
