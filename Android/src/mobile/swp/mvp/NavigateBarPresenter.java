package mobile.swp.mvp;

public interface NavigateBarPresenter {
	public boolean gotoPrevPage();
	public boolean gotoNextPage();
	public boolean gotoMenuPage();
}
