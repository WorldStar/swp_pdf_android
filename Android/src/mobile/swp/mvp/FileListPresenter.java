package mobile.swp.mvp;

import org.json.JSONObject;


public interface FileListPresenter {
	public static final String FILE_NAME = "file_name";
	public static final String FILE_DATE = "file_date";
	
	public void openPDFFile(JSONObject info);
}
	