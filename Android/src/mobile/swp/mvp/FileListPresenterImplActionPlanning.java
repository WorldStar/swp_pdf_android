package mobile.swp.mvp;

import java.util.List;

import org.json.JSONObject;



public class FileListPresenterImplActionPlanning extends FileListPresenterImpl implements FileListPresenter, NavigateBarPresenter, BasePresenter {
	
	
	public FileListPresenterImplActionPlanning(FileListView view, int page)
	{
		super(view, page);
	}

	@Override
	protected List<JSONObject> getFileList()
	{
		return getFileList(m_nPage);
	}
	
	
	@Override
	protected void showPageTitle()
	{
		String title = "Action Planning";
		
		view.showPageTitle(title);
		
	}
	
	@Override
	public boolean gotoNextPage() {		
		view.gotoActionPlanningPage();
		return true;
	}
}
