package mobile.swp.mvp;

import java.util.ArrayList;

import mobile.swp.AppContext;
import mobile.swp.choice.ChoiceManager;
import mobile.swp.choice.EnvironmentalScanManager;
import mobile.swp.component.dialog.DialogFactory;
import mobile.swp.constant.Const;
import android.app.Activity;
import android.content.Context;
import common.library.utils.MessageUtils;
import common.list.adapter.ItemCallBack;
import common.list.adapter.ItemResult;


public class EnvironmentalScanPresenterImpl implements EnvironmentalScanPresenter, NavigateBarPresenter, BasePresenter {
	EnvironmentalScanView			view;
	EnvironmentalScanManager		m_Manager;
	public EnvironmentalScanPresenterImpl(EnvironmentalScanView view)
	{
		this.view = view;
		m_Manager = ChoiceManager.getInstance().getEnvironmentalScanManager();
	}

	private boolean saveEnvironmentalScan()
	{
		ArrayList<String> data = view.getEnvironmentalScanData();
		if( data.size() < 1 )
			return false;
		
		m_Manager.saveEnvironmentalScan(data);
		return true;
	}
	
	@Override
	public boolean gotoPrevPage() {			
		saveEnvironmentalScan();
		
		if( m_Manager.prevStage() == false)
		{
			view.gotoFileListPage();
		}
		else
			view.changeSelfPage();
		
		return true;
	}

	@Override
	public boolean gotoNextPage() {		
		if( saveEnvironmentalScan() == false )
		{
			MessageUtils.showMessageDialog((Context)view, Const.ERROR_FILL_CONTENT);
			return false;
		}
		
		if( m_Manager.nextStage() == false)
		{			
			view.gotoEstimatingRiskPage();
		}
		else
		{			
			view.changeSelfPage();
		}
		
		return true;
	}
	
	@Override
	public boolean gotoMenuPage() {
		DialogFactory.getInstance().showMenu((Context)view, new ItemCallBack() {
			
			@Override
			public void doClick(ItemResult result) {
				ChoiceManager.getInstance().getEnvironmentalScanManager().setFirstStage();
				AppContext.gotoMenuPage((Activity)view, result.mItemNumber);			
			}
		});
		return true;
	}

	@Override
	public void initData() {
		String title = m_Manager.getTitle();
		String description = m_Manager.getDescription();
		ArrayList<String> data = m_Manager.getEnvironmentalScan();
		
		view.showFillContent(title, description, data);

	}	

}
