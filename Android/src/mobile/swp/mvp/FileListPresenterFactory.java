package mobile.swp.mvp;

import mobile.swp.constant.Const;


public class FileListPresenterFactory {
	private volatile static FileListPresenterFactory instance;
	
	public static FileListPresenterFactory getInstance() {
		if (instance == null) {
			synchronized (FileListPresenterFactory.class) {
				if(instance == null)
					instance = new FileListPresenterFactory();
			}
			
		}
		
		return instance;
	}	
	
	public FileListPresenter	createFileListPresenter(FileListView view, int page)
	{
		FileListPresenter presenter = null;
		
		switch(page)
		{
		case Const.BUSINESS_STRATEGY:
		case Const.SEGMANET_ROLE:
			presenter = new FileListPresenterImplBusinessStrategry(view, page);
			break;
		case Const.ENVIRONMENTAL_SCAN:
			presenter = new FileListPresenterImplEnvironmentalScan(view, page);
			break;
		case Const.CURRENT_STATE:
			presenter = new FileListPresenterImplCurrentState(view, page);
			break;
		case Const.FUTURING:
			presenter = new FileListPresenterImplFuturing(view, page);
			break;
		case Const.GAP_ANALYSIS:
			presenter = new FileListPresenterImplGapAnalysis(view, page);
			break;
		case Const.ACTION_PLANNING:
			presenter = new FileListPresenterImplActionPlanning(view, page);
			break;
		case Const.MONITOR_REPORT:
			presenter = new FileListPresenterImplMonitorReport(view, page);
			break;
		}
		
		return presenter;
	}
}
