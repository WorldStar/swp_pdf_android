package mobile.swp.mvp;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import mobile.swp.AppContext;
import mobile.swp.choice.ChoiceManager;
import mobile.swp.component.dialog.DialogFactory;
import mobile.swp.pdf.creator.SWPDocumentCreator;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import common.library.utils.BackgroundTaskUtils;
import common.library.utils.BackgroundTaskUtils.OnTaskProgress;
import common.library.utils.MyTime;
import common.list.adapter.ItemCallBack;
import common.list.adapter.ItemResult;



public class FileListPresenterImpl implements FileListPresenter, NavigateBarPresenter, BasePresenter {
	FileListView			view;
	int						m_nSelectedNum = -1;
	int				m_nPage = 0;

	public FileListPresenterImpl(FileListView view, int page)
	{
		this.view = view;
		m_nPage = page;
		SWPDocumentCreator.getInstance().setCreatePage(page);
	}	

	@Override
	public boolean gotoPrevPage() {				
		return true;
	}

	@Override
	public boolean gotoNextPage() {		
		return true;
	}
	
	@Override
	public boolean gotoMenuPage() {
		DialogFactory.getInstance().showMenu((Context)view, new ItemCallBack() {
			
			@Override
			public void doClick(ItemResult result) {
				ChoiceManager.getInstance().getBusinessResponseManager().setFirstStage();
				AppContext.gotoMenuPage((Activity)view, result.mItemNumber);			
			}
		});
		return true;
	}

	@Override
	public void initData() {
		showPageTitle();		
		((BaseView)view).showProgress("", "Please wait...");

		new BackgroundTaskUtils(new OnTaskProgress() {
			
			@Override
			public void onProgress() {					
			}
			
			@Override
			public void onFinished() {
				loadFileList();			
				((BaseView)view).hideProgress();	
			}
		}).execute();			
	}
	
	protected void loadFileList()
	{
		List<JSONObject> filelist = getFileList();
		view.showFileListData(filelist);	
	}
	protected void showPageTitle()
	{
		
	}
	
	protected List<JSONObject> getFileList()
	{
		List<JSONObject> filelist = new ArrayList<JSONObject>();
		
		return filelist;
	}
	
	protected List<JSONObject> getFileList(int page)
	{
		List<JSONObject> filelist = new ArrayList<JSONObject>();
		
		String savePath = SWPDocumentCreator.getInstance().getPDFSavePath(page);
		
		File file = new File(savePath);
		String[] list = file.list();
	
		for(int i = 0; i < list.length; i++ )
		{
			JSONObject fileinfo = new JSONObject();
			File pdf = new File(savePath, list[i]);
			String date = MyTime.getCurrentDateForEnglish(pdf.lastModified());
			try {
				fileinfo.put(FILE_NAME, list[i]);
				fileinfo.put(FILE_DATE, date);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			filelist.add(fileinfo);
		}
		
		return filelist;
	}

	@Override
	public void openPDFFile(JSONObject info) {
		String name = info.optString(FILE_NAME, "");
		String path = SWPDocumentCreator.getInstance().getPDFSavePath(m_nPage);
		
		view.gotoPDFViewPage(path + "/" + name); 
	}


}
