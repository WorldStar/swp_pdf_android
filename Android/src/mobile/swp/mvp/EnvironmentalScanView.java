package mobile.swp.mvp;

import java.util.ArrayList;



public interface EnvironmentalScanView {
	public void showFillContent(String title, String subtitle, ArrayList<String> data);
	
	public void gotoEstimatingRiskPage();
	public void gotoFileListPage();	
	public void changeSelfPage();
	
	public ArrayList<String> getEnvironmentalScanData();	
}
