package mobile.swp.mvp;

import java.util.List;

import org.json.JSONObject;



public class FileListPresenterImplMonitorReport extends FileListPresenterImpl implements FileListPresenter, NavigateBarPresenter, BasePresenter {
	
	
	public FileListPresenterImplMonitorReport(FileListView view, int page)
	{
		super(view, page);
	}

	@Override
	protected List<JSONObject> getFileList()
	{
		return getFileList(m_nPage);
	}
	
	
	@Override
	protected void showPageTitle()
	{
		String title = "Monitor and Report";
		
		view.showPageTitle(title);
		
	}
	
	@Override
	public boolean gotoNextPage() {		
		view.gotoMonitorReportPage();
		return true;
	}
}
