package mobile.swp.mvp;

import java.util.List;

import org.json.JSONObject;



public class FileListPresenterImplFuturing extends FileListPresenterImpl implements FileListPresenter, NavigateBarPresenter, BasePresenter {
	
	
	public FileListPresenterImplFuturing(FileListView view, int page)
	{
		super(view, page);
	}

	@Override
	protected List<JSONObject> getFileList()
	{
		return getFileList(m_nPage);
	}
	
	
	@Override
	protected void showPageTitle()
	{
		String title = "Future State";
		
		view.showPageTitle(title);
		
	}
	
	@Override
	public boolean gotoNextPage() {		
		view.gotoFuturingPage();
		return true;
	}
}
