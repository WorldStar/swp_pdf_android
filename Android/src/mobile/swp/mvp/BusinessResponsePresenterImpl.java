package mobile.swp.mvp;

import mobile.swp.AppContext;
import mobile.swp.R;
import mobile.swp.choice.BusinessResponseManager;
import mobile.swp.choice.ChoiceManager;
import mobile.swp.component.dialog.DialogFactory;
import mobile.swp.constant.Const;
import mobile.swp.pdf.creator.SWPDocumentCreator;
import android.app.Activity;
import android.content.Context;

import common.library.utils.BackgroundTaskUtils;
import common.library.utils.BackgroundTaskUtils.OnTaskProgress;
import common.library.utils.MessageUtils;
import common.library.utils.OnAlertClickListener;
import common.list.adapter.ItemCallBack;
import common.list.adapter.ItemResult;


public class BusinessResponsePresenterImpl implements BusinessResponsePresenter, NavigateBarPresenter, BasePresenter {
	BusinessResponseView			view;
	int						m_nSelectedNum = -1;
	BusinessResponseManager m_Manager;

	
	public BusinessResponsePresenterImpl(BusinessResponseView view)
	{
		this.view = view;
		
		m_Manager = ChoiceManager.getInstance().getBusinessResponseManager();
	}
	
	private boolean saveBusinessResponse()
	{
		String response = view.getBusinessResponse();
//		if( CheckUtils.isEmpty(response) )
//			return false;
		
		m_Manager.saveBusinessResponse(response);
		
		return true;
	}
	
	@Override
	public boolean gotoPrevPage() {		
		saveBusinessResponse();
		
		if( m_Manager.prevStage() == false)
		{
			view.gotoFileListPage();
		}
		else
			view.changeSelfPage();
		
		return true;
	}

	@Override
	public boolean gotoNextPage() {		
		if( saveBusinessResponse() == false )
		{
			MessageUtils.showMessageDialog((Context)view, Const.ERROR_FILL_CONTENT);
			return false;
		}
		
		if( m_Manager.nextStage() == false)
		{
			MessageUtils.showEditDialog((Context)view, new OnAlertClickListener() {
				
				@Override
				public void onInputText(final String text) {
					
					((BaseView)view).showProgress("", "Please wait...");					
					new BackgroundTaskUtils(new OnTaskProgress() {
						boolean m_bSucess = false;
						@Override
						public void onProgress() {
							m_bSucess = SWPDocumentCreator.getInstance().makeBusinessStrategyDoc(text, m_Manager.getPageNum());							
						}
						
						@Override
						public void onFinished() {
							((BaseView)view).hideProgress();
							if( m_bSucess == true )
							{
								m_Manager.setFirstStage();
								view.gotoFileListPage();								
							}
							else
							{
								MessageUtils.showMessageDialog((Context)view, ((Context)view).getString(R.string.exit_file_error));
							}
						}
					}).execute();			
				}
			});
			
		}
		else
		{			
			view.changeSelfPage();
		}
		
		return true;
	}
	
	@Override
	public boolean gotoMenuPage() {
		DialogFactory.getInstance().showMenu((Context)view, new ItemCallBack() {
			
			@Override
			public void doClick(ItemResult result) {
				ChoiceManager.getInstance().getBusinessResponseManager().setFirstStage();
				AppContext.gotoMenuPage((Activity)view, result.mItemNumber);			
			}
		});
		return true;
	}

	@Override
	public void initData() {
		String title = m_Manager.getTitle();
		String description = m_Manager.getDescription();
		String response = m_Manager.getBusinessResponse();
		
		view.showBusinessResponseInfo(title, description, response);
		
		if( m_Manager.isLastStage() == true)
			((NavigateBarView)view).setNavigateButtonBackground(R.drawable.save_arrow, NavigateBarView.RIGHT_SIDE);
	}
	
	
}
