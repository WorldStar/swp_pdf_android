package mobile.swp.mvp;

import org.json.JSONObject;



public interface CurrentStateView {
	public void displayUILabel(JSONObject data);
	public JSONObject getCurrentStateData();
	public void 	showSelectedListInfo(String tag, String content);
	public void gotoPage(int pageNum);
	public void gotoFileListPage();	
}
