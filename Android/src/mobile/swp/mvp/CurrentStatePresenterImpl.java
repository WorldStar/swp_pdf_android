package mobile.swp.mvp;

import mobile.swp.AppContext;
import mobile.swp.R;
import mobile.swp.choice.ChoiceManager;
import mobile.swp.choice.CurrentStateManager;
import mobile.swp.component.dialog.CommonDialog;
import mobile.swp.component.dialog.DialogFactory;
import mobile.swp.pdf.creator.SWPDocumentCreator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import common.design.layout.ScreenAdapter;
import common.library.utils.BackgroundTaskUtils;
import common.library.utils.BackgroundTaskUtils.OnTaskProgress;
import common.library.utils.DataUtils;
import common.library.utils.MessageUtils;
import common.library.utils.OnAlertClickListener;
import common.list.adapter.ItemCallBack;
import common.list.adapter.ItemResult;


public class CurrentStatePresenterImpl implements CurrentStatePresenter, NavigateBarPresenter, BasePresenter {
	CurrentStateView			view;
	CurrentStateManager			m_Manager = null;
	int							m_nPage = 1;
		
	public CurrentStatePresenterImpl(CurrentStateView view, int page)
	{
		this.view = view;		
		m_nPage = page;
		m_Manager = ChoiceManager.getInstance().getCurrentStateManager();
	}
	
	
	@Override
	public boolean gotoPrevPage() {		
		
		return true;
	}

	@Override
	public boolean gotoNextPage() {		
		JSONObject data = view.getCurrentStateData();	
		DataUtils.savePreference(CURRENT_STATE_KEY + m_nPage, data.toString());
		
		if( 11 <= m_nPage && m_nPage <= 14 )	// Last Page
		{
			final int page = m_nPage;
			MessageUtils.showEditDialog((Context)view, new OnAlertClickListener() {
				
				@Override
				public void onInputText(final String text) {
					((BaseView)view).showProgress("", "Please wait...");
					new BackgroundTaskUtils(new OnTaskProgress() {
						boolean m_bSucess = false;
						@Override
						public void onProgress() {
							switch(page)
							{
							case 11:
								m_bSucess = SWPDocumentCreator.getInstance().makeCurrentStateFuturingDoc(text);
								break;
							case 12:
								m_bSucess = SWPDocumentCreator.getInstance().makeGapAnalysisDoc(text);						
								break;
							case 13:
								m_bSucess = SWPDocumentCreator.getInstance().makeActionPlnningDoc(text);						
								break;
							case 14:
								m_bSucess = SWPDocumentCreator.getInstance().makeMonitorReportDoc(text);						
								break;
							}							
						}
						
						@Override
						public void onFinished() {
							((BaseView)view).hideProgress();
							if( m_bSucess == true )
							{
								view.gotoFileListPage();								
							}
							else
							{
								MessageUtils.showMessageDialog((Context)view, ((Context)view).getString(R.string.exit_file_error));
							}
						}
					}).execute();	
				}
			});			
		}
		else
			view.gotoPage(m_nPage + 1);
		
		return true;
	}
	
	@Override
	public boolean gotoMenuPage() {
		DialogFactory.getInstance().showMenu((Context)view, new ItemCallBack() {
			
			@Override
			public void doClick(ItemResult result) {
				AppContext.gotoMenuPage((Activity)view, result.mItemNumber);			
			}
		});
		return true;
	}

	@Override
	public void initData() {
		JSONObject data = m_Manager.getData();		
		JSONObject page = data.optJSONObject("page" + m_nPage);
		view.displayUILabel(page);
		
		if( 11 <= m_nPage && m_nPage <= 14 )
			((NavigateBarView)view).setNavigateButtonBackground(R.drawable.save_arrow, NavigateBarView.RIGHT_SIDE);
	}


	
	@Override
	public void showListDialog(String tag, String text) {		
		JSONObject data = m_Manager.getData();		
		JSONObject page = data.optJSONObject("page" + m_nPage);		
		String title = page.optString(tag + "_title", "");
		int type = page.optInt(tag + "_type", 0);
		
		JSONArray list = new JSONArray();
		int selnum = 0;
		
		if( type == 0 )
		{
			list = page.optJSONArray(tag);
			
			
			for(int i = 0; i < list.length(); i++)
			{
				if( text.equals(list.optString(i, "")) )
				{
					selnum = i;
					break;
				}
			}
		}
		
		if(type == 1 )
		{
			JSONArray array = page.optJSONArray(tag);
			int start = array.optInt(0, 0);
			int end = array.optInt(1, 5);
		
			for(int i = start; i <= end; i++ )
			{
				String value = "" + i;
				list.put(value);
				if( text.equals(value) )
				{
					selnum = i - start;
				}
			}
		}
		
		int height = 750 * (list.length() + 1) / 4; 
		if( height > 1000 )
			height = 1000;
	
		try {
			data.put(CommonDialog.DIALOG_WIDTH, ScreenAdapter.computeWidth(924));
			data.put(CommonDialog.DIALOG_HEIGHT, ScreenAdapter.computeHeight(height));
			data.put(DialogFactory.DIALOG_TITLE, title);
			data.put(DialogFactory.SEL_NUM, selnum);
			data.put(DialogFactory.ITEMS, list);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		final String key = tag;
		DialogFactory.getInstance().createDialog((Context)view, DialogFactory.SELECT_LIST_DIALOG_ID, data, new ItemCallBack() {
			
			@Override
			public void doClick(ItemResult result) {
				selectedListInfo(key, result);
			}
		});
	}
	
	private void selectedListInfo(String tag, ItemResult result)
	{
		String content = result.itemData.optString("item_name", "");
		DataUtils.savePreference(CURRENT_STATE_KEY + m_nPage + tag, result.mItemNumber);
		view.showSelectedListInfo(tag, content);	
		content = "";
	}


	
}
