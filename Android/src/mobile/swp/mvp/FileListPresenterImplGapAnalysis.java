package mobile.swp.mvp;

import java.util.List;

import org.json.JSONObject;



public class FileListPresenterImplGapAnalysis extends FileListPresenterImpl implements FileListPresenter, NavigateBarPresenter, BasePresenter {
	
	
	public FileListPresenterImplGapAnalysis(FileListView view, int page)
	{
		super(view, page);
	}

	@Override
	protected List<JSONObject> getFileList()
	{
		return getFileList(m_nPage);
	}
	
	
	@Override
	protected void showPageTitle()
	{
		String title = "Gap Analysis";
		
		view.showPageTitle(title);
		
	}
	
	@Override
	public boolean gotoNextPage() {		
		view.gotoGapAnalysisPage();
		return true;
	}
}
