package mobile.swp.mvp;

import java.util.List;

import mobile.swp.choice.ChoiceManager;
import mobile.swp.choice.EnvironmentalScanManager;

import org.json.JSONObject;



public class FileListPresenterImplEnvironmentalScan extends FileListPresenterImpl implements FileListPresenter, NavigateBarPresenter, BasePresenter {
	
	EnvironmentalScanManager m_Manager;
	
	public FileListPresenterImplEnvironmentalScan(FileListView view, int page)
	{
		super(view, page);
		m_Manager = ChoiceManager.getInstance().getEnvironmentalScanManager();
	}

	@Override
	protected List<JSONObject> getFileList()
	{
		return getFileList(m_nPage);
	}
	
	@Override
	protected void loadFileList()
	{
		if( m_Manager.isFirstStage() )
		{
			super.loadFileList();
		}
	}
	
	@Override
	protected void showPageTitle()
	{
		String title = "Environmental Scan";
		
		view.showPageTitle(title);
		
	}
	
	@Override
	public boolean gotoNextPage() {		
		view.gotoEnvironmentalScanPage();
		return true;
	}
}
