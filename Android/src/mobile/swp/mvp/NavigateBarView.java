package mobile.swp.mvp;



public interface NavigateBarView {
	static final	int	LEFT_SIDE = 0;
	static final	int	RIGHT_SIDE = 1;

	public void setNavigateButtonBackground(int resourceID, int side);
}
