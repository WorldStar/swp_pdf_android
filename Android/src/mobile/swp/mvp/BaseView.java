package mobile.swp.mvp;



public interface BaseView {
	public void initProgress();
	public void showProgress(String title, String message);
	public void changeProgress(String title, String message);
	public void hideProgress();
}
