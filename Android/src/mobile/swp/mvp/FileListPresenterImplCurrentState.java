package mobile.swp.mvp;

import java.util.List;

import org.json.JSONObject;



public class FileListPresenterImplCurrentState extends FileListPresenterImpl implements FileListPresenter, NavigateBarPresenter, BasePresenter {
	
	
	public FileListPresenterImplCurrentState(FileListView view, int page)
	{
		super(view, page);
	}

	@Override
	protected List<JSONObject> getFileList()
	{
		return getFileList(m_nPage);
	}
	
	
	@Override
	protected void showPageTitle()
	{
		String title = "Current State: Critical and Core Roles";
		
		view.showPageTitle(title);
		
	}
	
	@Override
	public boolean gotoNextPage() {		
		view.gotoCurrentStatePage();
		return true;
	}
}
