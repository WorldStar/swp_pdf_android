package mobile.swp.mvp;

import mobile.swp.AppContext;
import mobile.swp.R;
import mobile.swp.choice.ChoiceManager;
import mobile.swp.choice.EnvironmentalScanManager;
import mobile.swp.component.dialog.CommonDialog;
import mobile.swp.component.dialog.DialogFactory;
import mobile.swp.pdf.creator.SWPDocumentCreator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import common.design.layout.ScreenAdapter;
import common.library.utils.BackgroundTaskUtils;
import common.library.utils.BackgroundTaskUtils.OnTaskProgress;
import common.library.utils.DataUtils;
import common.library.utils.MessageUtils;
import common.library.utils.OnAlertClickListener;
import common.list.adapter.ItemCallBack;
import common.list.adapter.ItemResult;



public class EstimatingRiskPresenterImpl implements EstimatingRiskPresenter, NavigateBarPresenter, BasePresenter {
	EstimatingRiskView			view;
	EnvironmentalScanManager 	m_Manager;

	
	public EstimatingRiskPresenterImpl(EstimatingRiskView view)
	{
		this.view = view;
		
		m_Manager = ChoiceManager.getInstance().getEnvironmentalScanManager();
	}

	private void eraseSelectedData()
	{
		for(int i = 0; i < 3; i++ )
		{
			for(int j = 0; j < 3; j++)
			{
				String tag = "panel_" + i + j;
				DataUtils.savePreference(tag, -1);
			}
		}
	}
	
	@Override
	public boolean gotoPrevPage() {			
		
		return true;
	}

	@Override
	public boolean gotoNextPage() {		
		MessageUtils.showEditDialog((Context)view, new OnAlertClickListener() {
			
			@Override
			public void onInputText(final String text) {
				((BaseView)view).showProgress("", "Please wait...");
				new BackgroundTaskUtils(new OnTaskProgress() {
					boolean m_bSucess = false;
					@Override
					public void onProgress() {
						m_bSucess = SWPDocumentCreator.getInstance().makeEnvironmentalScanDoc(text);
						
					}
					
					@Override
					public void onFinished() {
						((BaseView)view).hideProgress();
						if( m_bSucess == true )
						{
							m_Manager.setFirstStage();
							view.gotoFileListPage();
							eraseSelectedData();								
						}
						else
						{
							MessageUtils.showMessageDialog((Context)view, ((Context)view).getString(R.string.exit_file_error));
						}						
					}
				}).execute();			
			}
		});
		
		return true;
	}
	
	@Override
	public boolean gotoMenuPage() {
		DialogFactory.getInstance().showMenu((Context)view, new ItemCallBack() {
			
			@Override
			public void doClick(ItemResult result) {
				ChoiceManager.getInstance().getEnvironmentalScanManager().setFirstStage();
				AppContext.gotoMenuPage((Activity)view, result.mItemNumber);			
			}
		});
		return true;
	}

	@Override
	public void initData() {
		String [][] choice = new String[3][3];
		JSONObject data = m_Manager.getData();
		JSONArray  items = data.optJSONArray("items");
		
		for(int i = 0; i < choice.length; i++ )
		{
			for(int j = 0; j < choice[i].length; j++)
			{
				String tag = "panel_" + i + j;
				int selnum = DataUtils.getPreference(tag, -1);
				
				choice[i][j] = "";
				if(selnum >= 0)
				{
					JSONObject selinfo = items.optJSONObject(selnum);
					if( selinfo != null )
						choice[i][j] = selinfo.optString("subtitle", "");
				}
			}
		}
		
//		view.showSelectedChoiceInfo(choice);
	}

	@Override
	public void showSelectChoiceDialog(final String tag) {
		JSONObject data = new JSONObject();
		
		int selNum = DataUtils.getPreference(tag, 0);
		try {
			data.put(CommonDialog.DIALOG_WIDTH, ScreenAdapter.computeWidth(924));
			data.put(CommonDialog.DIALOG_HEIGHT, ScreenAdapter.computeHeight(750 * 5 / 4));
			data.put(DialogFactory.DIALOG_TITLE, "Estimating Risk");
			data.put(DialogFactory.SEL_NUM, selNum);
			data.put(DialogFactory.ITEMS, m_Manager.getData());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DialogFactory.getInstance().createDialog((Context)view, DialogFactory.SELECT_LEVEL_DIALOG_ID, data, new ItemCallBack() {
			
			@Override
			public void doClick(ItemResult result) {
				selectedChoiceInfo(tag, result);				
			}
		});
	}	
	
	private void selectedChoiceInfo(String tag, ItemResult result)
	{
		String content = result.itemData.optString("item_name", "");
		DataUtils.savePreference(tag, result.mItemNumber);
		
		view.showSelectedChoiceInfo(tag, content);
	}

}
