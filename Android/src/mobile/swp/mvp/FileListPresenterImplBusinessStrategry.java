package mobile.swp.mvp;

import java.util.List;

import mobile.swp.AppContext;
import mobile.swp.choice.BusinessResponseManager;
import mobile.swp.choice.ChoiceManager;
import mobile.swp.component.dialog.DialogFactory;
import mobile.swp.constant.Const;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;

import common.list.adapter.ItemCallBack;
import common.list.adapter.ItemResult;



public class FileListPresenterImplBusinessStrategry extends FileListPresenterImpl implements FileListPresenter, NavigateBarPresenter, BasePresenter {
	BusinessResponseManager m_Manager;
	
	public FileListPresenterImplBusinessStrategry(FileListView view, int page)
	{
		super(view, page);
		
		JSONObject choice = ChoiceManager.getInstance().getChoiceInfo();
		m_Manager = ChoiceManager.getInstance().getBusinessResponseManager();
		if(page == Const.BUSINESS_STRATEGY)
			m_Manager.setData(choice.optJSONObject("business"));
		if(page == Const.SEGMANET_ROLE)
			m_Manager.setData(choice.optJSONObject("role"));
		
		m_Manager.setFirstStage();
	}
	
	@Override
	protected void loadFileList()
	{
		if( m_Manager.isFirstStage() )
		{
			super.loadFileList();
		}
	}
	
	@Override
	protected List<JSONObject> getFileList()
	{
		return getFileList(m_nPage);
	}
	
	@Override
	protected void showPageTitle()
	{
		JSONObject choice = ChoiceManager.getInstance().getChoiceInfo();
		
		JSONObject data = new JSONObject();
		if(m_nPage == Const.BUSINESS_STRATEGY)
			data = choice.optJSONObject("business");
		if(m_nPage == Const.SEGMANET_ROLE)
			data = choice.optJSONObject("role");
		
		String title = data.optString("title", "Business Strategy");
		
		view.showPageTitle(title);
		
	}
	@Override
	public boolean gotoNextPage() {		
		m_Manager.setFirstStage();
		view.gotoBusinessResponsePage();
		return true;
	}
}
