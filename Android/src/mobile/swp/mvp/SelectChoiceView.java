package mobile.swp.mvp;


public interface SelectChoiceView {
	public void showSelectedButton(int num);
	public void showChoiceInfo(String title, String description, int level, boolean firstFlag);
	
	public void gotoHomePage(int menuNum);		
	public void changeSelfPage();
}
