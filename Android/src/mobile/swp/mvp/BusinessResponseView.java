package mobile.swp.mvp;



public interface BusinessResponseView {
	public void gotoFileListPage();		
	public void changeSelfPage();
	public String getBusinessResponse();
	
	public void showBusinessResponseInfo(String title, String description, String response);

}
