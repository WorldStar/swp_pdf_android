package mobile.swp.mvp;

import java.util.List;

import org.json.JSONObject;


public interface FileListView {
	public void showPageTitle(String title);
	public void showFileListData(List<JSONObject> filelist);
	
	public void gotoBusinessResponsePage();
	public void gotoEnvironmentalScanPage();
	public void gotoCurrentStatePage();
	public void gotoFuturingPage();
	public void gotoGapAnalysisPage();
	public void gotoActionPlanningPage();
	public void gotoMonitorReportPage();
	public void gotoPDFViewPage(String path);
}
