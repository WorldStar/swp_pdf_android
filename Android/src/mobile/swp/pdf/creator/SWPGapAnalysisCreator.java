package mobile.swp.pdf.creator;

import org.json.JSONArray;
import org.json.JSONObject;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import common.pdf.utils.PDFMaker;

public class SWPGapAnalysisCreator extends SWPBaseCreator {
	public SWPGapAnalysisCreator(JSONObject data)
	{
		super();
		m_Data = data;
		m_nBasePageNum = 0;
	}

	@Override
	public void createPages(Document document, PdfWriter writer) {
		addTitlePage(document, writer);
		addContentPage(document, writer);
	}

	@Override
	public void addTitlePage(Document document, PdfWriter writer) {
		super.addTitlePage(document, writer);
		SWPCreatorUtils.addSWPTitle(document, writer, "Gap Analysis");
	}

	@Override
	public void addContentPage(Document document, PdfWriter writer) {
		addPage(document, writer);
	}
	
	private void addPage(Document document, PdfWriter writer)
	{
		SWPCreatorUtils.createEmptyPage(document, writer, 0);
		SWPCreatorUtils.addTableTitle(document, writer, getPageTitle(12));	
		PDFMaker.addParagraph(document, getLabel(12, 0, 0) + " " + getData(12, 0, 1), 10, 0, FontFamily.UNDEFINED, 12, BaseColor.BLACK);
		addTable(document, writer);
	}
	
	private void addTable(Document document, PdfWriter writer)
	{
		float[] columnWidths = {1, 1, 1, 1};

		PdfPTable table = new PdfPTable(columnWidths.length); 
		table.setWidthPercentage(100.0f);
		table.setSpacingBefore(10f);
		table.setSpacingAfter(10f);
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		float height = document.getPageSize().getWidth() / 11f;
		
		JSONObject data = getData(12);
		if( data == null )
			return;
		
		JSONArray  list = data.optJSONArray("grid_data");
		if( list == null )
			return;
		
		try {
			table.setWidths(columnWidths);
			
			// Table Header
			for(int i = 0; i < columnWidths.length; i++ )
			{
				PdfPCell cell = PDFMaker.makePDFTabelCell(list.optString(i, ""), FontFamily.UNDEFINED, 12.0f, BaseColor.WHITE, SWPCreatorUtils.tableHeaderBackColor );
				PDFMaker.layoutPDFTabelCell(cell, 5, Element.ALIGN_MIDDLE, Element.ALIGN_CENTER, 30);
				table.addCell(cell);					
			}
			
			for(int i = 0; i < 10; i++ )
			{
				for(int j = 0; j < columnWidths.length; j++ )
				{
					BaseColor backColor = BaseColor.WHITE;
					if( i % 2 == 1 )
						backColor = new BaseColor(241, 241, 242);
					PdfPCell cell = PDFMaker.makePDFTabelCell(list.optString((i + 1) * columnWidths.length + j, ""), FontFamily.UNDEFINED, 10.0f, BaseColor.BLACK, backColor);
					PDFMaker.layoutPDFTabelCell(cell, 5, Element.ALIGN_TOP, height);
					
					table.addCell(cell);					
				}
			}
			document.add(table);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}	
	}
}
