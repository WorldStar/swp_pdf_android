package mobile.swp.pdf.creator;

import org.json.JSONObject;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import common.pdf.utils.PDFMaker;

public class SWPFuturingCreator extends SWPBaseCreator {

	public SWPFuturingCreator(JSONObject data)
	{
		super();
		m_Data = data;
	}

	@Override
	public void createPages(Document document, PdfWriter writer) {
		addTitlePage(document, writer);
		addContentPage(document, writer);
	}

	@Override
	public void addTitlePage(Document document, PdfWriter writer) {
		super.addTitlePage(document, writer);
		SWPCreatorUtils.addSWPTitle(document, writer, "Futuring");
	}

	@Override
	public void addContentPage(Document document, PdfWriter writer) {
		m_nBasePageNum = 0;

		addPage1(document, writer);
		addPage2(document, writer);
		addPage3(document, writer);
		
		SWPCurrentStateCreator creator = new SWPCurrentStateCreator(m_Data);
		creator.setCategory(2);
		creator.addContentPage(document, writer);
	}
	
	private void addPage1(Document document, PdfWriter writer)
	{
		SWPCreatorUtils.createEmptyPage(document, writer, 0);
		SWPCreatorUtils.addTableTitle(document, writer, getLabel(1, 0, 1));	
		PDFMaker.addParagraph(document, "Segment:", 20, 0, FontFamily.UNDEFINED, 12, BaseColor.BLACK);
		addTable1(document, writer);
	}
	
	private void addTable1(Document document, PdfWriter writer)
	{
		float[] columnWidths = {1, 1.8f, 1.8f, 2.5f};
		String [] column1 = { "Profile Data", "Trend Data" };
		String [][] column2 = {
				{
					getLabel(1, 1, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(1, 2, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(1, 3, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(1, 4, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(1, 5, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(1, 6, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(1, 7, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(1, 8, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(1, 9, 0).replace("  ", "").replace(":", "").replace("\r\n", " ")
				},
				{
					getLabel(2, 1, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(2, 2, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(2, 3, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(2, 4, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(2, 5, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(2, 6, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(2, 7, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(2, 8, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(2, 9, 0).replace("  ", "").replace(":", "").replace("\r\n", " ")
				}
		};
		
		String [][] datacolumn = {
				{
					getData(1, 1, 2),
					getData(1, 2, 2),
					getData(1, 3, 2),
					getData(1, 4, 2),
					getData(1, 5, 2),
					getData(1, 6, 2),
					getData(1, 7, 2),
					getData(1, 8, 2),
					getData(1, 9, 2)
				},
				{
					getData(2, 1, 2),
					getData(2, 2, 2),
					getData(2, 3, 2),
					getData(2, 4, 2),
					getData(2, 5, 2),
					getData(2, 6, 2),
					getData(2, 7, 2),
					getData(2, 8, 2),
					getData(2, 9, 2)
				}
		};
		
		String [][] flagcolumn = {
				{
					getDataFlag(1, 1, 1),
					getDataFlag(1, 2, 1),
					getDataFlag(1, 3, 1),
					getDataFlag(1, 4, 1),
					getDataFlag(1, 5, 1),
					getDataFlag(1, 6, 1),
					getDataFlag(1, 7, 1),
					getDataFlag(1, 8, 1),
					getDataFlag(1, 9, 1)
				},
				{
					getDataFlag(2, 1, 1),
					getDataFlag(2, 2, 1),
					getDataFlag(2, 3, 1),
					getDataFlag(2, 4, 1),
					getDataFlag(2, 5, 1),
					getDataFlag(2, 6, 1),
					getDataFlag(2, 7, 1),
					getDataFlag(2, 8, 1),
					getDataFlag(2, 9, 1)
				}
		};

		PdfPTable table = new PdfPTable(columnWidths.length); 
		table.setWidthPercentage(100.0f);
		table.setSpacingBefore(20f);
		table.setSpacingAfter(10f);
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		float height = document.getPageSize().getWidth() / 2.4f;
		try {
			table.setWidths(columnWidths);
			
			// Table Header
			String [] header = { "Type", "Examples", getLabel(1, 0, 0), getLabel(1, 0, 1)};
			for(int i = 0; i < columnWidths.length; i++ )
			{
				PdfPCell cell = PDFMaker.makePDFTabelCell(header[i], FontFamily.UNDEFINED, 12.0f, BaseColor.WHITE, SWPCreatorUtils.tableHeaderBackColor );
				PDFMaker.layoutPDFTabelCell(cell, 5, Element.ALIGN_MIDDLE, 30);
				table.addCell(cell);					
			}
			
			for(int i = 0; i < column1.length; i++ )
			{
				for(int j = 0; j < columnWidths.length; j++ )
				{
					PdfPCell cell = null;
					if( j == 0 )
					{
						cell = PDFMaker.makePDFTabelCell(column1[i], FontFamily.UNDEFINED, 10.0f );
						PDFMaker.layoutPDFTabelCell(cell, 5, Element.ALIGN_TOP, height);
					}
					if( j == 1 )
					{
						cell = PDFMaker.makePDFTabelCell(column2[i], FontFamily.UNDEFINED, 10.0f );
						PDFMaker.layoutPDFTabelCell(cell, 5, Element.ALIGN_TOP, height);
					}
					if( j == 2 )
					{
						cell = PDFMaker.makePDFTabelCell(datacolumn[i], FontFamily.UNDEFINED, 10.0f );
						PDFMaker.layoutPDFTabelCell(cell, 5, Element.ALIGN_TOP, height);
					}
					if( j == 3 )
					{
						cell = PDFMaker.makePDFTabelCell(flagcolumn[i], FontFamily.UNDEFINED, 10.0f );
						PDFMaker.layoutPDFTabelCell(cell, 5, Element.ALIGN_TOP, height);
					}
					table.addCell(cell);					
				}
			}
			document.add(table);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}	
	}
	
	private void addPage2(Document document, PdfWriter writer)
	{
		SWPCreatorUtils.createEmptyPage(document, writer, 0);
		SWPCreatorUtils.addTableTitle(document, writer, getPageTitle(3));	
		addTable2(document, writer);
	}
	
	private void addTable2(Document document, PdfWriter writer)
	{
		float[] columnWidths = {1.2f, 1.8f, 1.8f, 2.5f};
		String [] column1 = { "", "Competencies\r\nand Skills", "Attributes" };

		String [][] column2 = {
				{
					getLabel(3, 1, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(3, 2, 0).replace("  ", "").replace(":", "").replace("\r\n", " ")					
				},
				{
					getLabel(3, 4, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(3, 5, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(3, 6, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(3, 7, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(3, 8, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(3, 9, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(3, 10, 0).replace("  ", "").replace(":", "").replace("\r\n", " ")
				},
				{
					getLabel(4, 1, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(4, 2, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(4, 3, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(4, 4, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(4, 5, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(4, 6, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(4, 7, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(4, 8, 0).replace("  ", "").replace(":", "").replace("\r\n", " "),
					getLabel(4, 9, 0).replace("  ", "").replace(":", "").replace("\r\n", " ")
				}
		};
		
		String [][] datacolumn = {
				{
					getData(3, 1, 2),
					getData(3, 2, 2)					
				},
				{
					getData(3, 4, 2),
					getData(3, 5, 2),
					getData(3, 6, 2),
					getData(3, 7, 2),
					getData(3, 8, 2),
					getData(3, 9, 2),
					getData(3, 10, 2)
				},
				{
					getData(4, 1, 2),
					getData(4, 2, 2),
					getData(4, 3, 2),
					getData(4, 4, 2),
					getData(4, 5, 2),
					getData(4, 6, 2),
					getData(4, 7, 2),
					getData(4, 8, 2),
					getData(4, 9, 2)
				}
		};
		
		String [][] flagcolumn = {
				{
					getDataFlag(3, 1, 1),
					getDataFlag(3, 2, 1)					
				},
				{
					getDataFlag(3, 4, 1),
					getDataFlag(3, 5, 1),
					getDataFlag(3, 6, 1),
					getDataFlag(3, 7, 1),
					getDataFlag(3, 8, 1),
					getDataFlag(3, 9, 1),
					getDataFlag(3, 10, 1)
				},
				{
					getDataFlag(4, 1, 1),
					getDataFlag(4, 2, 1),
					getDataFlag(4, 3, 1),
					getDataFlag(4, 4, 1),
					getDataFlag(4, 5, 1),
					getDataFlag(4, 6, 1),
					getDataFlag(4, 7, 1),
					getDataFlag(4, 8, 1),
					getDataFlag(4, 9, 1)
				}
		};
		

		PdfPTable table = new PdfPTable(columnWidths.length); 
		table.setWidthPercentage(100.0f);
		table.setSpacingBefore(20f);
		table.setSpacingAfter(10f);
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		float height = document.getPageSize().getWidth() / 2.4f;
		float [] heightArray = { height / 4, height, height };
		try {
			table.setWidths(columnWidths);
			
			// Table Header
			String [] header = { "Type", "Examples", "Current State", "No Change Future State"};
			for(int i = 0; i < columnWidths.length; i++ )
			{
				PdfPCell cell = PDFMaker.makePDFTabelCell(header[i], FontFamily.UNDEFINED, 12.0f, BaseColor.WHITE, SWPCreatorUtils.tableHeaderBackColor );
				PDFMaker.layoutPDFTabelCell(cell, 5, Element.ALIGN_MIDDLE, 30);
				table.addCell(cell);					
			}
			
			for(int i = 0; i < column1.length; i++ )
			{
				for(int j = 0; j < columnWidths.length; j++ )
				{
					PdfPCell cell = null;
					if( j == 0 )
					{
						cell = PDFMaker.makePDFTabelCell(column1[i], FontFamily.UNDEFINED, 10.0f );
						PDFMaker.layoutPDFTabelCell(cell, 5, Element.ALIGN_TOP, heightArray[i]);
					}
					if( j == 1 )
					{
						cell = PDFMaker.makePDFTabelCell(column2[i], FontFamily.UNDEFINED, 10.0f );
						PDFMaker.layoutPDFTabelCell(cell, 5, Element.ALIGN_TOP, heightArray[i]);
					}
					if( j == 2 )
					{
						cell = PDFMaker.makePDFTabelCell(datacolumn[i], FontFamily.UNDEFINED, 10.0f );
						PDFMaker.layoutPDFTabelCell(cell, 5, Element.ALIGN_TOP, heightArray[i]);
					}
					if( j == 3 )
					{
						cell = PDFMaker.makePDFTabelCell(flagcolumn[i], FontFamily.UNDEFINED, 10.0f );
						PDFMaker.layoutPDFTabelCell(cell, 5, Element.ALIGN_TOP, heightArray[i]);
					}
					table.addCell(cell);					
				}
			}
			document.add(table);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}	
	}
	
	private void addPage3(Document document, PdfWriter writer)
	{
		SWPCreatorUtils.createEmptyPage(document, writer, 0);
		SWPCreatorUtils.addTableTitle(document, writer, "Future Scenarios");	
		PDFMaker.addParagraph(document, "Futuring Grid", 20, 0, FontFamily.UNDEFINED, 12, BaseColor.BLACK);
		addTable3(document, writer);
		PDFMaker.addParagraphUnderline(document, getData(5, 2, 0), 20, 0, FontFamily.UNDEFINED, 12, BaseColor.BLACK, 7);
	}
	
	private void addTable3(Document document, PdfWriter writer)
	{
		float[] columnWidths = {1, 4, 1};

		PdfPTable table = new PdfPTable(columnWidths.length); 
		table.setWidthPercentage(80.0f);
		table.setSpacingBefore(20f);
		table.setSpacingAfter(10f);
		table.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		float height = document.getPageSize().getWidth() * 0.8f * 4 / 6;;
		
		try {
			table.setWidths(columnWidths);
			
			PdfPCell emptyCell = PDFMaker.makeStringOnlyCell("", FontFamily.UNDEFINED, 10, BaseColor.WHITE);
			PDFMaker.layoutPDFTabelCell(emptyCell, 10, Element.ALIGN_CENTER, 0);
			table.addCell(emptyCell);
			
			PdfPCell topCell = PDFMaker.makeStringOnlyCell("Factor 1", FontFamily.UNDEFINED, 12, BaseColor.BLACK);
			PDFMaker.layoutPDFTabelCell(topCell, 10, Element.ALIGN_MIDDLE, Element.ALIGN_CENTER, 0);
			table.addCell(topCell);

			table.addCell(emptyCell);
			
			PdfPCell leftCell = PDFMaker.makeStringOnlyCell("Factor 2", FontFamily.UNDEFINED, 12, BaseColor.BLACK);
			PDFMaker.layoutPDFTabelCell(leftCell, 0, Element.ALIGN_MIDDLE, Element.ALIGN_CENTER, height);
			table.addCell(leftCell);

			PdfPCell centerCell = new PdfPCell();
			addScenarioGraph(centerCell, height / 2);
			PDFMaker.layoutPDFTabelCell(centerCell, 0, Element.ALIGN_CENTER, height);
			table.addCell(centerCell);

			table.addCell(leftCell);
			
			table.addCell(emptyCell);
			table.addCell(topCell);
			table.addCell(emptyCell);

			
			document.add(table);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}	
		
//		Rectangle rect = document.getPageSize();
//		float x = (rect.getLeft() + rect.getRight()) / 2;
//		float y = (rect.getTop() + rect.getBottom()) / 2;
//		PDFMaker.drawLines(writer, rect.getLeft(), y, rect.getRight(), y, 3, BaseColor.BLACK);
//		PDFMaker.drawLines(writer, x, rect.getTop(), x, rect.getBottom(), 3, BaseColor.BLACK);
	}
	
	private void addScenarioGraph(PdfPCell cell, float height)
	{
		float[] columnWidths = {1, 1};

		PdfPTable table = new PdfPTable(columnWidths.length); 
		table.setWidthPercentage(100.0f);
		table.setHorizontalAlignment(Element.ALIGN_CENTER);
		float thickWidth = 1.0f;
		try {
			table.setWidths(columnWidths);
			
			String content = getData(5, 0, 1);

			
			for(int i = 0; i < 4; i++ )
			{
				BaseColor textColor = BaseColor.LIGHT_GRAY;
				if( content.contains("" + (i + 1)) )
					textColor = BaseColor.BLUE;
					
				PdfPCell subcell = PDFMaker.makeStringOnlyCell("Scenario " + (i + 1), FontFamily.UNDEFINED, 10, textColor);
				PDFMaker.layoutPDFTabelCell(subcell, 0, Element.ALIGN_MIDDLE, Element.ALIGN_CENTER, height);	
				
				subcell.setBorderWidthLeft(0);
				subcell.setBorderWidthTop(0);
				subcell.setBorderWidthRight(0);
				subcell.setBorderWidthBottom(0);
				
				switch(i)
				{
				case 0:
					subcell.setBorderWidthRight(thickWidth);
					subcell.setBorderWidthBottom(thickWidth);
					break;
				case 1:
					subcell.setBorderWidthLeft(thickWidth);
					subcell.setBorderWidthBottom(thickWidth);
					break;
				case 2:
					subcell.setBorderWidthRight(thickWidth);
					subcell.setBorderWidthTop(thickWidth);
					break;
				case 3:
					subcell.setBorderWidthLeft(thickWidth);
					subcell.setBorderWidthTop(thickWidth);
					break;
				}
					
				
				table.addCell(subcell);				
			}


			
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}	
		
		cell.addElement(table);
		cell.setBorder(Rectangle.NO_BORDER);

	}
	
}
