package mobile.swp.pdf.creator;

import mobile.swp.choice.ChoiceManager;
import mobile.swp.choice.EnvironmentalScanManager;

import org.json.JSONArray;
import org.json.JSONObject;

import android.graphics.Color;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import common.library.utils.DataUtils;
import common.pdf.utils.PDFMaker;

public class SWPEstimatingRiskCreator extends SWPBaseCreator {
	public SWPEstimatingRiskCreator()
	{
		super();
	}

	@Override
	public void createPages(Document document, PdfWriter writer) {
		addTitlePage(document, writer);
		addContentPage(document, writer);
	}

	@Override
	public void addTitlePage(Document document, PdfWriter writer) {
	}

	@Override
	public void addContentPage(Document document, PdfWriter writer) {
		super.addContentPage(document, writer);
		SWPCreatorUtils.addTableTitle(document, writer, "Estimating Risk");	
		addTableSubTitle(document, writer, "Where on the grid does each environmental scan item ﬁt?");
		
		addTable(document, writer);
	}

	private void addTableSubTitle(Document document, PdfWriter writer, String titleString)
	{
		if( document == null || writer == null )
			return;
		PDFMaker.addParagraph(document, titleString, 20, 0, FontFamily.UNDEFINED, 11.0f, new BaseColor(81, 92, 114));
	}
	
	private void addTable(Document document, PdfWriter writer)
	{
		PdfPTable table = new PdfPTable(2); 
		table.setWidthPercentage(95.0f);
		table.setSpacingBefore(20f);
		table.setSpacingAfter(10f);
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		Rectangle rect = document.getPageSize();
		float size = rect.getWidth() / 4;
		float[] columnWidths = {0.1f, 1.0f};
		try {
			table.setWidths(columnWidths);
			
			PdfPCell leftCell = new PdfPCell();
			addLeftSide(leftCell, "Impact on Business", size );
			table.addCell(leftCell);
			
			PdfPCell panelCell = new PdfPCell();
			addPanel(panelCell, size );
			table.addCell(panelCell);
			
			PdfPCell emptyCell = PDFMaker.makePDFTabelCell("", FontFamily.UNDEFINED, 10.0f);
			PDFMaker.layoutPDFTabelCell(emptyCell, 0, Element.ALIGN_CENTER, 0);
			emptyCell.setBorder(Rectangle.NO_BORDER);
			table.addCell(emptyCell);

			PdfPCell bottomCell = new PdfPCell();
			addBottomSide(bottomCell, "Impact on Business Objectives", 0 );
			table.addCell(bottomCell);

			document.add(table);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}	
	}
	
	private void addLeftSide(PdfPCell cell, String content, float size)
	{
		PdfPTable table = new PdfPTable(1); 
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPCell subcell = PDFMaker.makePDFTabelCell(content, FontFamily.UNDEFINED, 14.0f, SWPCreatorUtils.tableBorderColor, BaseColor.WHITE);
		PDFMaker.layoutPDFTabelCell(subcell, 0, Element.ALIGN_MIDDLE, Element.ALIGN_CENTER, size * 3 + size / 7 + size / 3);
		subcell.setRotation(90);
		subcell.setBorder(Rectangle.NO_BORDER);
		table.addCell(subcell);
		
		cell.addElement(table);
		cell.setBorder(Rectangle.NO_BORDER);
	}
	
	private void addBottomSide(PdfPCell cell, String content, float size)
	{
		PdfPTable table = new PdfPTable(1); 
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPCell subcell = PDFMaker.makePDFTabelCell(content, FontFamily.UNDEFINED, 14.0f, SWPCreatorUtils.tableBorderColor, BaseColor.WHITE);
		PDFMaker.layoutPDFTabelCell(subcell, 0, Element.ALIGN_MIDDLE, Element.ALIGN_CENTER, size);
		subcell.setBorder(Rectangle.NO_BORDER);
		table.addCell(subcell);
		
		cell.addElement(table);
		cell.setBorder(Rectangle.NO_BORDER);
	}
	
	private void addPanel(PdfPCell cell, float size)
	{
		PdfPTable table = new PdfPTable(4); 
		table.setWidthPercentage(100.0f);
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		float[] columnWidths = {1.0f/7, 1.0f, 1.0f, 1.0f};
		String []label = { "High", "Medium", "Low"};
		int 	[] alignment = {Element.ALIGN_RIGHT, Element.ALIGN_CENTER, Element.ALIGN_LEFT};
		
		EnvironmentalScanManager manager = ChoiceManager.getInstance().getEnvironmentalScanManager();
		JSONObject data = manager.getData();
		JSONArray  items = data.optJSONArray("items");

		int [][] panelColor = {
				{ Color.rgb(180, 173, 173), Color.rgb(152, 144, 144), Color.rgb(102, 99, 99) },
				{ Color.rgb(133, 128, 128), Color.rgb(133, 128, 128), Color.rgb(152, 144, 144) },
				{ Color.rgb(133, 128, 128), Color.rgb(133, 128, 128), Color.rgb(180, 173, 173) }
		};
			
		
		try {
			table.setWidths(columnWidths);
			
			for(int i = 0; i < 3; i++)
			{
				for(int j = 0; j < 4; j++ )
				{
					if( j == 0 )
					{
						PdfPCell subcell = PDFMaker.makePDFTabelCell(label[i], FontFamily.UNDEFINED, 10.0f, SWPCreatorUtils.tableBorderColor, BaseColor.WHITE);
						PDFMaker.layoutPDFTabelCell(subcell, 5, Element.ALIGN_MIDDLE, alignment[i], size);
						subcell.setRotation(90);
						subcell.setBorder(Rectangle.NO_BORDER);
						table.addCell(subcell);
					}
					else
					{
						String tag = "panel_" + i + (j - 1);
						
						String choice = "";
						int selnum = DataUtils.getPreference(tag, -1);
						
						if(selnum >= 0)
						{
							JSONObject selinfo = items.optJSONObject(selnum);
							if( selinfo != null )
								choice = selinfo.optString("subtitle", "");
						}
						
						PdfPCell subcell = PDFMaker.makePDFTabelCell(choice, FontFamily.UNDEFINED, 12.0f, BaseColor.WHITE, new BaseColor(panelColor[i][j-1]), 2.0f, BaseColor.BLACK);
						PDFMaker.layoutPDFTabelCell(subcell, 0, Element.ALIGN_MIDDLE, Element.ALIGN_CENTER, size);
						table.addCell(subcell);
					}
				}
			}
			
			PdfPCell emptyCell = PDFMaker.makePDFTabelCell("", FontFamily.UNDEFINED, 10.0f);
			PDFMaker.layoutPDFTabelCell(emptyCell, 0, Element.ALIGN_CENTER, size / 7);
			emptyCell.setBorder(Rectangle.NO_BORDER);
			table.addCell(emptyCell);
			
			for(int j = 0; j < 3; j++ )
			{
				PdfPCell subcell = PDFMaker.makePDFTabelCell(label[2 - j], FontFamily.UNDEFINED, 10.0f, SWPCreatorUtils.tableBorderColor, BaseColor.WHITE);
				PDFMaker.layoutPDFTabelCell(subcell, 5, Element.ALIGN_MIDDLE, alignment[2 - j], size / 7);
				subcell.setBorder(Rectangle.NO_BORDER);
				table.addCell(subcell);
				
			}
			
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}	
		
		cell.addElement(table);
		cell.setBorder(Rectangle.NO_BORDER);
	}
	
}
