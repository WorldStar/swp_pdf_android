package mobile.swp.pdf.creator;

import mobile.swp.choice.ChoiceManager;

import org.json.JSONArray;
import org.json.JSONObject;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import common.library.utils.CheckUtils;
import common.pdf.utils.PDFMaker;

public class SWPDefaultPageCreator extends SWPBaseCreator {
	public SWPDefaultPageCreator()
	{
		super();
	}

	@Override
	public void createPages(Document document, PdfWriter writer) {
		addTitlePage(document, writer);
		addContentPage(document, writer);
	}

	@Override
	public void addTitlePage(Document document, PdfWriter writer) {
		super.addTitlePage(document, writer);
		SWPCreatorUtils.addSWPTitle(document, writer, "Assessments");
	}

	@Override
	public void addContentPage(Document document, PdfWriter writer) {
		if( document == null || writer == null )
			return;
				
		JSONObject choice = ChoiceManager.getInstance().getChoiceInfo();
		if( choice == null )
			return;
		
		JSONObject choice1 = choice.optJSONObject("choice1");
		if( choice1 == null )
			return;
		
		addStrategicPage(document, writer, choice1);
		
		JSONObject choice2 = choice.optJSONObject("choice2");
		if( choice2 == null )
			return;
		
		addStrategicPage(document, writer, choice2);
	}

	private void addStrategicPage(Document document, PdfWriter writer, JSONObject choice)
	{
		if( document == null || writer == null )
			return;

		document.newPage();  
		
		SWPCreatorUtils.addHeaderLine1(document);
		SWPCreatorUtils.addFooterLine1(document, writer);
		
		PdfPTable table = new PdfPTable(2); 
		table.setWidthPercentage(95.0f);
		table.setSpacingBefore(40f);
		table.setSpacingAfter(10f);
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		float[] columnWidths = {1.8f, 1f};
		try {
			table.setWidths(columnWidths);
			addStrategicTableFirstRow(table, choice);
			addStrategicTableSecondRow(table);
			addChoiceSelectedInfo(table, choice);
			
			document.add(table);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}	
		
	}
	
	private void addStrategicTableFirstRow(PdfPTable table, JSONObject choice)
	{
		String fieldName = choice.optString("title");		
		PdfPCell cell1 = PDFMaker.makePDFTabelCell(fieldName, Font.FontFamily.UNDEFINED, 15.0f, BaseColor.WHITE, SWPCreatorUtils.tableHeaderBackColor);
		PDFMaker.layoutPDFTabelCell(cell1, 5, Element.ALIGN_MIDDLE, 25);
	
		PdfPCell cell2 = PDFMaker.makePDFTabelCell("How Do We Rate?", Font.FontFamily.COURIER, 12.0f, new BaseColor(151, 193, 209), SWPCreatorUtils.tableHeaderBackColor);
		PDFMaker.layoutPDFTabelCell(cell2, 5, Element.ALIGN_MIDDLE, 25);

		table.addCell(cell1);
		table.addCell(cell2);		

	}
	private void addStrategicTableSecondRow(PdfPTable table)
	{
		PdfPCell cell1 = PDFMaker.makePDFTabelCell("", Font.FontFamily.COURIER, 15.0f, BaseColor.WHITE, BaseColor.WHITE);
		PDFMaker.layoutPDFTabelCell(cell1, 5, Element.ALIGN_MIDDLE, 0);
		
		PdfPCell cell2 = new PdfPCell();
		cell2.setVerticalAlignment(Element.ALIGN_CENTER);
		cell2.setBackgroundColor(new BaseColor(59, 148, 209));
		addChoiceLevelLabelToTable(cell2);
		
		table.addCell(cell1);
		table.addCell(cell2);		

	}
	
	private void addChoiceSelectedInfo(PdfPTable table, JSONObject choice)
	{		
		int 	num = choice.optInt("num", 0);
		JSONArray choice1_items = choice.optJSONArray("items");
		if( CheckUtils.isEmpty(choice1_items) )
			return;
		
		for(int i = 0; i < choice1_items.length(); i++)
		{
			PdfPCell cell1 = PDFMaker.makePDFTabelCell((i + 1) + ". " + choice1_items.optString(i), Font.FontFamily.UNDEFINED, 10.0f);
			PDFMaker.layoutPDFTabelCell(cell1, 5, Element.ALIGN_MIDDLE, 0);

			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell1.setFollowingIndent(15.0f);
			
			PdfPCell cell2 = new PdfPCell();
			cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			int selNum = ChoiceManager.getInstance().getChoiceLevel(num, i);
			if( selNum < 0 || selNum > 4 )
				selNum = 2;
			selNum = 4 - selNum;
			addChoiceMarkToTable(cell2, selNum);
			
			table.addCell(cell1);
			table.addCell(cell2);
		}
	}
	
	private void addChoiceLevelLabelToTable(PdfPCell cell)
	{
		int columnCount = 5;
		PdfPTable table = new PdfPTable(columnCount); 
		table.setWidthPercentage(100.0f);
		float[] columnWidths = {1.0f, 1.0f, 1.2f, 1.0f, 1.6f};
		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
			
		for(int i = 0; i < columnCount; i++ )
		{
			PdfPCell subcell = PDFMaker.makePDFTabelCell("" + (i + 1), Font.FontFamily.COURIER, 9.0f, BaseColor.WHITE, new BaseColor(59, 148, 209));
			PDFMaker.layoutPDFTabelCell(subcell, 0, Element.ALIGN_CENTER, Element.ALIGN_CENTER, 15.0f);
			subcell.setBorder(Rectangle.NO_BORDER);
			
			table.addCell(subcell);
		}
		
		String level_name[] = {
			"Poor", "Fair", "Average", "Good", "Excellent"
		};
		for(int i = 0; i < columnCount; i++ )
		{
			PdfPCell subcell = PDFMaker.makePDFTabelCell(level_name[i], Font.FontFamily.COURIER, 7.0f, BaseColor.WHITE, new BaseColor(59, 148, 209));
			PDFMaker.layoutPDFTabelCell(subcell, 0, Element.ALIGN_CENTER, Element.ALIGN_CENTER, 15.0f);
			subcell.setBorder(Rectangle.NO_BORDER);
			
			table.addCell(subcell);
		}
		
		cell.addElement(table);
	}
	
	private void addChoiceMarkToTable(PdfPCell cell, int num)
	{
		int columnCount = 5;
		PdfPTable table = new PdfPTable(columnCount); 
		table.setWidthPercentage(100.0f);
			
		for(int i = 0; i < columnCount; i++ )
		{
			Image image = null;			
			if( i == num )
				image = SWPCreatorUtils.m_imgCheckmark[0];
			else
				image = SWPCreatorUtils.m_imgCheckmark[1];
			
			image.setAlignment(Element.ALIGN_CENTER);
			PdfPCell subcell = new PdfPCell(image, false);
			PDFMaker.layoutPDFTabelCell(subcell, 0, Element.ALIGN_MIDDLE, Element.ALIGN_CENTER, 0);
			subcell.setBorder(Rectangle.NO_BORDER);
					
			table.addCell(subcell);
		}
		
		cell.addElement(table);
	}

}
