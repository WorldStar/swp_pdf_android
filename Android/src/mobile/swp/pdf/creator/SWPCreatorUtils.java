package mobile.swp.pdf.creator;

import java.io.IOException;
import java.net.MalformedURLException;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;
import common.pdf.utils.PDFMaker;

public class SWPCreatorUtils {
	public static BaseColor   tableHeaderBackColor = new BaseColor(22, 39,57);
	public static BaseColor   tableBorderColor = new BaseColor(81, 92, 114);
	
	private static Image		m_imgSWPMark = null;
	public static Image		m_imgCheckmark[] = new Image[2];


	public static void loadSWPMark(String strSWPMarkPath)
	{
		try {
			m_imgSWPMark = Image.getInstance(strSWPMarkPath);
		} catch (BadElementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void loadSWPCheckMark(String strCheckMarkPath, int num)
	{
		 try {
	      	   m_imgCheckmark[num] = Image.getInstance(strCheckMarkPath);
	      	   m_imgCheckmark[num].scalePercent(12.5f, 15.0f);
			} catch (BadElementException e) {
				e.printStackTrace();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
		}
	}
	
	public static void createEmptyPage(Document document, PdfWriter writer, int type)
	{
		if( document == null || writer == null )
			return;
		document.newPage();  
		addHeaderLine1(document);			
		addFooterLine1(document, writer);	
	}
	
	public static void addHeaderLine1(Document document)
	{
		if( document == null )
			return;
		
		String empty = "";
		for( int i = 0; i < 134; i++ )
		{
			empty += " ";
		}
		Paragraph paragraph = new Paragraph();

		Chunk underline = new Chunk(empty);
		underline.setUnderline(BaseColor.DARK_GRAY, 0.1f, 0, -2f, 0, 3);
		
		Font font = new Font(Font.FontFamily.UNDEFINED, 8.0f);
		font.setColor(BaseColor.GRAY);
		Chunk headerLabel = new Chunk("                " + "ToolKit", font);
		try {
			paragraph.add(underline);
			paragraph.add(headerLabel);
			document.add(paragraph);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	public static void addFooterLine1(Document document, PdfWriter writer)
	{
		if( document == null || writer == null )
			return;
		
		String empty = "";
		for( int i = 0; i < 100; i++ )
			empty += " ";
		Paragraph paragraph = new Paragraph();

		Chunk underline = new Chunk(empty);
		underline.setUnderline(BaseColor.DARK_GRAY, 0.1f, 0, -2f, 0, 0);
		paragraph.add(underline);
		
		Paragraph	label = new Paragraph();
		Font font = new Font(Font.FontFamily.UNDEFINED, 8.0f);
		font.setColor(BaseColor.GRAY);
		Chunk version = new Chunk("Version 09.20.13, Human Capital Institute, © 2013", font);
		label.add(version);
		
		PdfContentByte canvas = writer.getDirectContent();		
		ColumnText ct = new ColumnText(canvas);
		
		Rectangle rect = document.getPageSize();
		try {
			ct.setSimpleColumn(rect.getLeft(30), rect.getBottom(70), rect.getRight(30), rect.getBottom(20));
 			ct.addElement(paragraph);
			ct.addElement(label);
			ct.go();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	public static void addSWPTitle(Document document, PdfWriter writer, String titleString)
	{
		if( document == null || writer == null )
			return;
		
		Paragraph	label = new Paragraph();
		Font font = new Font(Font.FontFamily.UNDEFINED, 50.0f);
		font.setColor(81, 92, 114);
		font.setStyle(Font.BOLD);
		Chunk title = new Chunk(titleString, font);
		label.add(title);
		
		PdfContentByte canvas = writer.getDirectContent();		
		ColumnText ct = new ColumnText(canvas);
		
		Rectangle rect = document.getPageSize();
		float titleHeight = rect.getBottom() + rect.getHeight() * 3 / 5;
		try {
			m_imgSWPMark.setAbsolutePosition(rect.getLeft(50), titleHeight + 170);
			m_imgSWPMark.scalePercent(70);			
			document.add(m_imgSWPMark);
			
			ct.setSimpleColumn(rect.getLeft(50), titleHeight - 100, rect.getRight(30), titleHeight + 200);
			ct.addElement(title);
			ct.go();			
		} catch (DocumentException e) {
			e.printStackTrace();
		} 
	}
	
	public static void addTableTitle(Document document, PdfWriter writer, String titleString)
	{
		if( document == null || writer == null )
			return;
		
		PDFMaker.addParagraph(document, titleString, 65f, 0, FontFamily.UNDEFINED, 20.0f, new BaseColor(111, 169, 217) );
	
	}
	

}
