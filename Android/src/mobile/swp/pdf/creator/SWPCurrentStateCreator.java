package mobile.swp.pdf.creator;

import org.json.JSONObject;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import common.pdf.utils.PDFMaker;

public class SWPCurrentStateCreator extends SWPBaseCreator {
	int m_nCategory = 1;
	public SWPCurrentStateCreator(JSONObject data)
	{
		super();
		m_Data = data;
	}
	
	public void setCategory(int category)
	{
		m_nCategory = category;
	}
	
	@Override
	public void createPages(Document document, PdfWriter writer) {
		addTitlePage(document, writer);
		addContentPage(document, writer);
	}

	@Override
	public void addTitlePage(Document document, PdfWriter writer) {
		super.addTitlePage(document, writer);
		String title = m_Data.optString("title", "Current State");
		SWPCreatorUtils.addSWPTitle(document, writer, title);
	}

	@Override
	public void addContentPage(Document document, PdfWriter writer) {
		addPage1(document, writer);
		addPage2(document, writer);
		addPage3(document, writer);
		addPage4(document, writer);
	}
	
	private String  getPageHeadTitle(int category, int num)
	{
		String prefix = m_Data.optString("prefix_" + category, "");
		String appendix = m_Data.optString("appendix_" + category, "");
		
		return prefix + ": " + appendix;
	}
	private void addPage1(Document document, PdfWriter writer)
	{		
		SWPCreatorUtils.createEmptyPage(document, writer, 0);		
		SWPCreatorUtils.addTableTitle(document, writer, getPageHeadTitle(m_nCategory, 1) );	
		
		addParagraph11(document, writer);		
		PDFMaker.addParagraph(document, getLabel(1, 2, 0), 30, 0, FontFamily.UNDEFINED, 12, BaseColor.BLACK);
		PDFMaker.addParagraph(document, getLabel(1, 2, 1),
				10, 0, FontFamily.UNDEFINED, 9.5f, BaseColor.BLACK);
		
		PDFMaker.addParagraphUnderline(document, getData(1, 2, 1), 15, 0, FontFamily.UNDEFINED, 12, BaseColor.BLACK, 5 );

		PDFMaker.addParagraph(document, getLabel(1, 3, 0), 25, 0, FontFamily.UNDEFINED, 11.5f, BaseColor.BLACK);
		addTable1(document);
		
		addParagraph12(document);

	}
	
	private void addParagraph11(Document document, PdfWriter writer)
	{		
		String [] content  = new String[4];
		
		content[0] = getLabel(1, 0, 0).replace(" ", "");
		content[1] = getData(1, 0, 1);
		content[2] = getLabel(1, 1, 0).replace(" ", "");
		content[3] = getData(1, 1, 1);
		
		float [] width = {1, 6.5f, 1, 6.5f};
		boolean [] flag = { false, true, false, true };
		PDFMaker.addFillParagraph(document, content, width, flag, 12.0f, 100, 20, Element.ALIGN_LEFT);
	}
	
	private void addTable1(Document document)
	{
		PdfPTable table = new PdfPTable(2); 
		table.setWidthPercentage(95.0f);
		table.setSpacingBefore(20f);
		table.setSpacingAfter(10f);
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		float height = document.getPageSize().getWidth() / 3;
		try {
			PdfPCell cell1 = PDFMaker.makePDFTabelCell(getLabel(1, 4, 0) + "\r\n\r\n" + getData(1, 4, 1), FontFamily.UNDEFINED, 12.0f);
			PDFMaker.layoutPDFTabelCell(cell1, 5, Element.ALIGN_TOP, height);
			table.addCell(cell1);
			
			PdfPCell cell2 = PDFMaker.makePDFTabelCell(getLabel(1, 5, 0) + "\r\n\r\n" + getData(1, 5, 1), FontFamily.UNDEFINED, 12.0f);
			PDFMaker.layoutPDFTabelCell(cell2, 5, Element.ALIGN_TOP, height);
			table.addCell(cell2);
			
			document.add(table);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}	
	}

	public void addParagraph12(Document document )
	{
		if( document == null )
			return;
			
		Paragraph	label = new Paragraph();
		label.setSpacingBefore(40);
		label.setSpacingAfter(0);
 
		Font font1 = new Font(FontFamily.UNDEFINED, 12);
		Chunk chunk1 = new Chunk(getComboBoxTitle(2, 1), font1);
		label.add(chunk1);
		
		Font font2 = new Font(FontFamily.UNDEFINED, 10);		
		Chunk chunk2 = new Chunk("         " + getData(2, 0, 1), font2);
		label.add(chunk2);
		
		try {
			document.add(label);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	private void addPage2(Document document, PdfWriter writer)
	{
		SWPCreatorUtils.createEmptyPage(document, writer, 0);
		
		addParagraph21(document);
		PDFMaker.addParagraph(document, getLabel(2, 1, 0), 24, 0, FontFamily.UNDEFINED, 12, BaseColor.BLACK);
		
		String label [] = {
			getLabel(2, 7, 1),
			getLabel(3, 0, 0),
			getLabel(3, 1, 0),
			getLabel(3, 2, 0)
		};
		
		String [] data = {
				getData(2, 7, 1),
				getData(3, 0, 0),
				getData(3, 1, 0),
				getData(3, 2, 0)
		};
		for(int i = 0; i < label.length; i++ )
		{
			float space = 20;
			if( i == 0 )
				space = 9;
			PDFMaker.addParagraph(document, label[i], space, 0, FontFamily.UNDEFINED, 9, BaseColor.BLACK);
			PDFMaker.addParagraphUnderline(document, data[i], 15, 0, FontFamily.UNDEFINED, 12, BaseColor.BLACK, 3 );			
		}


	}
	
	private void addParagraph21(Document document)
	{
		PdfPTable table = new PdfPTable(3); 
		table.setWidthPercentage(85.0f);
		table.setSpacingBefore(30f);
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		float[] columnWidths = {4.0f, 1, 14};
		try {
			table.setWidths(columnWidths);
			
			for( int i = 0; i < 6; i++ )
			{
				String content = "";
				if( i == 0 )
					content = getLabel(2, 1, 0);
				PdfPCell leftCell = PDFMaker.makeStringOnlyCell(content, FontFamily.UNDEFINED, 12.0f, BaseColor.BLACK);
				PDFMaker.layoutPDFTabelCell(leftCell, 2, Element.ALIGN_MIDDLE, Element.ALIGN_RIGHT, 20.0f);
				table.addCell(leftCell);
				
				PdfPCell middleCell = PDFMaker.makeStringOnlyCell((i + 1) + ".", FontFamily.UNDEFINED, 12.0f, BaseColor.BLACK);
				PDFMaker.layoutPDFTabelCell(middleCell, 2, Element.ALIGN_MIDDLE, Element.ALIGN_RIGHT, 20.0f);
				table.addCell(middleCell);
				
				PdfPCell rightCell = PDFMaker.makeStringUnderlineCell(getData(2, i + 2, 0), FontFamily.UNDEFINED, 12.0f, BaseColor.BLACK, BaseColor.BLACK);
				PDFMaker.layoutPDFTabelCell(rightCell, 2, Element.ALIGN_MIDDLE, Element.ALIGN_LEFT, 20.0f);
				table.addCell(rightCell);
			}
			
			document.add(table);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}	
	}
	
	private void addPage3(Document document, PdfWriter writer)
	{
		SWPCreatorUtils.createEmptyPage(document, writer, 0);
		SWPCreatorUtils.addTableTitle(document, writer, getPageHeadTitle(m_nCategory, 2));	
		
		addParagraph31(document, writer);		
		addParagraph32(document, writer);		
		addParagraph33(document, writer);
		
		PDFMaker.addParagraph(document, getLabel(4, 4, 0), 30, 0, FontFamily.UNDEFINED, 12, BaseColor.BLACK);
		PDFMaker.addParagraphUnderline(document, getData(4, 4, 1), 15, 0, FontFamily.UNDEFINED, 12, BaseColor.BLACK, 5 );
		
		addTable3(document);
	}
	
	private void addParagraph31(Document document, PdfWriter writer)
	{		
		String [] content  = {
				getLabel(4, 0, 0).replace(" ", ""),
				getData(4, 0, 1)
		};
		
		float [] width = {1.4f, 7};
		boolean [] flag = { false, true };
		PDFMaker.addFillParagraph(document, content, width, flag, 12.0f, 70, 20, Element.ALIGN_LEFT);
	}
	
	private void addParagraph32(Document document, PdfWriter writer)
	{		
		String [] content  = {
				getLabel(4, 1, 0).replace(" ", ""),
				getData(4, 1, 1),
				getLabel(4, 2, 0).replace(" ", ""),
				getData(4, 2, 1)
		};
		
		float [] width = {4.4f, 5, 1.1f, 3};
		boolean [] flag = { false, true, false, true };
		PDFMaker.addFillParagraph(document, content, width, flag, 12.0f, 90, 20, Element.ALIGN_LEFT);
	}
	
	private void addParagraph33(Document document, PdfWriter writer)
	{		
		String [] content  = {
				getLabel(4, 3, 0).replace(" ", ""),
				getData(4, 3, 1),
				getLabel(4, 3, 2).replace(" ", "")
		};
		
		float [] width = {6, 1, 3};
		boolean [] flag = { false, true, false };
		PDFMaker.addFillParagraph(document, content, width, flag, 12.0f, 55, 20, Element.ALIGN_RIGHT);	
	}
	
	private void addTable3(Document document)
	{
		PdfPTable table = new PdfPTable(3); 
		table.setWidthPercentage(100f);
		table.setSpacingBefore(20f);
		table.setSpacingAfter(10f);
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		float height = 25;
		float[] columnWidths = {2.4f, 1, 3};
		try {
			table.setWidths(columnWidths);
			String [] header = { "Attribute", "Level", "Comments" };
			
			for(int i = 0; i < header.length; i++ )
			{
				PdfPCell cell = PDFMaker.makePDFTabelCell(header[i], FontFamily.UNDEFINED, 12, BaseColor.BLACK, new BaseColor(198, 203, 209));
				PDFMaker.layoutPDFTabelCell(cell, 5, Element.ALIGN_MIDDLE, Element.ALIGN_CENTER, height);
				
				table.addCell(cell);
			}
			
			String [] title = {
				getLabel(4, 5, 1),
				getLabel(5, 0, 0),
				getLabel(5, 1, 0),
				getLabel(5, 2, 0),
				getLabel(5, 3, 0),
				getLabel(5, 4, 0),
				getLabel(5, 5, 0),
				getLabel(5, 6, 0)
			};
			String [] description = {
					getComboBoxContent(4, 1),
					getComboBoxContent(5, 1),
					getComboBoxContent(5, 2),
					getComboBoxContent(5, 3),
					"(" + getLabel(5, 4, 1) + ")",
					"(" + getLabel(5, 5, 1) + ")",
					getComboBoxContent(5, 6),
					getComboBoxContent(5, 7)
				};
			String [] level = {
					getData(4, 5, 1),
					getData(5, 0, 1),
					getData(5, 1, 1),
					getData(5, 2, 1),
					getData(5, 3, 1),
					getData(5, 4, 1),
					getData(5, 5, 1),
					getData(5, 6, 1)
				};
			FontFamily [] textFont = { FontFamily.UNDEFINED, FontFamily.UNDEFINED };
			float	[] textSize = { 12.0f, 8.0f };
			BaseColor [] textColor = { BaseColor.BLACK, BaseColor.BLACK };

			
			for(int i = 0; i < title.length; i++ )
			{
				BaseColor backColor = BaseColor.WHITE;
				if( i % 2 == 1 )
					backColor = new BaseColor(241, 241, 242);

				for(int j = 0; j < columnWidths.length; j++ )
				{
					PdfPCell cell = null;
					if( j == 0 )
					{
						String [] content = { title[i], description[i] };
						cell = PDFMaker.makePDFTabelCell(content, textFont, textSize, textColor, backColor );						
					}
					else if( j == 1 )
					{
						cell = PDFMaker.makePDFTabelCell(level[i], FontFamily.UNDEFINED, 12, BaseColor.BLACK, backColor );
					}
					else
					{
						cell = PDFMaker.makePDFTabelCell("", FontFamily.UNDEFINED, 12, BaseColor.BLACK, backColor );						
					}
					PDFMaker.layoutPDFTabelCell(cell, 5, Element.ALIGN_MIDDLE, height * 4 / 3);
					table.addCell(cell);
				}
			}
			document.add(table);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}	
	}
	
	private void addPage4(Document document, PdfWriter writer)
	{
		SWPCreatorUtils.createEmptyPage(document, writer, 0);
		
	
		String content [] = {
			getLabel(6, 0, 0),
			getLabel(6, 1, 0),
			getLabel(6, 2, 0)
		};
		for(int i = 0; i < content.length; i++ )
		{
			float space = 40;
			PDFMaker.addParagraph(document, content[i], space, 0, FontFamily.UNDEFINED, 12, BaseColor.BLACK);
			PDFMaker.addParagraphUnderline(document, getData(6, i, 1), 15, 0, FontFamily.UNDEFINED, 12, BaseColor.BLACK, 6 );			
		}
	}
	
}
