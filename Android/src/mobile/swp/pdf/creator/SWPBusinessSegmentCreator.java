package mobile.swp.pdf.creator;

import mobile.swp.choice.BusinessResponseManager;
import mobile.swp.choice.ChoiceManager;

import org.json.JSONArray;
import org.json.JSONObject;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import common.pdf.utils.PDFMaker;

public class SWPBusinessSegmentCreator extends SWPBaseCreator {
	int m_nCategory = 0;
	public SWPBusinessSegmentCreator(int category)
	{
		super();
		m_nCategory = category;
	}

	@Override
	public void createPages(Document document, PdfWriter writer) {
		addTitlePage(document, writer);
		addContentPage(document, writer);
	}

	@Override
	public void addTitlePage(Document document, PdfWriter writer) {
		super.addTitlePage(document, writer);
		if(m_nCategory == 0)
			SWPCreatorUtils.addSWPTitle(document, writer, "Business Strategy");
		if(m_nCategory == 1)
			SWPCreatorUtils.addSWPTitle(document, writer, "Segmentation Tools");
		
	}

	@Override
	public void addContentPage(Document document, PdfWriter writer) {
		if(m_nCategory == 0)
			addBusinessStrategyTablePage(document, writer);
		if(m_nCategory == 1)
			addSegmentationRolesTablePage(document, writer);

	}
	
	private void addBusinessStrategyTablePage(Document document, PdfWriter writer)
	{
		if( document == null || writer == null )
			return;
		
		SWPCreatorUtils.createEmptyPage(document, writer, 0);		
		SWPCreatorUtils.addTableTitle(document, writer, "Business Strategy");
		
		JSONObject data = ChoiceManager.getInstance().getChoiceInfo();		
		JSONObject business = data.optJSONObject("business");
		addBusinessSegmentTable(document, writer, business);
			
	}
	
	private void addSegmentationRolesTablePage(Document document, PdfWriter writer)
	{
		if( document == null || writer == null )
			return;
		
		SWPCreatorUtils.createEmptyPage(document, writer, 0);		
		SWPCreatorUtils.addTableTitle(document, writer, "Segmentation of Roles");
		
		JSONObject data = ChoiceManager.getInstance().getChoiceInfo();		
		JSONObject role = data.optJSONObject("role");
		addBusinessSegmentTable(document, writer, role);
	}
	
	private void addBusinessSegmentTable(Document document, PdfWriter writer, JSONObject data)
	{
		if( document == null || writer == null )
			return;	
		
		PdfPTable table = new PdfPTable(2); 
		table.setWidthPercentage(95.0f);
		table.setSpacingBefore(20f);
		table.setSpacingAfter(10f);
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		float[] columnWidths = {1, 1.8f};
		try {
			table.setWidths(columnWidths);
					
			addBusinessSegmentFirstRow(table, data);
			addBusinessSegmentContent(table, data, document.getPageSize());
			
			document.add(table);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}	
	}
	
	private void addBusinessSegmentFirstRow(PdfPTable table, JSONObject data)
	{
		if( table == null || data == null )
			return;	
		
		String fieldName = data.optString("title", "");
	
		PdfPCell cell1 = PDFMaker.makePDFTabelCell(fieldName, FontFamily.UNDEFINED, 15.0f, BaseColor.WHITE, SWPCreatorUtils.tableHeaderBackColor, 1.5f, SWPCreatorUtils.tableBorderColor);
		PDFMaker.layoutPDFTabelCell(cell1, 5, Element.ALIGN_MIDDLE, 0);
		
		PdfPCell cell2 = PDFMaker.makePDFTabelCell("Response", FontFamily.UNDEFINED, 15.0f, BaseColor.WHITE, SWPCreatorUtils.tableHeaderBackColor, 1.5f, SWPCreatorUtils.tableBorderColor);
		PDFMaker.layoutPDFTabelCell(cell2, 5, Element.ALIGN_MIDDLE, 0);
		
		table.addCell(cell1);
		table.addCell(cell2);

	}
	
	private void addBusinessSegmentContent(PdfPTable table, JSONObject data, Rectangle rect)
	{
		if( table == null || data == null )
			return;	
		
		
		JSONArray items = data.optJSONArray("items");
		if( items == null )
			return;
		
		float rowHeight = (rect.getHeight() - 250) / items.length();
		BusinessResponseManager manager = ChoiceManager.getInstance().getBusinessResponseManager();
		for(int i = 0; i < items.length(); i++)
		{
			String fieldName = items.optString(i, "");
			PdfPCell cell1 = PDFMaker.makePDFTabelCell(fieldName, FontFamily.UNDEFINED, 10.0f, new BaseColor(81, 92, 114), BaseColor.WHITE, 1.5f, SWPCreatorUtils.tableBorderColor);
			PDFMaker.layoutPDFTabelCell(cell1, 5, Element.ALIGN_TOP, rowHeight);
			PDFMaker.layoutPaddingPDFTableCell(cell1, 9, 13, 9, 13);
			
			String response = manager.getBusinessResponse(m_nCategory, i);
			PdfPCell cell2 = PDFMaker.makePDFTabelCell(response, FontFamily.UNDEFINED, 10.0f, BaseColor.BLACK, BaseColor.WHITE, 1.5f, SWPCreatorUtils.tableBorderColor);
			PDFMaker.layoutPDFTabelCell(cell2, 5, Element.ALIGN_TOP, rowHeight);
			PDFMaker.layoutPaddingPDFTableCell(cell2, 9, 13, 9, 13);
			
			table.addCell(cell1);
			table.addCell(cell2);
		}

	}	
}
