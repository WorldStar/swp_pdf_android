package mobile.swp.pdf.creator;

import java.util.ArrayList;

import mobile.swp.choice.ChoiceManager;
import mobile.swp.choice.EnvironmentalScanManager;

import org.json.JSONArray;
import org.json.JSONObject;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import common.pdf.utils.PDFMaker;

public class SWPEnvironmentScanCreator extends SWPBaseCreator {
	public SWPEnvironmentScanCreator()
	{
		super();
	}

	@Override
	public void createPages(Document document, PdfWriter writer) {
		addTitlePage(document, writer);
		addContentPage(document, writer);
	}

	@Override
	public void addTitlePage(Document document, PdfWriter writer) {
		super.addTitlePage(document, writer);
		SWPCreatorUtils.addSWPTitle(document, writer, "Environamental\r\nScan");
	}

	@Override
	public void addContentPage(Document document, PdfWriter writer) {
		super.addContentPage(document, writer);
		SWPCreatorUtils.addTableTitle(document, writer, "Environmental Scan");	
		addTable(document, writer);
	}

	private void addTable(Document document, PdfWriter writer)
	{
		JSONObject data = ChoiceManager.getInstance().getChoiceInfo();
		if( data == null )
			return;
		
		JSONObject environment = data.optJSONObject("environment");
		if( environment == null )
			return;
		
		int column		= environment.optInt("column", 2);
		JSONArray items = environment.optJSONArray("items");
		
		PdfPTable table = new PdfPTable(3); 
		table.setWidthPercentage(95.0f);
		table.setSpacingBefore(20f);
		table.setSpacingAfter(10f);
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		float[] columnWidths = {1.1f, 1, 0.125f};
		float height = 25.0f;
		try {
			table.setWidths(columnWidths);
			
			for(int i = 0; i < column; i++)
			{
				PdfPCell cell = new PdfPCell();
				PDFMaker.layoutPDFTabelCell(cell, 0, Element.ALIGN_MIDDLE, Element.ALIGN_MIDDLE, 0);
				addFirstRowCell(cell, items.optJSONObject(i), i);
				cell.setBorder(Rectangle.NO_BORDER);
				
				table.addCell(cell);
			}
			
			PdfPCell emptyCell = PDFMaker.makePDFTabelCell("", FontFamily.UNDEFINED, 10.0f);
			PDFMaker.layoutPDFTabelCell(emptyCell, 0, Element.ALIGN_CENTER, height);
			emptyCell.setBorder(Rectangle.NO_BORDER);
			table.addCell(emptyCell);
			
			for(int i = 0; i < column; i++)
			{
				PdfPCell cell = new PdfPCell();
				PDFMaker.layoutPDFTabelCell(cell, 0, Element.ALIGN_MIDDLE, Element.ALIGN_MIDDLE, 0);
				addCell(cell, items.optJSONObject(i), i);				
				
				table.addCell(cell);
			}

			PdfPCell externalCell = new PdfPCell();
			addRightSide(externalCell, "External", items.optJSONObject(column - 1));
			table.addCell(externalCell);	
			
			
			for(int i = column; i < 2 * column; i++)
			{
				PdfPCell cell = new PdfPCell();
				PDFMaker.layoutPDFTabelCell(cell, 0, Element.ALIGN_MIDDLE, Element.ALIGN_MIDDLE, 0);
				addCell(cell, items.optJSONObject(i), i);
				
				table.addCell(cell);
			}

			PdfPCell internalCell = new PdfPCell();
			addRightSide(internalCell, "Internal", items.optJSONObject(2 * column - 1));
			table.addCell(internalCell);			

			document.add(table);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}	
		
	}
	
	private void addFirstRowCell(PdfPCell cell, JSONObject data, int num)
	{
		PdfPTable table = new PdfPTable(1); 
		
		if( num % 2 == 0 )
			table.setWidthPercentage(100 / 1.1f);
		else
			table.setWidthPercentage(100);
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		String title = data.optString("title", "");
		
		PdfPCell titlecell = PDFMaker.makePDFTabelCell(title, FontFamily.UNDEFINED, 12.0f, BaseColor.WHITE, SWPCreatorUtils.tableHeaderBackColor);
		PDFMaker.layoutPDFTabelCell(titlecell, 5, Element.ALIGN_CENTER, Element.ALIGN_CENTER, 25);
	
		table.addCell(titlecell);
		
		cell.addElement(table);
	}
	
	private void addCell(PdfPCell cell, JSONObject data, int num)
	{
		float height = 25.0f;
		PdfPTable table = new PdfPTable(1); 
		if( num % 2 == 0 )
			table.setWidthPercentage(100 / 1.1f);
		else
			table.setWidthPercentage(100);
		
		EnvironmentalScanManager manager = ChoiceManager.getInstance().getEnvironmentalScanManager();
		ArrayList<String> contentList = manager.getEnvironmentalScan(num);
		
		table.setSpacingBefore(0.0f);
		table.setSpacingAfter(height);
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		String subtitle = data.optString("subtitle", "");
		int count = data.optInt("count", 9);
				
		PdfPCell subtitlecell = PDFMaker.makePDFTabelCell(subtitle, FontFamily.UNDEFINED, 12.0f, BaseColor.BLACK, new BaseColor(198, 203, 209));
		PDFMaker.layoutPDFTabelCell(subtitlecell, 5, Element.ALIGN_CENTER, Element.ALIGN_CENTER, height);

		table.addCell(subtitlecell);
		
		for(int i = 0; i < count; i++ )
		{
			String content = "";
			if( i < contentList.size() )
				content = contentList.get(i);
			PdfPCell contentcell = PDFMaker.makePDFTabelCell(content, FontFamily.UNDEFINED, 12.0f);
			PDFMaker.layoutPDFTabelCell(contentcell, 5, Element.ALIGN_MIDDLE, Element.ALIGN_MIDDLE, height);
			
			table.addCell(contentcell);
		}
		
		cell.addElement(table);
		cell.setBorder(Rectangle.NO_BORDER);
	}
	
	private void addRightSide(PdfPCell cell, String content, JSONObject data)
	{
		int count = data.optInt("count", 9);

		float height = 25.0f;
		PdfPTable table = new PdfPTable(1); 
		table.setWidthPercentage(100);
		table.setSpacingBefore(0.0f);
		table.setSpacingAfter(height);
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPCell subcell = PDFMaker.makePDFTabelCell(content, FontFamily.UNDEFINED, 12.0f, BaseColor.WHITE, SWPCreatorUtils.tableHeaderBackColor);
		PDFMaker.layoutPDFTabelCell(subcell, 0, Element.ALIGN_MIDDLE, Element.ALIGN_CENTER, height * (count + 1));
		subcell.setRotation(-90);
		table.addCell(subcell);
		
		cell.addElement(table);
		cell.setBorder(Rectangle.NO_BORDER);

	}
	
}
