package mobile.swp.pdf.creator;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import mobile.swp.choice.ChoiceManager;
import mobile.swp.constant.Const;

import org.json.JSONObject;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import common.library.utils.AndroidUtils;
import common.library.utils.FileUtils;


public class SWPDocumentCreator {
	
	private volatile static SWPDocumentCreator instance = null;
	private static final String	[]	PDF_ASSET_CHECKMARK_NAME = {"check_on_bg.png", "check_off_bg.png"};
	private static final String		PDF_ASSET_SWPMARK_NAME = "swp_mark.png";
	private static final String		PDF_ASSET_FOLDER_PATH = "pdf/";
	private static final String		PDF_SDCARD_TEMPLATE_PATH = "/SWP_Template";
	private static final String		PDF_BUSINESS_STRATEGY = "/business_strategy";
	private static final String		PDF_SEGMENT_ROLE = "/segment_role";
	private static final String		PDF_ENVIRONMENTAL_SCAN = "/environmental_scan";
	private static final String		PDF_CURRENT_STATE = "/current_state";
	private static final String		PDF_FUTURING = "/futuring";
	private static final String		PDF_GAP_ANALYSIS = "/gap_analysis";
	private static final String		PDF_ACTION_PLANNING = "/action_planning";
	private static final String		PDF_MONITOR_REPORT = "/monitor_report";
	
	Context		m_Context = null;
	String		m_strTemplatePDFPath = "";
	
	private int		m_nCreatePage = Const.BUSINESS_STRATEGY;
	
	private SWPDocumentCreator()
	{		
	}
	
	public static SWPDocumentCreator getInstance() {
		if (instance == null) {
			synchronized (SWPDocumentCreator.class) {
				if(instance == null)
					instance = new SWPDocumentCreator();
			}
			
		}
		
		return instance;
	}	
	
	public void setContext(Context context)
	{
		m_Context = context;
	}
	public int	copyPDFTemplateToExternalStorage()
	{
		if( m_Context == null )
			return -1;
		
       File tempLocation = null;

       // If we have external storage use it for the disk cache. Otherwise we use
       // the cache dir
       if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
    	   tempLocation = new File(
                   Environment.getExternalStorageDirectory() + PDF_SDCARD_TEMPLATE_PATH);
       } else {
    	   tempLocation = new File(m_Context.getFilesDir() + PDF_SDCARD_TEMPLATE_PATH);
       }
       tempLocation.mkdirs();
             
       for(int i = 0; i < PDF_ASSET_CHECKMARK_NAME.length; i++)
       {
    	   String checkMarkPath = "";
    	   checkMarkPath = m_Context.getFilesDir() + "/" + AndroidUtils.getVersionNumber(m_Context) + "_" + PDF_ASSET_CHECKMARK_NAME[i];
//    	   new File(checkMarkPath).delete();
           FileUtils.copyAssetFileToSDCard(m_Context, PDF_ASSET_FOLDER_PATH + PDF_ASSET_CHECKMARK_NAME[i], checkMarkPath);
           SWPCreatorUtils.loadSWPCheckMark(checkMarkPath, i);   	   
       }
	       
       String strSWPMarkPath = m_Context.getFilesDir() + "/" + AndroidUtils.getVersionNumber(m_Context) + "_" + PDF_ASSET_SWPMARK_NAME;
//	   new File(strSWPMarkPath).delete();
       FileUtils.copyAssetFileToSDCard(m_Context, PDF_ASSET_FOLDER_PATH + PDF_ASSET_SWPMARK_NAME, strSWPMarkPath);
       
       SWPCreatorUtils.loadSWPMark(strSWPMarkPath);
       
//       removeAllSavedFiles();
       
 		return 0;
	}
	
	public void removeAllSavedFiles()
	{
       for(int i = 0; i < 8; i++)
       {
    	   String path = getPDFSavePath(i);
    	   File [] fileList = new File(path).listFiles();
    	   for(int j = 0; j < fileList.length; j++ )
    	   {
    		   fileList[j].delete();
    	   }
       }
	}
	public int makeSWPModuleTemplate()
	{
		Document pdfDoc = new Document(PageSize.A4, 50f, 25f, 20.0f, 0.0f);
		
		String filePath = Environment.getExternalStorageDirectory() + PDF_SDCARD_TEMPLATE_PATH + "/" + "Head.pdf";
		
		File file = new File(filePath);
		file.delete();
        try {
			file.createNewFile();
			
			PdfWriter writer = PdfWriter.getInstance(pdfDoc,  new FileOutputStream(file.getAbsoluteFile()));
			pdfDoc.open();

			SWPCreatePage pageCreator1 = new SWPDefaultPageCreator();
			pageCreator1.createPages(pdfDoc, writer);
			
			SWPCreatePage pageCreator2 = new SWPBusinessSegmentCreator(0);
			pageCreator2.createPages(pdfDoc, writer);
			
			SWPCreatePage pageCreator3 = new SWPEnvironmentScanCreator();
			pageCreator3.createPages(pdfDoc, writer);

			SWPCreatePage pageCreator4 = new SWPEstimatingRiskCreator();
			pageCreator4.createPages(pdfDoc, writer);
			
			SWPCreatePage pageCreator5 = new SWPCurrentStateCreator(new JSONObject());
			pageCreator5.createPages(pdfDoc, writer);

			SWPCreatePage pageCreator6 = new SWPFuturingCreator(new JSONObject());
			pageCreator6.createPages(pdfDoc, writer);
			
			SWPCreatePage pageCreator7 = new SWPGapAnalysisCreator(new JSONObject());
			pageCreator7.createPages(pdfDoc, writer);
			
			pdfDoc.close();

            Log.d("Suceess", "Sucess");
            
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
        
		  
		 
		return 0;
	}
	
	public void setCreatePage(int page)
	{
		m_nCreatePage = page;
	}
	
	public int getCreatePage()
	{
		return m_nCreatePage;
	}
	
	public boolean makeBusinessStrategyDoc(String name, int page)
	{
		Document pdfDoc = new Document(PageSize.A4, 50f, 25f, 20.0f, 0.0f);
		
		String folderPath = getPDFSavePath(page);
		
		File file = new File(folderPath, name + ".pdf");
		if( file.exists() )
			return false;
		
		file.delete();
        try {
			file.createNewFile();
			
			PdfWriter writer = PdfWriter.getInstance(pdfDoc,  new FileOutputStream(file.getAbsoluteFile()));
			pdfDoc.open();

			SWPCreatePage pageCreator1 = new SWPDefaultPageCreator();
			pageCreator1.createPages(pdfDoc, writer);
			
			SWPCreatePage pageCreator2 = new SWPBusinessSegmentCreator(page);
			pageCreator2.createPages(pdfDoc, writer);
			
			pdfDoc.close();

            Log.d("Suceess", "Sucess");
            
            return true;
            
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
        
		  
		 
		return false;
	}
	
	public boolean makeEnvironmentalScanDoc(String name)
	{
		Document pdfDoc = new Document(PageSize.A4, 50f, 25f, 20.0f, 0.0f);
		
		String folderPath = getPDFSavePath(Const.ENVIRONMENTAL_SCAN);
		
		File file = new File(folderPath, name + ".pdf");
		if( file.exists() )
			return false;
        try {
			file.createNewFile();
			
			PdfWriter writer = PdfWriter.getInstance(pdfDoc,  new FileOutputStream(file.getAbsoluteFile()));
			pdfDoc.open();

			SWPCreatePage pageCreator1 = new SWPDefaultPageCreator();
			pageCreator1.createPages(pdfDoc, writer);
			
			SWPCreatePage pageCreator2 = new SWPEnvironmentScanCreator();
			pageCreator2.createPages(pdfDoc, writer);
			
			SWPCreatePage pageCreator3 = new SWPEstimatingRiskCreator();
			pageCreator3.createPages(pdfDoc, writer);	
			
			pdfDoc.close();

            Log.d("Suceess", "Sucess");
            
            return true;
            
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
        
		  
		 
		return false;
	}
	
	public boolean makeCurrentStateFuturingDoc(String name)
	{
		Document pdfDoc = new Document(PageSize.A4, 50f, 25f, 20.0f, 0.0f);
		
		String folderPath = getPDFSavePath(m_nCreatePage);
		
		File file = new File(folderPath, name + ".pdf");
		if( file.exists() )
			return false;
        try {
			file.createNewFile();
			
			PdfWriter writer = PdfWriter.getInstance(pdfDoc,  new FileOutputStream(file.getAbsoluteFile()));
			pdfDoc.open();

			SWPCreatePage pageCreator1 = new SWPDefaultPageCreator();
			pageCreator1.createPages(pdfDoc, writer);
			
			JSONObject data = ChoiceManager.getInstance().getCurrentStateManager().getData();
			
			if( m_nCreatePage == Const.CURRENT_STATE )
			{
				SWPCreatePage pageCreator2 = new SWPCurrentStateCreator(data);
				pageCreator2.createPages(pdfDoc, writer);				
			}
			
			if( m_nCreatePage == Const.FUTURING )
			{
				SWPCreatePage pageCreator2 = new SWPFuturingCreator(data);
				pageCreator2.createPages(pdfDoc, writer);	
			}
			
			pdfDoc.close();

            Log.d("Suceess", "Sucess");
            
            return true;
            
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
      
        
		  
		 
		return false;
	}
	
	public boolean makeGapAnalysisDoc(String name)
	{
		Document pdfDoc = new Document(PageSize.A4, 50f, 25f, 20.0f, 0.0f);
		
		String folderPath = getPDFSavePath(Const.GAP_ANALYSIS);
		
		File file = new File(folderPath, name + ".pdf");
		if( file.exists() )
			return false;
        try {
			file.createNewFile();
			
			PdfWriter writer = PdfWriter.getInstance(pdfDoc,  new FileOutputStream(file.getAbsoluteFile()));
			pdfDoc.open();

			SWPCreatePage pageCreator1 = new SWPDefaultPageCreator();
			pageCreator1.createPages(pdfDoc, writer);
			
			JSONObject data = ChoiceManager.getInstance().getCurrentStateManager().getData();
			SWPCreatePage pageCreator2 = new SWPGapAnalysisCreator(data);
			pageCreator2.createPages(pdfDoc, writer);
			
			pdfDoc.close();

            Log.d("Suceess", "Sucess");
            
            return true;
            
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
        
		  
		 
		return false;
	}
	
	public boolean makeActionPlnningDoc(String name)
	{
		Document pdfDoc = new Document(PageSize.A4, 50f, 25f, 20.0f, 0.0f);
		
		String folderPath = getPDFSavePath(Const.ACTION_PLANNING);
		
		File file = new File(folderPath, name + ".pdf");
		if( file.exists() )
			return false;
        try {
			file.createNewFile();
			
			PdfWriter writer = PdfWriter.getInstance(pdfDoc,  new FileOutputStream(file.getAbsoluteFile()));
			pdfDoc.open();

			SWPCreatePage pageCreator1 = new SWPDefaultPageCreator();
			pageCreator1.createPages(pdfDoc, writer);
			
			JSONObject data = ChoiceManager.getInstance().getCurrentStateManager().getData();
			SWPCreatePage pageCreator2 = new SWPActionPlanningCreator(data);
			pageCreator2.createPages(pdfDoc, writer);
			
			pdfDoc.close();

            Log.d("Suceess", "Sucess");
            
            return true;
            
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
        
		  
		 
		return false;
	}
	
	public boolean makeMonitorReportDoc(String name)
	{
		Document pdfDoc = new Document(PageSize.A4, 50f, 25f, 20.0f, 0.0f);
		
		String folderPath = getPDFSavePath(Const.MONITOR_REPORT);
		
		File file = new File(folderPath, name + ".pdf");
		if( file.exists() )
			return false;
        try {
			file.createNewFile();
			
			PdfWriter writer = PdfWriter.getInstance(pdfDoc,  new FileOutputStream(file.getAbsoluteFile()));
			pdfDoc.open();

			SWPCreatePage pageCreator1 = new SWPDefaultPageCreator();
			pageCreator1.createPages(pdfDoc, writer);
			
			JSONObject data = ChoiceManager.getInstance().getCurrentStateManager().getData();
			SWPCreatePage pageCreator2 = new SWPMonitorReportCreator(data);
			pageCreator2.createPages(pdfDoc, writer);
			
			pdfDoc.close();

            Log.d("Suceess", "Sucess");
            
            return true;
            
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
        
		  
		 
		return false;
	}
	
	
	public String getPDFSavePath(int page)
	{
		 String path = "";
		 switch( page )
		 {
		 case Const.BUSINESS_STRATEGY:
			 path = PDF_BUSINESS_STRATEGY;
			 break;
		 case Const.SEGMANET_ROLE:
			 path = PDF_SEGMENT_ROLE;
			 break;
		 case Const.ENVIRONMENTAL_SCAN:
			 path = PDF_ENVIRONMENTAL_SCAN;
			 break;
		 case Const.CURRENT_STATE:
			 path = PDF_CURRENT_STATE;
			 break;
		 case Const.FUTURING:
			 path = PDF_FUTURING;
			 break;
		 case Const.GAP_ANALYSIS:
			 path = PDF_GAP_ANALYSIS;
			 break;
		 case Const.ACTION_PLANNING:
			 path = PDF_ACTION_PLANNING;
			 break;
		 case Const.MONITOR_REPORT:
			 path = PDF_MONITOR_REPORT;
			 break;
			 
		 }
		 File localtion = null;
         if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
        	 localtion = new File(
                   Environment.getExternalStorageDirectory() + PDF_SDCARD_TEMPLATE_PATH + path);
         } else {
        	 localtion = new File(m_Context.getFilesDir() + PDF_SDCARD_TEMPLATE_PATH + path);
         }
         localtion.mkdirs();
         
         return localtion.getAbsolutePath();
	}

	
}
