package mobile.swp.pdf.creator;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;

public interface SWPCreatePage {
	public void createPages(Document document, PdfWriter writer);
	public void addTitlePage(Document document, PdfWriter writer);
	public void addContentPage(Document document, PdfWriter writer);
}
