package mobile.swp.pdf.creator;

import org.json.JSONArray;
import org.json.JSONObject;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import common.pdf.utils.PDFMaker;

public class SWPActionPlanningCreator extends SWPBaseCreator {
	public SWPActionPlanningCreator(JSONObject data)
	{
		super();
		m_Data = data;
		m_nBasePageNum = 0;
	}

	@Override
	public void createPages(Document document, PdfWriter writer) {
		addTitlePage(document, writer);
		addContentPage(document, writer);
	}

	@Override
	public void addTitlePage(Document document, PdfWriter writer) {
		super.addTitlePage(document, writer);
		SWPCreatorUtils.addSWPTitle(document, writer, "Action Planning");
	}

	@Override
	public void addContentPage(Document document, PdfWriter writer) {
		addPage(document, writer);
	}
	

	
	private void addPage(Document document, PdfWriter writer)
	{
		SWPCreatorUtils.createEmptyPage(document, writer, 0);
		SWPCreatorUtils.addTableTitle(document, writer, getPageTitle(13));	
		PDFMaker.addParagraph(document, getLabel(13, 0, 0) + " " + getData(13, 0, 1), 10, 0, FontFamily.UNDEFINED, 12, BaseColor.BLACK);
		PDFMaker.addParagraph(document, getLabel(13, 1, 0) + " " + getData(13, 1, 1), 10, 0, FontFamily.UNDEFINED, 12, BaseColor.BLACK);
		addTable(document, writer);
	}
	
	private void addTable(Document document, PdfWriter writer)
	{
		float[] columnWidths = {0.85f, 1, 1, 1, 1};

		PdfPTable table = new PdfPTable(columnWidths.length); 
		table.setWidthPercentage(100.0f);
		table.setSpacingBefore(10f);
		table.setSpacingAfter(10f);
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		float height = document.getPageSize().getWidth() / (7 + 1);
		
		JSONObject data = getData(13);
		if( data == null )
			return;
		
		JSONArray  list = data.optJSONArray("grid_data");
		if( list == null )
			return;
		
		try {
			table.setWidths(columnWidths);
			
			// Table Header
			for(int i = 0; i < columnWidths.length; i++ )
			{
				PdfPCell cell = PDFMaker.makePDFTabelCell(list.optString(i, ""), FontFamily.UNDEFINED, 12.0f, BaseColor.WHITE, SWPCreatorUtils.tableHeaderBackColor );
				PDFMaker.layoutPDFTabelCell(cell, 5, Element.ALIGN_MIDDLE, Element.ALIGN_CENTER, 50);
				table.addCell(cell);					
			}
			
			for(int i = 0; i < 7; i++ )
			{
				for(int j = 0; j < columnWidths.length; j++ )
				{
					PdfPCell cell = PDFMaker.makePDFTabelCell(list.optString((i + 1) * columnWidths.length + j, ""), FontFamily.UNDEFINED, 10.0f);
					PDFMaker.layoutPDFTabelCell(cell, 5, Element.ALIGN_TOP, height);
					
					table.addCell(cell);					
				}
			}
			document.add(table);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}	
	}
	
	
}
