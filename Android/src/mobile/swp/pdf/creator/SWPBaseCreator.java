package mobile.swp.pdf.creator;

import mobile.swp.mvp.CurrentStatePresenter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import common.library.utils.DataUtils;

public class SWPBaseCreator implements SWPCreatePage {
	JSONObject m_Data = new JSONObject();
	int 		m_nBasePageNum = 5;

	@Override
	public void createPages(Document document, PdfWriter writer) {
		
	}

	@Override
	public void addTitlePage(Document document, PdfWriter writer) {
		SWPCreatorUtils.createEmptyPage(document, writer, 0);
	}

	@Override
	public void addContentPage(Document document, PdfWriter writer) {
		SWPCreatorUtils.createEmptyPage(document, writer, 0);

	}	
	
	protected String getData(int pageNum, int rowNum, int colNum )
	{
		String result =  "";
		JSONObject data = getData(m_nBasePageNum + pageNum);
		if( data == null )
			return result;
		
		result = data.optString("value_" + rowNum + "_" + colNum, "");
		
		return result;
	}
	
	protected String getDataFlag(int pageNum, int rowNum, int colNum )
	{
		String data = getData(pageNum, rowNum, colNum);
		if( data.equals("1"))
			return "Yes";
		
		return "No";
	}
	
	protected JSONObject getData(int pageNum)
	{
		String temp = DataUtils.getPreference(CurrentStatePresenter.CURRENT_STATE_KEY + pageNum, "");
		JSONObject data = new JSONObject();
		try {
			data = new JSONObject(temp);
		} catch (JSONException e) {
		}
		
		return data;
	}
	
	protected String getPageTitle(int pageNum)
	{
		String title = "";
		JSONObject page = m_Data.optJSONObject("page" + (m_nBasePageNum + pageNum));
		if( page == null )
			return title;
		
		title = page.optString("title", "");
		
		return title;
	}
	
	protected String getComboBoxTitle(int pageNum, int num)
	{
		String title = "";
		JSONObject page = m_Data.optJSONObject("page" + (m_nBasePageNum + pageNum));
		if( page == null )
			return title;
		
		title = page.optString("combo_" + num + "_title", "");
		
		return title;
	}
	
	protected String getComboBoxContent(int pageNum, int num)
	{
		String content = "";
		JSONObject page = m_Data.optJSONObject("page" + (m_nBasePageNum + pageNum));
		if( page == null )
			return content;
		
		JSONArray items = page.optJSONArray("combo_" + num);
		if( items == null )
			return content;
		
		content = "(";
		for(int i = 0; i < items.length(); i++ )
		{
			content += items.optString(i, "");
			if( i < items.length() - 1 )
				content += ", ";
		}
		
		content += ")";
		
		return content;
	}
	
	protected String getComboBoxItem(int pageNum, int num, int subnum)
	{
		String content = "";
		JSONObject page = m_Data.optJSONObject("page" + (m_nBasePageNum + pageNum));
		if( page == null )
			return content;
		
		JSONArray items = page.optJSONArray("combo_" + num);
		if( items == null )
			return content;
		
		int type = page.optInt("combo_" + num + "_type", 0);
		if( type == 0 )
		{
			content = items.optString(subnum, "");
		}
		if( type == 1 )
		{
			int start = items.optInt(0, 0);
			content = "" + (start + subnum);
		}
		
		return content;
	}
	
	protected String getLabel(int pageNum, int rowNum, int colNum)
	{
		String label = "";
		JSONObject page = m_Data.optJSONObject("page" + (m_nBasePageNum + pageNum));
		if( page == null )
			return label;
	
		JSONArray  items = page.optJSONArray("items");
		if( items == null )
			return label;
		
		JSONArray  subitems = items.optJSONArray(rowNum);
		if( subitems == null )
			return label;
		
		label = subitems.optString(colNum, "");
		return label;
	}

	

}
