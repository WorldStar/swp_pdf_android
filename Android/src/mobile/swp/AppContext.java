package mobile.swp;

import mobile.swp.activity.FileListActivity;
import mobile.swp.activity.SWPHomeActivity;
import mobile.swp.constant.Const;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import common.manager.activity.ActivityManager;

public class AppContext {
	public static void gotoMenuPage(Activity context, int menuNum)
	{
		if( context == null )
			return;
		
		if( menuNum == 0 )	// Home Page
		{
			boolean isExist = ActivityManager.getInstance().popAllActivityExceptOne(SWPHomeActivity.class);
			if( isExist == false )
			{
				Bundle bundle = new Bundle();
				bundle.putInt(Const.PAGE_NAME, menuNum);
				ActivityManager.changeActivity(context, SWPHomeActivity.class, bundle, true, null );			
			}
			
			return;
		}		
		
		if( menuNum < 1 || menuNum > 8 )
			return;
		
		boolean isTopFileListActivity = false;
		Activity activity = ActivityManager.getInstance().currentActivity();
		if( activity != null && activity instanceof FileListActivity )
			isTopFileListActivity = true;
		
		boolean isExist = ActivityManager.getInstance().popAllActivityExceptOne(FileListActivity.class);
		
		if( isExist == false )	// Dead Code
		{				
			Log.e("activity_stack", "filelist is no");
			activity = ActivityManager.getInstance().currentActivity();
			if( activity == null )
				return;
			
			if( activity instanceof SWPHomeActivity == false )
				return;
			
			SWPHomeActivity home = (SWPHomeActivity) activity;
			
			int [] btn_category_bg_resourceid = {
					R.id.btn_business_strategy,
					R.id.btn_segment_roles,
					R.id.btn_environmental_scan,
					R.id.btn_current_state_analysis,
					R.id.btn_futuring,
					R.id.btn_gap_analysis,
					R.id.btn_action_planning,
					R.id.btn_monitor_report
				};
			home.gotoOtherPage(btn_category_bg_resourceid[menuNum - 1]);
		}
		else
		{			
			activity = ActivityManager.getInstance().currentActivity();
			if( activity == null )
				return;
			
			if( activity instanceof FileListActivity == false )
				return;
			
			FileListActivity filelist = (FileListActivity) activity;
			filelist.refreshFileList(menuNum, isTopFileListActivity);
		}

	}
}
