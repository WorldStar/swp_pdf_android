package mobile.swp.dialogs;

import java.util.ArrayList;
import java.util.List;

import mobile.swp.R;
import mobile.swp.adapters.ItemListAdapter;
import mobile.swp.component.dialog.CommonDialog;
import mobile.swp.component.dialog.DialogFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.TypedValue;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;

import common.design.layout.LayoutUtils;
import common.design.layout.ScreenAdapter;
import common.list.adapter.ItemCallBack;
import common.list.adapter.ItemResult;

public class SelectChoiceDialog extends CommonDialog {
	
	TextView	m_txtDialogTitle = null;
	ListView	m_listItems = null;
	
	public ItemListAdapter 		m_adapterItemList = null;
	
	public SelectChoiceDialog(Context context, int layoutResID, JSONObject param, ItemCallBack callback) {
		super(context, layoutResID, param, callback);
	}
	
	@Override
	protected void findViews() 
	{
		m_txtDialogTitle = (TextView) findViewById(R.id.txt_title);
		m_listItems = (ListView) findViewById(R.id.listItems);
	}
	
	@Override
	protected void layoutDialog()
	{
		super.layoutDialog();
		
		LayoutUtils.setSize(m_txtDialogTitle, LayoutParams.FILL_PARENT, 177, true);
		m_txtDialogTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(40));
		LayoutUtils.setMargin(m_txtDialogTitle, 0, 0, 0, 0, true);		
	}
	
	@Override
	protected void initDialogData()
	{
		JSONObject dialogData = m_Param.optJSONObject(DIALOG_DATA);
		m_txtDialogTitle.setText(dialogData.optString(DialogFactory.DIALOG_TITLE, ""));
		initItmeListData();
	}

	private List<JSONObject> getItemList()
	{
		List<JSONObject> itemlist = new ArrayList<JSONObject>();
		
		JSONObject dialogData = m_Param.optJSONObject(DIALOG_DATA);
		JSONObject data = dialogData.optJSONObject(DialogFactory.ITEMS);
		JSONArray items = data.optJSONArray("items");
		// TODO
		for(int i = 0; i < items.length(); i++ )
		{
			JSONObject listinfo = new JSONObject();			
			JSONObject item = items.optJSONObject(i);
			try {
				listinfo.put(ItemListAdapter.ITEM_NAME, item.optString("subtitle", ""));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			itemlist.add(listinfo);
		}
		
		return itemlist;
	}
	
	private void initItmeListData()
	{
		List<JSONObject> itemlist = getItemList();
		
		JSONObject dialogData = m_Param.optJSONObject(DIALOG_DATA);
		int selNum = dialogData.optInt(DialogFactory.SEL_NUM, 0);
		m_adapterItemList = new ItemListAdapter(getContext(), itemlist, R.layout.fragment_label_center, selNum, 40, 177, new ItemCallBack() {
			
			@Override
			public void doClick(ItemResult result) {
				if( SelectChoiceDialog.this.m_Callback != null )
				{
					SelectChoiceDialog.this.m_Callback.doClick(result);
				}
				hideDialog();
			}
		});
		
		m_listItems.setAdapter(m_adapterItemList);
	}
	
	
}
