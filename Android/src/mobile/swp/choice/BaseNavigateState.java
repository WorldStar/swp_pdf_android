package mobile.swp.choice;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

public class BaseNavigateState implements NavigateState {
	protected JSONObject m_Data = null;
	
	protected int m_nSelectNum = 0;
	protected int m_nTotalCount = 0;
	
	public BaseNavigateState(JSONObject data)
	{
		m_Data = data;
		if( data != null )
		{
			JSONArray items = data.optJSONArray("items");
			if(items != null)
			{
				m_nTotalCount = items.length();
			}
		}
	}
	
	@Override
	public JSONObject getData() {
		return m_Data;
	}
	
	@Override
	public boolean nextPage() {
		if( m_Data == null )
			return false;
		
		m_nSelectNum++;
		if( m_nSelectNum >= m_nTotalCount )
		{
			m_nSelectNum = m_nTotalCount - 1;
			Log.e("state", "" + m_nSelectNum);
			return false;	
		}
		
		Log.e("state", "" + m_nSelectNum);

		return true;
	}

	@Override
	public boolean prevPage() {
		if( m_Data == null )
			return false;
		
		m_nSelectNum--;
		if( m_nSelectNum < 0 )
		{
			m_nSelectNum = 0;
			Log.e("state", "" + m_nSelectNum);
			return false;	
		}
		
		Log.e("state", "" + m_nSelectNum);
		
		return true;
	}

	@Override
	public void setFirstState() {
		m_nSelectNum = 0;		
	}
	
	public boolean isFirstStage()
	{
		return (m_nSelectNum == 0);
	}

	@Override
	public String getTitle() {
		String title =  "";		
		
		return title;
	}

	@Override
	public String getDescription() {
		String description = "";
		
		return description;
	}

	@Override
	public int getCurrentNum() {
		return m_nSelectNum;
	}

	@Override
	public boolean isLastStage() {
		return (m_nSelectNum == (m_nTotalCount - 1));
	}

	@Override
	public int getTotalCount() {
		return m_nTotalCount;
	}


	
}
