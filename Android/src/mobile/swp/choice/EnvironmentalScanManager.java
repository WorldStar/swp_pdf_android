package mobile.swp.choice;

import java.util.ArrayList;

import org.json.JSONObject;

import common.library.utils.DataUtils;

public class EnvironmentalScanManager {
		
	NavigateState		m_EnvironmentalScanState = null;

	public void setData(JSONObject data)
	{
		m_EnvironmentalScanState = new EnvironmentalScanState(data);
	}
	
	public JSONObject getData()
	{
		return m_EnvironmentalScanState.getData();
	}
	
	private void eraseSavedValue()
	{
		int totalCount = m_EnvironmentalScanState.getTotalCount();
		
		for(int i = 0; i < totalCount; i++ )
		{
			DataUtils.savePreference(getCountKey(i), "0");
		}	
	}
	
	public void setFirstStage()
	{
		m_EnvironmentalScanState.setFirstState();
		eraseSavedValue();
	}
	
	public boolean isFirstStage()
	{
		return m_EnvironmentalScanState.isFirstStage();
	}
	
	
	public String getTitle()
	{
		return m_EnvironmentalScanState.getTitle();
	}
	
	public String getDescription()
	{
		return m_EnvironmentalScanState.getDescription();
	}
	
	public boolean prevStage()
	{		
		return m_EnvironmentalScanState.prevPage();
	}
	
	public boolean nextStage()
	{		
		return m_EnvironmentalScanState.nextPage();
	}
	
	private String getCountKey(int num)
	{
		return "environmental_scan_count" + num;
	}
	
	private String getContentKey(int num, int subnum)
	{
		return "environmental_scan_" + num + "_" + subnum;
	}

	public void saveEnvironmentalScan(ArrayList<String> data)
	{
		if(data == null)
			return;
		
		DataUtils.savePreference(getCountKey(m_EnvironmentalScanState.getCurrentNum()), data.size());
		for(int i = 0; i < data.size(); i++)
		{
			String content = data.get(i);
			DataUtils.savePreference(getContentKey(m_EnvironmentalScanState.getCurrentNum(), i), content);
		}
	}
	
	public ArrayList<String> getEnvironmentalScan(int num)
	{
		ArrayList<String> data = new ArrayList<String>();
		
		int count = DataUtils.getPreference(getCountKey(num), 0);
		for(int i = 0; i < count; i++)
		{
			String content = DataUtils.getPreference(getContentKey(num, i), "");
			data.add(content);
		}
		return data;
	}
	
	public ArrayList<String> getEnvironmentalScan()
	{
		return getEnvironmentalScan(m_EnvironmentalScanState.getCurrentNum());
	}
}





