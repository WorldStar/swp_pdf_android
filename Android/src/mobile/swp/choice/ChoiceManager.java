package mobile.swp.choice;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;

import common.library.utils.DataUtils;
import common.library.utils.ParsingUtils;




public class ChoiceManager {
	private volatile static ChoiceManager instance;
	Context		m_Context = null;
	
	private JSONObject	m_Choice = null;
	
	private int			m_nChoiceCatory = 0;
	private int			m_nStageNum = 0;
	
	private BusinessResponseManager 	m_BusinessResponderMng = null;
	private EnvironmentalScanManager	m_EnvironmentalScanMng = null;
	private CurrentStateManager			m_CurrentStateManager = null;
	
	public static ChoiceManager getInstance() {
		if (instance == null) {
			synchronized (ChoiceManager.class) {
				if(instance == null)
					instance = new ChoiceManager();
			}
			
		}
		
		return instance;
	}	
	public void setContext(Context context)
	{
		m_Context = context;
		
		m_Choice = ParsingUtils.loadJSONObjectFromAsset(m_Context, "data/property.json");
		
		m_BusinessResponderMng = new BusinessResponseManager();
		m_EnvironmentalScanMng = new EnvironmentalScanManager();
		m_EnvironmentalScanMng.setData(m_Choice.optJSONObject("environment"));
		
		m_CurrentStateManager = new CurrentStateManager();
		m_CurrentStateManager.setData(ParsingUtils.loadJSONObjectFromAsset(context, "data/current_state.json"));
	}
	
	public BusinessResponseManager getBusinessResponseManager()
	{
		return m_BusinessResponderMng;
	}
	
	public EnvironmentalScanManager getEnvironmentalScanManager()
	{
		return m_EnvironmentalScanMng;
	}
	
	public CurrentStateManager getCurrentStateManager()
	{
		return m_CurrentStateManager;
	}
	
	public JSONObject getChoiceInfo()
	{
		return m_Choice;
	}
	
	public void setFirstStage()
	{
		m_nChoiceCatory = 0;
		m_nStageNum = 0;
	}
	
	public boolean isFirstStage()
	{
		return ((m_nChoiceCatory == 0) && (m_nStageNum == 0 ));
	}
	
	public boolean prevStage()
	{
		if( m_Choice == null )
			return false;
		
		if( m_nChoiceCatory == 0 && m_nStageNum <= 0 )
			return false;
		
		JSONObject choice1 = m_Choice.optJSONObject("choice1");
		JSONArray  item1 = choice1.optJSONArray("items");
		
		m_nStageNum--;
		if( m_nChoiceCatory == 1 && m_nStageNum < 0 )
		{
			m_nChoiceCatory--;
			m_nStageNum = item1.length() - 1;
		}
		
		return true;
	}
	
	public boolean nextStage()
	{
		if( m_Choice == null )
			return false;
		
		JSONObject choice1 = m_Choice.optJSONObject("choice1");
		JSONArray  item1 = choice1.optJSONArray("items");

		JSONObject choice2 = m_Choice.optJSONObject("choice2");
		JSONArray  item2 = choice2.optJSONArray("items");
		
		if( m_nChoiceCatory >= 1 && m_nStageNum >= item2.length() - 1 )
			return false;
		
		m_nStageNum++;
		if( m_nChoiceCatory == 0 && m_nStageNum >= item1.length() )
		{
			m_nChoiceCatory++;
			m_nStageNum = 0;
		}
			
		
		return true;
	}
	

	
	private String getChoiceLevelKey(int category, int num)
	{
		return "choice_" + category + "_" + num;
	}
	
	public void setChoiceLevel(int num)
	{
		DataUtils.savePreference(getChoiceLevelKey(m_nChoiceCatory, m_nStageNum), num);
	}
	
	public int getChoiceLevel(int category, int num)
	{		
		return DataUtils.getPreference(getChoiceLevelKey(category, num), -1);
	}
	
	public int getChoiceLevel()
	{		
		return DataUtils.getPreference(getChoiceLevelKey(m_nChoiceCatory, m_nStageNum), -1);
	}
	
	public boolean isCompletedChoiceSelection()
	{
		JSONObject choice1 = m_Choice.optJSONObject("choice1");
		JSONArray  item1 = choice1.optJSONArray("items");
		
		JSONObject choice2 = m_Choice.optJSONObject("choice2");
		JSONArray  item2 = choice2.optJSONArray("items");
		
		for(int i = 0; i < item1.length(); i++ )
		{
//			// Test
//			DataUtils.savePreference(getChoiceLevelKey(1, i), i % 5);
			
			int level = getChoiceLevel(0, i);
			if( level < 0 || level >= 5 )
				return false;
		}
		
		for(int i = 0; i < item2.length(); i++ )
		{
//			// Test
//			DataUtils.savePreference(getChoiceLevelKey(2, i), i % 5);
			
			int level = getChoiceLevel(1, i );
			if( level < 0 || level >= 5 )
				return false;
		}
		
		return true;				
	}
	
	public String getChoiceTitle()
	{
		String title = "";
		
		JSONObject choice = null;
		if( m_nChoiceCatory == 0 ) 
			choice = m_Choice.optJSONObject("choice1");
		if( m_nChoiceCatory == 1 ) 
			choice = m_Choice.optJSONObject("choice2");
		title = choice.optString("title", "");		
		
		return title;
	}
	
	public String getChoiceDescription()
	{
		String description = "";
		
		JSONObject choice = null;
		if( m_nChoiceCatory == 0 ) 
			choice = m_Choice.optJSONObject("choice1");
		if( m_nChoiceCatory == 1 ) 
			choice = m_Choice.optJSONObject("choice2");
		
		JSONArray  item = choice.optJSONArray("items");
		if( m_nStageNum < 0 || m_nStageNum >= item.length() )
			return description;
		
		description = item.optString(m_nStageNum, "");
		
		return description;
	}
	

	
	
	

}





