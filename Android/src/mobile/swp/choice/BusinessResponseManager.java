package mobile.swp.choice;

import org.json.JSONObject;

import common.library.utils.DataUtils;

public class BusinessResponseManager {
	
	NavigateState		m_BusinessResponseState = null;
	int					m_nPage = 0;

	
	public void setData(JSONObject data)
	{
		m_nPage = data.optInt("num", 0);
		m_BusinessResponseState = new BusinessResponseState(data);
	}
	
	private void eraseSavedValue()
	{
		int totalCount = m_BusinessResponseState.getTotalCount();
		
		for(int i = 0; i < totalCount; i++ )
		{
			DataUtils.savePreference(getKey(m_nPage, i), "");
		}
	}
	public void setFirstStage()
	{
		m_BusinessResponseState.setFirstState();
		eraseSavedValue();
	}
	
	public boolean isFirstStage()
	{
		return m_BusinessResponseState.isFirstStage();
	}
	
	public boolean isLastStage()
	{
		return m_BusinessResponseState.isLastStage();
	}
	
	public String getTitle()
	{
		return m_BusinessResponseState.getTitle();
	}
	
	public String getDescription()
	{
		return m_BusinessResponseState.getDescription();
	}
	
	public boolean prevStage()
	{
		
		return m_BusinessResponseState.prevPage();
	}
	
	public boolean nextStage()
	{		
		return m_BusinessResponseState.nextPage();
	}

	private String getKey(int page, int num)
	{
		return "business_segment_" + m_nPage + "_" + num;
	}
	
	public void saveBusinessResponse(int num, String response)
	{
		DataUtils.savePreference(getKey(m_nPage, num), response);
	}
	

	
	public void saveBusinessResponse(String response)
	{
		saveBusinessResponse(m_BusinessResponseState.getCurrentNum(), response);
	}
	
	public String getBusinessResponse(int page, int num)
	{
		return DataUtils.getPreference(getKey(page, num), "");
	}
	
	public String getBusinessResponse()
	{
		return DataUtils.getPreference(getKey(m_nPage, m_BusinessResponseState.getCurrentNum()), "");
	}
	
	public int getPageNum()
	{
		return m_nPage;
	}
}





