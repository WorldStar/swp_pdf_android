package mobile.swp.choice;

import org.json.JSONObject;

public interface NavigateState {
	public boolean nextPage();
	public boolean prevPage();
	public void    setFirstState();
	public boolean isFirstStage();
	public boolean isLastStage();
	public String	getTitle();
	public String	getDescription();
	public int		getCurrentNum();
	public JSONObject getData();
	public int		getTotalCount();
}
