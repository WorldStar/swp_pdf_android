package mobile.swp.choice;

import org.json.JSONArray;
import org.json.JSONObject;

public class BusinessResponseState extends BaseNavigateState implements NavigateState {

	public BusinessResponseState(JSONObject data)
	{
		super(data);
	}
	
	@Override
	public String getTitle() {
		String title =  m_Data.optString("title", "");		
		
		return title;
	}

	@Override
	public String getDescription() {
		String description = "";
		JSONArray  item = m_Data.optJSONArray("items");
		if( m_nSelectNum < 0 || m_nSelectNum >= m_nTotalCount )
			return description;
		
		description = item.optString(m_nSelectNum, "");
		
		return description;
	}

	
}
