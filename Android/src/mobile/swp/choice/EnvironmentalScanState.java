package mobile.swp.choice;

import org.json.JSONArray;
import org.json.JSONObject;

public class EnvironmentalScanState extends BaseNavigateState implements NavigateState {

	public EnvironmentalScanState(JSONObject data)
	{
		super(data);
	}
	
	@Override
	public String getTitle() {
		String title =  "";		
		JSONArray  item = m_Data.optJSONArray("items");
		if( m_nSelectNum < 0 || m_nSelectNum >= m_nTotalCount )
			return title;
		JSONObject json = item.optJSONObject(m_nSelectNum);
		if(json == null)
			return title;
		title = json.optString("title");
		return title;
	}

	@Override
	public String getDescription() {
		String description =  "";		
		JSONArray  item = m_Data.optJSONArray("items");
		if( m_nSelectNum < 0 || m_nSelectNum >= m_nTotalCount )
			return description;
		JSONObject json = item.optJSONObject(m_nSelectNum);
		if(json == null)
			return description;
		description = json.optString("subtitle");
		return description;
		
	}

	
}
