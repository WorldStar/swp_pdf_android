package mobile.swp.constant;

import android.graphics.Color;

public class Const {
	public static final int APP_BACK_COLOR = Color.rgb(4, 153, 168);
	
	public static final int 	DEFAULT_SCREEN_X = 1080;
	public static final int 	DEFAULT_SCREEN_Y = 1850;

	public static final int QUESTION_TEXT_COLOR = Color.rgb(61, 97, 67);
	public static final int TABLE_HEADER_BACK_COLOR = Color.rgb(9, 95, 104);

	public static final String	PAGE_NAME = "page_name";
	
	public static final int BUSINESS_STRATEGY = 0;
	public static final int SEGMANET_ROLE = 1;
	public static final int ENVIRONMENTAL_SCAN = 2;
	public static final int CURRENT_STATE = 3;
	public static final int FUTURING = 4;
	public static final int GAP_ANALYSIS = 5;
	public static final int ACTION_PLANNING = 6;
	public static final int MONITOR_REPORT = 7;
	
	public static final String	ERROR_FILL_CONTENT = "Please fill the content.";
	public static final String	ERROR_SELECT_CONTENT = "Please select the content.";
	
}
