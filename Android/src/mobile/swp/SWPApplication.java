package mobile.swp;

import mobile.swp.choice.ChoiceManager;
import mobile.swp.constant.Const;
import mobile.swp.pdf.creator.SWPDocumentCreator;
import android.app.Application;
import android.content.Context;

import common.design.layout.ScreenAdapter;
import common.library.utils.DataUtils;

public class SWPApplication extends Application {
	
	   @Override
	    public void onCreate() {
	        super.onCreate();
	        
	        initScreenAdapter();
	        setContextToComponents();
	   }   
	 
	   private void initScreenAdapter()
	   {
		   ScreenAdapter.setDefaultSize(Const.DEFAULT_SCREEN_X, Const.DEFAULT_SCREEN_Y);        
	       ScreenAdapter.setApplicationContext(this.getApplicationContext());
	   }
	   
	   private void setContextToComponents()
	   {
		   DataUtils.setContext(this);
		   ChoiceManager.getInstance().setContext(this);
		   
		   SWPDocumentCreator.getInstance().setContext(this);
		   SWPDocumentCreator.getInstance().copyPDFTemplateToExternalStorage();
	   }
	   public static SWPApplication getApplication(Context context) {
	       return (SWPApplication) context.getApplicationContext();
	   }   
	   
}
