package mobile.swp.adapters;

import java.util.List;

import mobile.swp.R;
import mobile.swp.mvp.FileListPresenter;

import org.json.JSONObject;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import common.design.layout.LayoutUtils;
import common.design.layout.ScreenAdapter;
import common.library.utils.MyTime;
import common.list.adapter.ItemCallBack;
import common.list.adapter.ItemResult;
import common.list.adapter.MyListAdapter;

public class FileListAdapter extends MyListAdapter {

	public FileListAdapter(Context context, List<JSONObject> data,
			int resource, ItemCallBack callback) {
		super(context, data, resource, callback);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected void loadItemViews(View rowView, int position)
	{
		ImageView imgFileIcon = (ImageView) rowView.findViewById(R.id.img_file_icon);
		TextView txtFileDesc = (TextView) rowView.findViewById(R.id.txt_file_description);
		TextView txtFileInfo = (TextView) rowView.findViewById(R.id.txt_file_info);
		
		LayoutUtils.setSize(imgFileIcon, 130, 100, true);
		LayoutUtils.setMargin(imgFileIcon, 25, 0, 20, 0, true);
		LayoutUtils.setMargin(rowView.findViewById(R.id.lay_file_info), 0, 31, 30, 31, true);
		
		txtFileDesc.setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(43));
		txtFileInfo.setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(43));
		txtFileInfo.setTextColor(Color.rgb(104, 102, 102));
		
		JSONObject file = getItem(position);
		if( file == null )
			return;
		
		txtFileDesc.setText(file.optString(FileListPresenter.FILE_NAME, "Unknown.pdf"));
		txtFileInfo.setText("Created: " + file.optString(FileListPresenter.FILE_DATE, MyTime.getCurrentDateForEnglish()));
		
		final int num = position;
		rowView.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View paramView) {
				if( m_callBack != null )
				{
					ItemResult itemData = new ItemResult();
					itemData.mItemNumber = num;
					itemData.itemData = getItem(num);
					m_callBack.doClick(itemData);
				}				
			}
		});
	}	
	
	
}
