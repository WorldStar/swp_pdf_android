package mobile.swp.adapters;

import java.util.List;

import mobile.swp.R;
import mobile.swp.constant.Const;

import org.json.JSONObject;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import common.design.layout.LayoutUtils;
import common.design.layout.ScreenAdapter;
import common.list.adapter.ItemCallBack;
import common.list.adapter.MyListAdapter;

public class MeasureTableAdapter extends MyListAdapter {
	int 	m_nCol = 1;
	public static final String ITEM_NAME = "item_name";
	public MeasureTableAdapter(Context context, List<JSONObject> data,
			int resource, int col, ItemCallBack callback) {
		super(context, data, resource, callback);
		m_nCol = col;
	}
	
	@Override
	protected void loadItemViews(View rowView, int position)
	{		
		TextView txtContent = (TextView) rowView.findViewById(R.id.txt_label);
		LayoutUtils.setSize(rowView, LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, true);
		if( position / m_nCol == 0 )
		{
			LayoutUtils.setSize(txtContent, LayoutParams.MATCH_PARENT, 113, true);
			rowView.setBackgroundColor(Const.TABLE_HEADER_BACK_COLOR);
			txtContent.setTextColor(Color.WHITE);
		}
		else
		{
			LayoutUtils.setSize(txtContent, LayoutParams.MATCH_PARENT, 237, true);
			rowView.setBackgroundColor(Color.WHITE);
			txtContent.setTextColor(Color.BLACK);
		}
		
		LayoutUtils.setPadding(txtContent, 8, 0, 8, 0, true);
		JSONObject item = getItem(position);
		txtContent.setText(item.optString(ITEM_NAME, ""));
		txtContent.setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(37));
	}	
	
	
}
