package mobile.swp.adapters;

import java.util.List;

import mobile.swp.R;

import org.json.JSONObject;

import android.content.Context;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import common.design.layout.LayoutUtils;
import common.design.layout.ScreenAdapter;
import common.list.adapter.ItemCallBack;
import common.list.adapter.ItemResult;
import common.list.adapter.MyListAdapter;

public class ItemListAdapter extends MyListAdapter {
	public static final String ITEM_NAME = "item_name";
	int		m_nSelectedNumber = 0;
	int		m_nFontSize = 40;
	int		m_nHeight = 177;
	
	public ItemListAdapter(Context context, List<JSONObject> data,
			int resource, int selectedNumber, int fontSize, int height, ItemCallBack callback) {
		super(context, data, resource, callback);

		m_nSelectedNumber = selectedNumber;
		m_nFontSize = fontSize;
		m_nHeight = height;
	}
	
	@Override
	protected void loadItemViews(View rowView, int position)
	{
		JSONObject item = getItem(position);
		
		TextView txtItem = (TextView) rowView.findViewById(R.id.txt_label);
		
		LayoutUtils.setSize(txtItem, LayoutParams.FILL_PARENT, m_nHeight, true);
		txtItem.setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(m_nFontSize));
		LayoutUtils.setMargin(txtItem, 0, 0, 0, 0, true);
		txtItem.setText("High");
		txtItem.setText(item.optString(ITEM_NAME, ""));
		txtItem.setBackgroundResource(R.drawable.item_bg_selector);
		
		if( position == m_nSelectedNumber )
			txtItem.setSelected(true);
		else
			txtItem.setSelected(false);
		
		final int num = position;
		
		txtItem.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View paramView) {
				if( m_callBack != null )
				{
					ItemResult itemData = new ItemResult();
					itemData.mItemNumber = num;
					itemData.itemData = getItem(num);
					m_callBack.doClick(itemData);
					setSelected(num);
				}
			}
		});
	}	
	
	public void setSelected(int selectedNumber)
	{
		m_nSelectedNumber = selectedNumber;
		notifyDataSetChanged();
	}
	
}
