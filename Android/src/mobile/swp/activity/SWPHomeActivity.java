package mobile.swp.activity;

import java.util.ArrayList;

import mobile.swp.R;
import mobile.swp.constant.Const;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import common.design.layout.LayoutUtils;
import common.design.layout.ScreenAdapter;
import common.design.utils.ResourceUtils;
import common.library.utils.CheckUtils;
import common.manager.activity.ActivityManager;

public class SWPHomeActivity extends NavigateBarActivity {
	TextView				m_txtTitle = null;
	ArrayList<Button>		m_btnArrayCategory = new ArrayList<Button>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.layout_home);
		
		addNavigateBar();
		findViews();		
		layoutControls();
		initData();
		initUIEvents();		
		checkOtherPageTransfer();
	}
	
	
	private void findViews()
	{
		m_txtTitle = (TextView) findViewById(R.id.txt_title);
		
		int [] btn_category_bg_resourceid = {
				R.id.btn_business_strategy,
				R.id.btn_segment_roles,
				R.id.btn_environmental_scan,
				R.id.btn_current_state_analysis,
				R.id.btn_futuring,
				R.id.btn_gap_analysis,
				R.id.btn_action_planning,
				R.id.btn_monitor_report
			};
		for(int i = 0; i < btn_category_bg_resourceid.length; i++ )
			m_btnArrayCategory.add((Button)findViewById(btn_category_bg_resourceid[i]));
	}
	
	private void layoutControls()
	{
		m_txtTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(50));
		LayoutUtils.setMargin(m_txtTitle, 160, 165, 160, 0, true);

		View layCategoryButton = findViewById(R.id.lay_category_button);
		LayoutUtils.setMargin(layCategoryButton, 0, 125, 0, 0, true);

		for(int i=0; i<((ViewGroup)layCategoryButton).getChildCount(); ++i) 
		{
			View nextChild = ((ViewGroup)layCategoryButton).getChildAt(i);
			if( nextChild == null || nextChild instanceof LinearLayout == false )
				continue;
			
			LayoutUtils.setMargin(nextChild, 0, 37, 0, 0, true);
		}
		
		for(int i = 0; i < m_btnArrayCategory.size(); i++)
		{
			Button btnCategory = m_btnArrayCategory.get(i);
			if( CheckUtils.isEmpty(btnCategory) == true )
				continue;
			
			btnCategory.setBackgroundResource(R.drawable.home_category_btn_bg);
			btnCategory.setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(42));
			ResourceUtils.addClickEffect(btnCategory);
			LayoutUtils.setSize(btnCategory, 478, 180, true);
			LayoutUtils.setMargin(btnCategory, 18, 0, 18, 0, true);				
		}
		
		m_btnBack.setVisibility(View.GONE);
		m_btnNext.setVisibility(View.GONE);
	}
	
	private void initData()
	{
		m_txtTitle.setText(R.string.home_category_title);
		
		int [] str_category_name_id = {
		     R.string.home_business_strategy,
		     R.string.home_segment_roles,
		     R.string.home_environmental_scan,
		     R.string.home_current_state_analysis,
		     R.string.home_futuring,
		     R.string.home_gap_analysis,
		     R.string.home_action_planning,
		     R.string.home_monitor_report	
		     };
		for(int i = 0; i < m_btnArrayCategory.size(); i++)
		{
			Button btnCategory = m_btnArrayCategory.get(i);
			if( CheckUtils.isEmpty(btnCategory) == true )
				continue;
			
			btnCategory.setText(str_category_name_id[i]);		
			btnCategory.setTextColor(Color.WHITE);
			btnCategory.setTypeface(null, Typeface.BOLD);
		}
	     
	}
	
	private void initUIEvents()
	{
		for(int i = 0; i < m_btnArrayCategory.size(); i++)
		{
			Button btnCategory = m_btnArrayCategory.get(i);
			if( CheckUtils.isEmpty(btnCategory) == true )
				continue;
			
			btnCategory.setOnClickListener(onClickButtonListener);
		}

	}
	
	private View.OnClickListener onClickButtonListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			switch(arg0.getId())
			{
			case R.id.btn_business_strategy:
			case R.id.btn_segment_roles:
			case R.id.btn_environmental_scan:
			case R.id.btn_current_state_analysis:
			case R.id.btn_futuring:
			case R.id.btn_gap_analysis:
			case R.id.btn_action_planning:
			case R.id.btn_monitor_report:
				gotoOtherPage(arg0.getId());
				break;
			}
		}
	};
	
	private void checkOtherPageTransfer()
	{
		int page = 0;
		Bundle bundle = getIntent().getExtras();
		if( bundle != null )
			page = bundle.getInt(Const.PAGE_NAME, 0);
		
		if(page > 0 && page < 9 )
		{
			int [] btn_category_bg_resourceid = {
					R.id.btn_business_strategy,
					R.id.btn_segment_roles,
					R.id.btn_environmental_scan,
					R.id.btn_current_state_analysis,
					R.id.btn_futuring,
					R.id.btn_gap_analysis,
					R.id.btn_action_planning,
					R.id.btn_monitor_report
				};
			gotoOtherPage(btn_category_bg_resourceid[page - 1]);
		}
	}
	public void gotoOtherPage(int resID)
	{
		Bundle bundle = new Bundle();
		
		switch(resID)
		{
		case R.id.btn_business_strategy:
			bundle.putInt(Const.PAGE_NAME, Const.BUSINESS_STRATEGY);
			ActivityManager.changeActivity(this, FileListActivity.class, bundle, false, null );
			break;
		case R.id.btn_segment_roles:
			bundle.putInt(Const.PAGE_NAME, Const.SEGMANET_ROLE);
			ActivityManager.changeActivity(this, FileListActivity.class, bundle, false, null );
			break;
		case R.id.btn_environmental_scan:
			bundle.putInt(Const.PAGE_NAME, Const.ENVIRONMENTAL_SCAN);
			ActivityManager.changeActivity(this, FileListActivity.class, bundle, false, null );
			break;
		case R.id.btn_current_state_analysis:
			bundle.putInt(Const.PAGE_NAME, Const.CURRENT_STATE);
			ActivityManager.changeActivity(this, FileListActivity.class, bundle, false, null );
			break;
		case R.id.btn_futuring:
			bundle.putInt(Const.PAGE_NAME, Const.FUTURING);
			ActivityManager.changeActivity(this, FileListActivity.class, bundle, false, null );
			break;
		case R.id.btn_gap_analysis:
			bundle.putInt(Const.PAGE_NAME, Const.GAP_ANALYSIS);
			ActivityManager.changeActivity(this, FileListActivity.class, bundle, false, null );
			break;
		case R.id.btn_action_planning:
			bundle.putInt(Const.PAGE_NAME, Const.ACTION_PLANNING);
			ActivityManager.changeActivity(this, FileListActivity.class, bundle, false, null );
			break;
		case R.id.btn_monitor_report:
			bundle.putInt(Const.PAGE_NAME, Const.MONITOR_REPORT);
			ActivityManager.changeActivity(this, FileListActivity.class, bundle, false, null );
			break;
		
		}
	}
	
}
