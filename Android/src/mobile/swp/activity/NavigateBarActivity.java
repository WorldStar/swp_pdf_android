package mobile.swp.activity;

import mobile.swp.R;
import mobile.swp.mvp.NavigateBarView;
import android.os.Bundle;
import android.support.v4.view.ViewPager.LayoutParams;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import common.design.layout.LayoutUtils;
import common.design.layout.ScreenAdapter;
import common.design.utils.ResourceUtils;

public class NavigateBarActivity extends BaseActivity implements NavigateBarView {
	protected View 		m_HeaderBarView = null;
	protected TextView 	m_txtPageTitle = null;	
	protected Button	 m_btnBack = null;
	protected Button	 m_btnNext = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		overridePendingTransition(R.anim.start_enter, R.anim.start_exit);
	}
	
	protected boolean addNavigateBar() 
	{
		LinearLayout layout_header = (LinearLayout) findViewById(R.id.navigate_bar);
		if( layout_header == null )
			return false;		
		
		m_HeaderBarView = LayoutInflater.from(this).inflate(R.layout.fragment_navigate_top_bar, null);
		if( m_HeaderBarView == null )
			return false;
		
		layout_header.addView(m_HeaderBarView);
		m_txtPageTitle = (TextView) m_HeaderBarView.findViewById(R.id.txt_navigate_bar_title);
		m_btnBack = (Button) m_HeaderBarView.findViewById(R.id.btn_left_button);
		m_btnNext = (Button) m_HeaderBarView.findViewById(R.id.btn_right_button);
		if( m_txtPageTitle == null || m_btnBack == null || m_btnNext == null )
			return false;
		
		layoutNavigateBar();
		initNavigateEvents();
		
		return true;
	}
	
	private void layoutNavigateBar()
	{
		int miniHeight = ScreenAdapter.miliToPx(100);
		if( ScreenAdapter.computeHeight(164) > miniHeight )
		{
			float titleSize = ScreenAdapter.computeHeight(55);
			m_txtPageTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, titleSize);
			LayoutUtils.setSize(m_btnBack, 70, 71, true);
			LayoutUtils.setSize(m_btnNext, 160, 48, true);
			LayoutUtils.setMargin(m_btnBack, 38, 0, 0, 0, true);
			LayoutUtils.setMargin(m_btnNext, 0, 0, 38, 0, true);
		}
		else
		{
			float ratio = 0.6f;
			float titleSize = ScreenAdapter.miliToPx(55) * ratio;
			m_txtPageTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, titleSize);
			
			int buttonSizeX = (int)(ScreenAdapter.miliToPx(160) * ratio);
			int buttonSizeY = (int)(ScreenAdapter.miliToPx(48) * ratio);
			LayoutUtils.setSize(m_btnNext, buttonSizeX, buttonSizeY, false);
			
			buttonSizeX = (int)(ScreenAdapter.miliToPx(70) * ratio);
			buttonSizeY = (int)(ScreenAdapter.miliToPx(71) * ratio);
			LayoutUtils.setSize(m_btnBack, buttonSizeX, buttonSizeY, false);
			
			LayoutUtils.setMargin(m_btnBack, (int)(ScreenAdapter.miliToPx(38) * ratio), 0, 0, 0, false);
			LayoutUtils.setMargin(m_btnNext, 0, 0, (int)(ScreenAdapter.miliToPx(38) * ratio), 0, false);
		}
		
		LayoutUtils.setSize(m_HeaderBarView, LayoutParams.MATCH_PARENT, 164, true);


		
		ResourceUtils.addClickEffect(m_btnBack);
		ResourceUtils.addClickEffect(m_btnNext);

	}
	
	private void initNavigateEvents()
	{
		m_btnBack.setOnClickListener(onClickNavigateButtonListener);
		m_btnNext.setOnClickListener(onClickNavigateButtonListener);
	}
	
	private View.OnClickListener onClickNavigateButtonListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			switch(arg0.getId())
			{
			case R.id.btn_left_button:
				gotoMenuPage();
				break;
			case R.id.btn_right_button:
				gotoNextPage();
				break;
			}
		}
	};
	
	protected void gotoBackPage()
	{
		overridePendingTransition(R.anim.end_enter, R.anim.end_exit);
		onFinishActivity();
	}
	
	protected void gotoNextPage()
	{
	}
	
	protected void gotoMenuPage()
	{
		
	}
	
	@Override 
	public void onBackPressed( ) 
	{	
		gotoBackPage();
	}

	@Override
	public void setNavigateButtonBackground(int resourceID, int side) {
		if( side == NavigateBarView.LEFT_SIDE )
			m_btnBack.setBackgroundResource(resourceID);
		if( side == NavigateBarView.RIGHT_SIDE )
			m_btnNext.setBackgroundResource(resourceID);
		
	}
}
