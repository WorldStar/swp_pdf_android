package mobile.swp.activity;

import mobile.swp.R;
import mobile.swp.mvp.CurrentStatePresenter;
import mobile.swp.mvp.CurrentStateView;
import android.os.Bundle;
import android.support.v4.view.ViewPager.LayoutParams;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import common.design.layout.LayoutUtils;
import common.design.layout.ScreenAdapter;

public class CurrentState6Activity extends CurrentStateBaseActivity implements CurrentStateView {	
	TextView		m_txtTitle 		= null;
	CurrentStatePresenter	presenter = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.layout_current_state_6);
	}	
	
	protected void layoutFragments(ViewGroup fragment, int num)
	{
		for(int j = 0; j < fragment.getChildCount(); j++ )
		{
			View view = fragment.getChildAt(j);
			if( view == null )
				continue;
			
			if( view instanceof TextView )
			{
				LayoutUtils.setMargin(view, 48, 0, 0, 0, true);
				((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(37));
			}
			
			if( view instanceof EditText )
			{
				LayoutUtils.setMargin(view, 48, 0, 48, 0, true);
				LayoutUtils.setSize(view, LayoutParams.FILL_PARENT, 384, true);
				
				((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(42));
				LayoutUtils.setPadding(view, m_EditPadding, 0, m_EditPadding, 0, true);
			}			
		}
		
	}
	
}
