package mobile.swp.activity;

import java.text.SimpleDateFormat;
import java.util.Locale;

import kankan.wheel.widget.DateWheel;
import mobile.swp.R;
import mobile.swp.component.combobox.ComboBoxView;
import mobile.swp.constant.Const;
import mobile.swp.mvp.BasePresenter;
import mobile.swp.mvp.CurrentStatePresenter;
import mobile.swp.mvp.CurrentStatePresenterImpl;
import mobile.swp.mvp.CurrentStateView;
import mobile.swp.mvp.NavigateBarPresenter;
import mobile.swp.pdf.creator.SWPDocumentCreator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Paint;
import android.os.Bundle;
import android.text.TextPaint;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import common.design.layout.LayoutUtils;
import common.design.layout.ScreenAdapter;
import common.design.utils.ResourceUtils;
import common.manager.activity.ActivityManager;

public class CurrentStateBaseActivity extends NavigateBarActivity implements CurrentStateView {	
	TextView		m_txtTitle 		= null;
	CurrentStatePresenter	presenter = null;
	static final int	m_EditPadding = 20;
	
	protected void onCreate(Bundle savedInstanceState, int layoutID) {
		super.onCreate(savedInstanceState);
		
		setContentView(layoutID);
		
		createPresenter();
		addNavigateBar();
		findViews();		
		layoutControls();
		initData();
//		initUIEvents();
	}
	
	private void createPresenter()
	{
		int page = 1;
		Bundle bundle = getIntent().getExtras();
		if( bundle != null )
			page = bundle.getInt(Const.PAGE_NAME, 1);
		
		presenter = new CurrentStatePresenterImpl(this, page);
	}
	
	protected void findViews()
	{
		m_txtTitle = (TextView) findViewById(R.id.txt_title);
		
		View layContainer = findViewById(R.id.lay_container);		
		for(int i=0; i<((ViewGroup)layContainer).getChildCount(); ++i) 
		{
			View subcontainer = ((ViewGroup)layContainer).getChildAt(i);
			if( subcontainer == null )
				continue;			
		}
	}
	
	protected void layoutControls()
	{
		m_txtTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(50));
		LayoutUtils.setMargin(m_txtTitle, 0, 20, 0, 0, true);
		
			
		View layContainer = findViewById(R.id.lay_container);		
		LayoutUtils.setMargin(layContainer, 0, 25, 0, 0, true);
				
		for(int i = 0; i < ((ViewGroup)layContainer).getChildCount(); i++ ) 
		{
			View subcontainer = ((ViewGroup)layContainer).getChildAt(i);
			if( subcontainer == null )
				continue;	
			
			if( i == ((ViewGroup)layContainer).getChildCount() - 1 )
				LayoutUtils.setMargin(subcontainer, 0, 15, 0, 15, true);
			else				
				LayoutUtils.setMargin(subcontainer, 0, 15, 0, 0, true);
			
			if( subcontainer instanceof RelativeLayout == false && 
					subcontainer instanceof LinearLayout == false )
				continue;
			
			layoutFragments((ViewGroup)subcontainer, i);					
		}
	}
	
	protected void layoutFragments(ViewGroup fragment, int num)
	{
		
	}
	
	private void initData()
	{
		TextPaint paint = new TextPaint(Paint.UNDERLINE_TEXT_FLAG);
		paint.setAntiAlias(true);
		m_txtTitle.setPaintFlags( paint.getFlags());
		m_txtTitle.setText("Current State: Workforce Supply Groups");
		
		((BasePresenter)presenter).initData();
	}

	@Override
	protected void gotoNextPage()
	{
		((NavigateBarPresenter)presenter).gotoNextPage();
	}
	
	@Override
	protected void gotoMenuPage()
	{
		if( presenter instanceof NavigateBarPresenter )
			((NavigateBarPresenter)presenter).gotoMenuPage();
	}

	@Override
	public void displayUILabel(JSONObject data) {
		m_txtTitle.setText(data.optString("title_" + SWPDocumentCreator.getInstance().getCreatePage(), ""));
		
		JSONArray items = data.optJSONArray("items");
		
		View layContainer = findViewById(R.id.lay_container);		
		for(int i=0; i<((ViewGroup)layContainer).getChildCount(); ++i) 
		{
			View subcontainer = ((ViewGroup)layContainer).getChildAt(i);
			if( subcontainer == null  )
				continue;	
			
			if( subcontainer instanceof RelativeLayout == false && 
					subcontainer instanceof LinearLayout == false )
				continue;

			JSONArray subitems = items.optJSONArray(i);
			ViewGroup fragment = (ViewGroup)subcontainer;
			for(int j = 0; j < fragment.getChildCount(); j++ )
			{
				View view = fragment.getChildAt(j);
				if( view == null )
					continue;				
				
				String content = subitems.optString(j, "");
				if( view instanceof EditText )
				{
					((EditText)view).setHint(content);
				}
				else if( view instanceof Button )
				{
					((Button)view).setHint(content);
					ResourceUtils.addClickEffect(view);
				}
				else if( view instanceof TextView )
				{
					((TextView)view).setText(content);
				}
				else if( view instanceof ComboBoxView )
				{
					((ComboBoxView)view).setHint(content);
				}
			}
		}
	}

	@Override
	public JSONObject getCurrentStateData() {
		JSONObject data = new JSONObject();
		
		View layContainer = findViewById(R.id.lay_container);		
		for(int i=0; i<((ViewGroup)layContainer).getChildCount(); ++i) 
		{
			View subcontainer = ((ViewGroup)layContainer).getChildAt(i);
			if( subcontainer == null  )
				continue;	
			
			if( subcontainer instanceof RelativeLayout == false && 
					subcontainer instanceof LinearLayout == false )
				continue;
			
			ViewGroup fragment = (ViewGroup)subcontainer;
			for(int j = 0; j < fragment.getChildCount(); j++ )
			{
				View view = fragment.getChildAt(j);
				if( view == null )
					continue;
				
				String content = "";
				if( view instanceof EditText )
					content = ((EditText)view).getText().toString();
				
				if( view instanceof DateWheel )
				{
					SimpleDateFormat format = new SimpleDateFormat("MMMM dd, yyyy",  Locale.ENGLISH);
					content = ((DateWheel)view).getDateStringForEnglish();
				}
				
				if( view instanceof ComboBoxView )
					content = ((ComboBoxView)view).getText();
				
				if( view instanceof Button )
				{
					if( ((Button)view).isSelected() )
						content = "1";
					else
						content = "0";	
				}
				
				try {
					data.put("value_" + i + "_" + j, content);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		
		return data;
	}

	@Override
	public void gotoPage(int pageNum) {
	}

	@Override
	public void showSelectedListInfo(String tag, String content) {	
		View layContainer = findViewById(R.id.lay_container);		
		for(int i=0; i<((ViewGroup)layContainer).getChildCount(); ++i) 
		{
			View subcontainer = ((ViewGroup)layContainer).getChildAt(i);
			if( subcontainer == null  )
				continue;	
			if( subcontainer instanceof RelativeLayout == false && 
					subcontainer instanceof LinearLayout == false )
				continue;
			
			ViewGroup fragment = (ViewGroup)subcontainer;
			for(int j = 0; j < fragment.getChildCount(); j++ )
			{
				View view = fragment.getChildAt(j);
				if( view == null )
					continue;
				
				Object tagObj = view.getTag();
				if( tagObj != null && tagObj instanceof String && tagObj.equals(tag) )
				{
					if(view instanceof TextView)
						((TextView) view).setText(content);
					if(view instanceof ComboBoxView)
						((ComboBoxView) view).setText(content);
				}
			}
		}
	}

	@Override
	public void gotoFileListPage() {
		ActivityManager.getInstance().popAllActivityExceptOne(FileListActivity.class);				
	}
	
}
