package mobile.swp.activity;

import mobile.swp.R;
import mobile.swp.component.combobox.ComboBoxView;
import mobile.swp.constant.Const;
import android.os.Bundle;
import android.support.v4.view.ViewPager.LayoutParams;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import common.design.layout.LayoutUtils;
import common.design.layout.ScreenAdapter;
import common.manager.activity.ActivityManager;

public class CurrentState5Activity extends CurrentStateBaseActivity {	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.layout_current_state_5);
	}
	
	protected void layoutFragments(ViewGroup fragment, int num)
	{
		for(int j = 0; j < fragment.getChildCount(); j++ )
		{
			View view = fragment.getChildAt(j);
			if( view == null )
				continue;
			
			if( view instanceof TextView )
			{
				LayoutUtils.setMargin(view, 48, 0, 0, 0, true);
				((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(37));
			}
			
			if( view instanceof EditText )
			{
				
				LayoutUtils.setMargin(view, 48, 0, 48, 0, true);
				LayoutUtils.setSize(view, LayoutParams.FILL_PARENT, 115, true);
				
				((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(42));
				LayoutUtils.setPadding(view, 15, 0, 15, 0, true);
			}			
			
			if( view instanceof ComboBoxView )
			{
				LayoutUtils.setMargin(view, 48, 0, 48, 0, true);
				LayoutUtils.setSize(view, LayoutParams.FILL_PARENT, 115, true);
				((ComboBoxView)view).setTextSize(ScreenAdapter.computeHeight(40));
				view.setOnClickListener(onClickButtonListener);	
				
				view.setTag("combo_" + (num + 1));				
			}
		}
		
	}
	
	private View.OnClickListener onClickButtonListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			if( arg0 instanceof ComboBoxView )
				presenter.showListDialog((String)arg0.getTag(), ((ComboBoxView)arg0).getText());			
		}
	};
	
	@Override
	public void gotoPage(int pageNum) {
		Bundle bundle = new Bundle();
		bundle.putInt(Const.PAGE_NAME, pageNum);		
		ActivityManager.changeActivity(this, CurrentState6Activity.class, bundle, false, null );	
	}
}
