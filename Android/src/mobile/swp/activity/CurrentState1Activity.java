package mobile.swp.activity;

import kankan.wheel.widget.DateWheel;
import mobile.swp.R;
import mobile.swp.constant.Const;
import mobile.swp.mvp.CurrentStateView;
import android.os.Bundle;
import android.support.v4.view.ViewPager.LayoutParams;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import common.design.layout.LayoutUtils;
import common.design.layout.ScreenAdapter;
import common.manager.activity.ActivityManager;

public class CurrentState1Activity extends CurrentStateBaseActivity implements CurrentStateView {	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.layout_current_state_1);
	}	
	
	protected void layoutFragments(ViewGroup fragment, int num)
	{
		for(int j = 0; j < fragment.getChildCount(); j++ )
		{
			View view = fragment.getChildAt(j);
			if( view == null )
				continue;
			
			if( view instanceof TextView )
			{
				switch( num )
				{
				case 0:
				case 1:
				case 4:
				case 5:
					LayoutUtils.setSize(view, 232, LayoutParams.WRAP_CONTENT, true);
					break;				
				}
				
				LayoutUtils.setMargin(view, 48, 0, 0, 0, true);
				((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(37));
			}
			
			if( view instanceof EditText )
			{
				if( num == 0 || num == 4 || num == 5 )
				{
					LayoutUtils.setMargin(view, 64, 0, 48, 0, true);
					if( num == 0 )
						LayoutUtils.setSize(view, LayoutParams.FILL_PARENT, 115, true);
					else
						LayoutUtils.setSize(view, LayoutParams.FILL_PARENT, 294, true);					
				}
				if( num == 2 )
				{
					LayoutUtils.setMargin(view, 48, 0, 48, 0, true);
					LayoutUtils.setSize(view, LayoutParams.FILL_PARENT, 384, true);
				}
				((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(42));
				LayoutUtils.setPadding(view, m_EditPadding, 0, m_EditPadding, 0, true);
			}			
			
			if( view instanceof DateWheel )
			{
				LayoutUtils.setSize(view, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT, true);
				LayoutUtils.setMargin(view, 64, 0, 48, 0, true);
			}
		}
		
	}

	@Override
	public void gotoPage(int pageNum) {
		Bundle bundle = new Bundle();
		bundle.putInt(Const.PAGE_NAME, pageNum);		
		ActivityManager.changeActivity(this, CurrentState2Activity.class, bundle, false, null );				
	}

	
}
