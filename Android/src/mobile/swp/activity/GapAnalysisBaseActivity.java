package mobile.swp.activity;

import java.util.ArrayList;
import java.util.List;

import mobile.swp.R;
import mobile.swp.adapters.MeasureTableAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import common.design.layout.LayoutUtils;
import common.design.layout.ScreenAdapter;
import common.library.utils.ActionUtils;

public class GapAnalysisBaseActivity extends CurrentStateBaseActivity {	
	GridView		m_gridMeasure 	= null;
	Button			m_btnAddRecord 	= null;
	
	MeasureTableAdapter 		m_adapterMeasureTable = null;
	int				m_nRowCount = 0;
	int				m_nColCount = 4;
	
	@Override
	protected void onCreate(Bundle savedInstanceState, int layoutID) {
		m_nRowCount = 0;
		
		super.onCreate(savedInstanceState, layoutID);
	}	
	
	protected void findViews()
	{
		super.findViews();
		m_gridMeasure = (GridView) findViewById(R.id.table_measure);
		m_btnAddRecord = (Button) findViewById(R.id.btn_record_add);
	}
	
	protected void layoutControls()
	{
		super.layoutControls();
		
		layoutGridView();
					
		m_btnAddRecord.setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(41));
		LayoutUtils.setMargin(m_btnAddRecord, 0, 25, 0, 25, true);
		LayoutUtils.setSize(m_btnAddRecord, 476, 100, true);
		m_btnAddRecord.setTextColor(Color.WHITE);
		
		m_btnAddRecord.setOnClickListener(onClickButtonListener);
	}
	
	protected void layoutGridView()
	{
		LayoutUtils.setSize(m_gridMeasure, 1080 * m_nColCount / 4, 113 + 237, true);
		LayoutUtils.setMargin(m_gridMeasure, 0, 25, 0, 0, true);
	}
		
	//initGapsData();
	protected List<JSONObject> getGapTableData(JSONObject data)
	{
		List<JSONObject> tableData = new ArrayList<JSONObject>();
		if( data == null )
			return tableData;
		
		JSONArray header = data.optJSONArray("grid_items");
		
		// TODO
		for(int i = 0; i < Math.max(header.length(), m_nColCount * 2); i++ )
		{
			JSONObject fileinfo = new JSONObject();
			try {
				fileinfo.put(MeasureTableAdapter.ITEM_NAME, header.optString(i, ""));
			} catch (JSONException e) {
				e.printStackTrace();
			}
			tableData.add(fileinfo);
		}
		
		return tableData;
	}
	
	@Override
	public void displayUILabel(JSONObject data) {
		super.displayUILabel(data);
		initGapsData(data);
	}
	private void initGapsData(JSONObject data)
	{
		List<JSONObject> tableData = getGapTableData(data);
		m_adapterMeasureTable = new MeasureTableAdapter(this, tableData, R.layout.fragment_label_center, m_nColCount, null);		
		m_gridMeasure.setAdapter(m_adapterMeasureTable);
	}
	
	private void removeInputedData()
	{
		View layContainer = findViewById(R.id.lay_container);		
		for(int i=0; i<((ViewGroup)layContainer).getChildCount(); ++i) 
		{
			if( i == 0 )
				continue;
			View subcontainer = ((ViewGroup)layContainer).getChildAt(i);
			if( subcontainer == null  )
				continue;	
			
			if( subcontainer instanceof RelativeLayout == false && 
					subcontainer instanceof LinearLayout == false )
				continue;

			ViewGroup fragment = (ViewGroup)subcontainer;
			for(int j = 0; j < fragment.getChildCount(); j++ )
			{
				View view = fragment.getChildAt(j);
				if( view == null )
					continue;				
				
				if( view instanceof EditText )
				{
					((EditText)view).setText("");
					ActionUtils.closeSoftKeyboard(view, this);
				}
			}
		}
	}
	
	protected List<JSONObject> getItemData(JSONObject data)
	{		
		List<JSONObject> tableData = new ArrayList<JSONObject>();
		
		return tableData;
	}
	
	protected void addRecords()
	{
		JSONObject data = super.getCurrentStateData();
		
		List<JSONObject> tableData = getItemData(data);


		m_adapterMeasureTable.replaceItemList(tableData, (m_nRowCount + 1) * m_nColCount);		
		removeInputedData();
		
		int rows = m_adapterMeasureTable.getCount() / m_nColCount;
		LayoutUtils.setSize(m_gridMeasure, 1080 * m_nColCount / 4, 113 + (rows - 1) * 237, true);		
	
		m_nRowCount++;
		
	}
	
	
	@Override
	public JSONObject getCurrentStateData() {
		JSONObject data = super.getCurrentStateData();
		
		List<JSONObject>  list = m_adapterMeasureTable.getData();
		JSONArray array = new JSONArray();
		for(int i = 0; i < list.size(); i++ )
			array.put(list.get(i).optString(MeasureTableAdapter.ITEM_NAME, ""));
		
		try {
			data.put("grid_data", array);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return data;
	}
	private View.OnClickListener onClickButtonListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			if( arg0.getId() == R.id.btn_record_add)
				addRecords();
		}
	};
}
