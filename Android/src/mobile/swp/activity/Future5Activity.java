package mobile.swp.activity;

import mobile.swp.R;
import mobile.swp.component.combobox.ComboBoxView;
import mobile.swp.constant.Const;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import common.design.layout.LayoutUtils;
import common.design.layout.ScreenAdapter;
import common.manager.activity.ActivityManager;

public class Future5Activity extends CurrentStateBaseActivity {	
	TextView[] m_txtScenario = new TextView[4];
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.layout_future_5);
	}
	
	protected void layoutControls()
	{
		super.layoutControls();
		
		for(int i = 0; i < 4; i++ )
			m_txtScenario[i].setTextColor(Color.rgb(114, 112, 112));
	}
	
	protected void layoutFragments(ViewGroup fragment, int num)
	{
		if( num == 1 )
		{
			LayoutUtils.setMargin(fragment, 75, 50, 66, 0, true);
			LayoutUtils.setSize(fragment, LayoutParams.FILL_PARENT, 652, true);
		}
		for(int j = 0; j < fragment.getChildCount(); j++ )
		{
			View view = fragment.getChildAt(j);
			if( view == null )
				continue;
			
			if( num == 0 )
			{
				if( view instanceof TextView )
				{
					LayoutUtils.setMargin(view, 75, 0, 0, 0, true);
					LayoutUtils.setSize(view, 320, LayoutParams.WRAP_CONTENT, true);
					((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(37));
				}	
				if( view instanceof ComboBoxView )
				{
					LayoutUtils.setMargin(view, 57, 0, 52, 0, true);
					LayoutUtils.setSize(view, LayoutParams.FILL_PARENT, 112, true);
					((ComboBoxView)view).setTextSize(ScreenAdapter.computeHeight(37));
					
					view.setOnClickListener(onClickButtonListener);	
					
					view.setTag("combo_1");	
				}	
			}
			
			if( num == 1 )
			{
				if( j == 0 && view instanceof ImageView )
				{
					LayoutUtils.setMargin(view, 0, 12, 0, 12, true);
					LayoutUtils.setSize(view, 34, LayoutParams.FILL_PARENT, true);						
				}
				if( j == 1 && view instanceof ImageView )
				{
					LayoutUtils.setMargin(view, 18, 0, 18, 0, true);
					LayoutUtils.setSize(view, LayoutParams.FILL_PARENT, 34, true);						
				}
				
				if( view instanceof TextView )
				{
					LayoutUtils.setSize(view, 414, 272, true);	
					((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(37));
					m_txtScenario[j - 2] = (TextView)view;
				}
				if( j == 2 )
					LayoutUtils.setMargin(view, 56, 652 / 2 - 272, 0, 0, true);
				if( j == 3 )
					LayoutUtils.setMargin(view, 0, 652 / 2 - 272, 0, 0, true);
				if( j == 4 )
					LayoutUtils.setMargin(view, 56, 0, 0, 0, true);
			}
			if( num == 2 )
			{				
				if( view instanceof EditText )
				{
					LayoutUtils.setSize(view, LayoutParams.FILL_PARENT, 370, true);	
					LayoutUtils.setMargin(view, 44, 0, 48, 0, true);	
					LayoutUtils.setPadding(view, 44, 44, 44, 44, true);	
					((TextView)view).setGravity(Gravity.LEFT);
					((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(37));
				}
			}
			
		}		
		
	}
	
	private View.OnClickListener onClickButtonListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			if( arg0 instanceof ComboBoxView )
				presenter.showListDialog((String)arg0.getTag(), ((ComboBoxView)arg0).getText());		
			
		}
	};
	
	@Override
	public void showSelectedListInfo(String tag, String content) {
		super.showSelectedListInfo(tag, content);
		for(int i = 0; i < 4; i ++ )
		{
			if( content.contains("" + (i + 1)) )
				m_txtScenario[i].setTextColor(Color.BLUE);
			else
				m_txtScenario[i].setTextColor(Color.rgb(114, 112, 112));
		}
		
	}
	@Override
	public void gotoPage(int pageNum) {
		Bundle bundle = new Bundle();
		bundle.putInt(Const.PAGE_NAME, pageNum);		
		ActivityManager.changeActivity(this, CurrentState1Activity.class, bundle, false, null );				
	}
	
}
