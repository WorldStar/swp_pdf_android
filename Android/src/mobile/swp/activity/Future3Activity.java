package mobile.swp.activity;

import mobile.swp.R;
import mobile.swp.constant.Const;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.TextPaint;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import common.design.layout.LayoutUtils;
import common.design.layout.ScreenAdapter;
import common.manager.activity.ActivityManager;

public class Future3Activity extends CurrentStateBaseActivity {	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.layout_future_3);
	}
	
	protected void layoutFragments(ViewGroup fragment, int num)
	{
		for(int j = 0; j < fragment.getChildCount(); j++ )
		{
			View view = fragment.getChildAt(j);
			if( view == null )
				continue;
			
			if( num == 0 )
			{
				if( j == 0 )
					LayoutUtils.setMargin(view, 297, 0, 0, 0, true);										
				if( j == 1 )
					LayoutUtils.setMargin(view, 0, 0, 14, 0, true);										
				LayoutUtils.setSize(view, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);
				((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(37));
				
			}
			else if( 1 <= num && num <= 2 )
			{
				if( view instanceof TextView )
				{
					LayoutUtils.setMargin(view, 57, 0, 0, 0, true);
					LayoutUtils.setSize(view, 189, LayoutParams.WRAP_CONTENT, true);
					((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(37));
				}
				
				if( view instanceof EditText )
				{
					LayoutUtils.setMargin(view, 48, 0, 0, 0, true);
					LayoutUtils.setSize(view, LayoutParams.FILL_PARENT, 93, true);				
					((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(42));
				}	
				
				if( view instanceof Button )
				{
					LayoutUtils.setMargin(view, 91, 0, 114, 0, true);
					LayoutUtils.setSize(view, 80, 80, true);		
				}
			}
			else if( num == 3 )
			{
				if( view instanceof TextView )
				{
					LayoutUtils.setSize(view, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);
					((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(50));
					
					TextPaint paint = new TextPaint(Paint.UNDERLINE_TEXT_FLAG);
					paint.setAntiAlias(true);
					((TextView)view).setPaintFlags( paint.getFlags());
				}
			}
			if( num > 3 )
			{
				if( view instanceof TextView )
				{
					LayoutUtils.setMargin(view, 57, 0, 0, 0, true);
					LayoutUtils.setSize(view, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);
					((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(37));
				}
				
				if( view instanceof EditText )
				{
					LayoutUtils.setMargin(view, 57, 0, 0, 0, true);
					LayoutUtils.setSize(view, LayoutParams.FILL_PARENT, 93, true);				
					((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(42));
				}	
				
				if( view instanceof Button )
				{
					LayoutUtils.setMargin(view, 91, 11, 114, 0, true);
					LayoutUtils.setSize(view, 80, 80, true);					
				}
			}
			
			if( view instanceof EditText )
				LayoutUtils.setPadding(view, m_EditPadding, 0, m_EditPadding, 0, true);
			
			if( view instanceof Button )
				view.setOnClickListener(onClickButtonListener);		
		}
		
	}
	
	private View.OnClickListener onClickButtonListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			if( arg0 instanceof Button )
			{
				arg0.setSelected(!arg0.isSelected());		
			}			
		}
	};
	
	@Override
	public void gotoPage(int pageNum) {
		Bundle bundle = new Bundle();
		bundle.putInt(Const.PAGE_NAME, pageNum);		
		ActivityManager.changeActivity(this, Future4Activity.class, bundle, false, null );				
	}
	
}
