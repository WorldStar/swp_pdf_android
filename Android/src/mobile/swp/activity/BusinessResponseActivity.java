package mobile.swp.activity;

import mobile.swp.R;
import mobile.swp.mvp.BasePresenter;
import mobile.swp.mvp.BusinessResponsePresenter;
import mobile.swp.mvp.BusinessResponsePresenterImpl;
import mobile.swp.mvp.BusinessResponseView;
import mobile.swp.mvp.NavigateBarPresenter;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.view.ViewPager.LayoutParams;
import android.text.TextPaint;
import android.util.TypedValue;
import android.widget.EditText;
import android.widget.TextView;
import common.design.layout.LayoutUtils;
import common.design.layout.ScreenAdapter;
import common.manager.activity.ActivityManager;

public class BusinessResponseActivity extends NavigateBarActivity implements BusinessResponseView {	
	TextView		m_txtTitle 		= null;
	TextView		m_txtBusinessQuestion 		= null;
	EditText 		m_editBusinessAnswer			= null;
	
	BusinessResponsePresenter presenter = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.layout_business_strategy_create);
		
		createPresenter();
		addNavigateBar();
		findViews();		
		layoutControls();
		initData();
		initUIEvents();
	}
	
	private void createPresenter()
	{
		presenter = new BusinessResponsePresenterImpl(this);
	}
	private void findViews()
	{
		m_txtTitle = (TextView) findViewById(R.id.txt_title);
		m_txtBusinessQuestion = (TextView) findViewById(R.id.txt_business_question);
		m_editBusinessAnswer = (EditText) findViewById(R.id.txt_business_answer);
	}
	
	
	private void layoutControls()
	{
		m_txtTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(50));
		LayoutUtils.setMargin(m_txtTitle, 0, 90, 0, 0, true);
		
		m_txtBusinessQuestion.setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(50));
		LayoutUtils.setSize(m_txtBusinessQuestion, LayoutParams.MATCH_PARENT, 300, true);
		LayoutUtils.setMargin(m_txtBusinessQuestion, 50, 130, 50, 0, true);
		LayoutUtils.setPadding(m_txtBusinessQuestion, 20, 0, 20, 17, false);
		
		m_editBusinessAnswer.setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(50));
		LayoutUtils.setSize(m_editBusinessAnswer, LayoutParams.MATCH_PARENT, 548, true);
		LayoutUtils.setMargin(m_editBusinessAnswer, 50, 37, 50, 0, true);


	}
	
	private void initData()
	{
		TextPaint paint = new TextPaint(Paint.UNDERLINE_TEXT_FLAG);
		paint.setAntiAlias(true);
		m_txtTitle.setPaintFlags( paint.getFlags());
		m_txtTitle.setText(getString(R.string.home_business_strategy).replace("\n", " "));
		
		((BasePresenter)presenter).initData();
	}
		
	private void initUIEvents()
	{
	}
	
	@Override
	protected void gotoBackPage()
	{
		((NavigateBarPresenter)presenter).gotoPrevPage();
	}
	@Override
	protected void gotoNextPage()
	{
		((NavigateBarPresenter)presenter).gotoNextPage();
	}
	
	@Override
	protected void gotoMenuPage()
	{
		if( presenter instanceof NavigateBarPresenter )
			((NavigateBarPresenter)presenter).gotoMenuPage();
	}	

	@Override
	public void showBusinessResponseInfo(String title, String description,
			String response) {
		m_txtTitle.setText(title);
		m_txtBusinessQuestion.setText(description);
		m_editBusinessAnswer.setText(response);
	}

	@Override
	public void changeSelfPage() {
		Bundle bundle = new Bundle();
		ActivityManager.changeActivity(this, getClass(), bundle, true, null );		
	}
	
	@Override
	public void gotoFileListPage() {
		onFinishActivity();		
	}

	@Override
	public String getBusinessResponse() {
		return m_editBusinessAnswer.getText().toString();
	}


}
