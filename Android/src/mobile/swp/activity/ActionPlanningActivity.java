package mobile.swp.activity;

import java.util.ArrayList;
import java.util.List;

import mobile.swp.R;
import mobile.swp.adapters.MeasureTableAdapter;
import mobile.swp.component.combobox.ComboBoxView;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import common.design.layout.LayoutUtils;
import common.design.layout.ScreenAdapter;

public class ActionPlanningActivity extends GapAnalysisBaseActivity {	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		m_nColCount = 5;
		super.onCreate(savedInstanceState, R.layout.layout_gap_1);
	}	
	
	@Override
	protected void layoutFragments(ViewGroup fragment, int num)
	{
		for(int j = 0; j < fragment.getChildCount(); j++ )
		{
			View view = fragment.getChildAt(j);
			if( view == null )
				continue;
			
			if( num == 0 || num == 1 )
			{
				if( view instanceof TextView )
				{
					LayoutUtils.setMargin(view, 57, 0, 0, 0, true);
					LayoutUtils.setSize(view, 144, LayoutParams.WRAP_CONTENT, true);
					((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(37));
				}
				
				if( view instanceof EditText )
				{
					LayoutUtils.setMargin(view, 0, 0, 48, 0, true);
					LayoutUtils.setSize(view, LayoutParams.FILL_PARENT, 105, true);				
					((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(42));
				}					
			}
			if( 2 < num && num < 8 )
			{
				if( view instanceof TextView && j == 0 )
				{
					LayoutUtils.setMargin(view, 40, 0, 0, 0, true);
					LayoutUtils.setSize(view, 304, LayoutParams.WRAP_CONTENT, true);
					((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(37));
				}
				
				if( view instanceof EditText )
				{
					LayoutUtils.setMargin(view, 0, 0, 18, 0, true);
					LayoutUtils.setSize(view, LayoutParams.FILL_PARENT, 105, true);				
					((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(42));
				}	
				
				if( view instanceof ComboBoxView )
				{
					LayoutUtils.setMargin(view, 0, 0, 18, 0, true);
					LayoutUtils.setSize(view, LayoutParams.FILL_PARENT, 105, true);				
					((ComboBoxView)view).setTextSize(ScreenAdapter.computeHeight(42));
					view.setOnClickListener(onClickButtonListener);		
					view.setTag("combo_1");
				}
			}
			
			if( view instanceof EditText )
				LayoutUtils.setPadding(view, m_EditPadding, 0, m_EditPadding, 0, true);

			
		}
		
	}
	
	@Override
	protected List<JSONObject> getItemData(JSONObject data)
	{		
		List<JSONObject> tableData = new ArrayList<JSONObject>();

		for(int i = 3; i < 8; i++ )
		{
			String content = data.optString("value_" + i + "_1", "");
				
			JSONObject fileinfo = new JSONObject();
			try {
				fileinfo.put(MeasureTableAdapter.ITEM_NAME, content);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			tableData.add(fileinfo);
		}
		
		return tableData;
	}
	
	private View.OnClickListener onClickButtonListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			if( arg0 instanceof ComboBoxView )
				presenter.showListDialog((String)arg0.getTag(), ((ComboBoxView)arg0).getText());	
		}
	};
	

}
