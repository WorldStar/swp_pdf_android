package mobile.swp.activity;

import java.util.ArrayList;

import mobile.swp.R;
import mobile.swp.constant.Const;
import mobile.swp.mvp.BasePresenter;
import mobile.swp.mvp.NavigateBarPresenter;
import mobile.swp.mvp.SelectChoicePresenter;
import mobile.swp.mvp.SelectChoicePresenterImpl;
import mobile.swp.mvp.SelectChoiceView;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.view.ViewPager.LayoutParams;
import android.text.TextPaint;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;

import common.design.layout.LayoutUtils;
import common.design.layout.ScreenAdapter;
import common.manager.activity.ActivityManager;

public class SelectChoiceActivity extends NavigateBarActivity implements SelectChoiceView {
	TextView		m_txtTitle 		= null;
	TextView		m_txtChoiceContent 		= null;
	LinearLayout 	m_layChoiceButton		= null;
	LinearLayout 	m_layChoiceName			= null;
	
	ArrayList<ImageButton>	m_arrayChoiceButtons = new ArrayList<ImageButton>();
	ArrayList<TextView>	m_arrayChoiceName = new ArrayList<TextView>();
	
	SelectChoicePresenter	presenter = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.layout_choice);
		
		presenter = new SelectChoicePresenterImpl(this);
		
		addNavigateBar();
		findViews();		
		layoutControls();
		initData();
		initUIEvents();
	}
	
	
	private void findViews()
	{
		m_txtTitle = (TextView) findViewById(R.id.txt_title);
		m_txtChoiceContent = (TextView) findViewById(R.id.txt_choice_content);
		m_layChoiceButton = (LinearLayout) findViewById(R.id.lay_choice_button);
		m_layChoiceName = (LinearLayout) findViewById(R.id.lay_choice_name);
		
		addViews();
	}
	
	private void addViews()
	{
		int [] btn_choice_bg_resourceid = {
				R.drawable.btn_poor_selector,
				R.drawable.btn_fair_selector,
				R.drawable.btn_average_selector,
				R.drawable.btn_good_selector,
				R.drawable.btn_excellent_selector
			};
		
		for(int i = 0; i < btn_choice_bg_resourceid.length; i++ )
		{
			ImageButton btnChoice = new ImageButton(this);
			btnChoice.setBackgroundResource(btn_choice_bg_resourceid[i]);
			btnChoice.setScaleType(ScaleType.FIT_XY);
			m_layChoiceButton.addView(btnChoice);
			m_arrayChoiceButtons.add(btnChoice);
		}
		
		int [] choice_name = {
				R.string.poor,
				R.string.fair,
				R.string.average,
				R.string.good,
				R.string.excellent
			};
		
		for(int i = 0; i < choice_name.length; i++ )
		{
			TextView txtChoiceName = new TextView(this);
			txtChoiceName.setText(choice_name[choice_name.length - 1- i]);
			txtChoiceName.setGravity(Gravity.CENTER);
			txtChoiceName.getPaint().setFakeBoldText(true);
			m_layChoiceName.addView(txtChoiceName);
			m_arrayChoiceName.add(txtChoiceName);
		}
	}
	
	private void layoutControls()
	{
		m_txtTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(50));
		LayoutUtils.setMargin(m_txtTitle, 0, 94, 0, 0, true);
		
		m_txtChoiceContent.setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(50));
		LayoutUtils.setSize(m_txtChoiceContent, LayoutParams.MATCH_PARENT, 331, true);
		LayoutUtils.setMargin(m_txtChoiceContent, 50, 130, 50, 0, true);
		
		LayoutUtils.setSize(m_layChoiceButton, 980, 151, true);
		LayoutUtils.setMargin(m_layChoiceButton, 50, 43, 50, 0, true);
		
		LayoutUtils.setMargin(m_layChoiceName, 50, 10, 50, 0, true);
		
		for(int i = 0; i < m_arrayChoiceButtons.size(); i++)
		{
			ImageButton btnChoice = m_arrayChoiceButtons.get(i);
			if( btnChoice == null )
				continue;
			
			
			LayoutUtils.setSize(btnChoice, 72, 68, true);
			LayoutUtils.setMargin(btnChoice, 55, 0, 55, 0, true);		
		}

		for(int i = 0; i < m_arrayChoiceName.size(); i++)
		{
			TextView txtChoiceName = m_arrayChoiceName.get(i);
			if( txtChoiceName == null )
				continue;
			
			txtChoiceName.setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(37));
			LayoutUtils.setSize(txtChoiceName, 150, 78, true);

			if( i == 4 )
				LayoutUtils.setMargin(txtChoiceName, 10, 0, 30, 0, true);
			else
				LayoutUtils.setMargin(txtChoiceName, 20, 0, 20, 0, true);	
			txtChoiceName.setTextColor(Color.WHITE);
		}
	}
	
	private void initData()
	{
		m_txtTitle.setText(R.string.choice2);
		TextPaint paint = new TextPaint(Paint.UNDERLINE_TEXT_FLAG);
		paint.setAntiAlias(true);
		m_txtTitle.setPaintFlags( paint.getFlags());

		
		((BasePresenter)presenter).initData();
	}
		
	private void initUIEvents()
	{
		for(int i = 0; i < m_arrayChoiceButtons.size(); i++)
		{
			ImageButton btnChoice = m_arrayChoiceButtons.get(i);
			if( btnChoice == null )
				continue;
			
			final int selectedNum = i;
			btnChoice.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					presenter.selectChoice(selectedNum);
				}
			});
		}
	}
	

	
	@Override
	public void showSelectedButton(int num)
	{
		for(int i = 0; i < m_arrayChoiceButtons.size(); i++)
		{
			ImageButton btnChoice = m_arrayChoiceButtons.get(i);
			if( btnChoice == null )
				continue;
			
			if( i == num )
			{
				LayoutUtils.setSize(btnChoice, 92, 86, true);
				LayoutUtils.setMargin(btnChoice, 45, 0, 45, 0, true);		
				btnChoice.setSelected(true);
			}
			else
			{
				LayoutUtils.setSize(btnChoice, 72, 68, true);
				LayoutUtils.setMargin(btnChoice, 55, 0, 55, 0, true);
				btnChoice.setSelected(false);
			}
			btnChoice.requestLayout();
		}
	}
	protected void gotoNextPage()
	{
		if( presenter instanceof NavigateBarPresenter )
			((NavigateBarPresenter)presenter).gotoNextPage();
	}
	protected void gotoBackPage()
	{
		if( presenter instanceof NavigateBarPresenter )
			((NavigateBarPresenter)presenter).gotoPrevPage();	
	}
	
	protected void gotoMenuPage()
	{
		if( presenter instanceof NavigateBarPresenter )
			((NavigateBarPresenter)presenter).gotoMenuPage();
	}


	@Override
	public void gotoHomePage(int menuNum) {
		Bundle bundle = new Bundle();
		bundle.putInt(Const.PAGE_NAME, menuNum);
		ActivityManager.changeActivity(this, SWPHomeActivity.class, bundle, true, null );
	}

	@Override
	public void changeSelfPage() {
		Bundle bundle = new Bundle();
		ActivityManager.changeActivity(this, getClass(), bundle, true, null );		
	}


	@Override
	public void showChoiceInfo(String title, String description, int level, boolean firstFlag) {
		m_txtTitle.setText(title);		
		m_txtChoiceContent.setText(description);
		showSelectedButton(level);
//		if( firstFlag == true )
//			m_btnBack.setVisibility(View.INVISIBLE);
//		else
//			m_btnBack.setVisibility(View.VISIBLE);
	}
	
}
