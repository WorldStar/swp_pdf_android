package mobile.swp.activity;

import mobile.swp.R;
import mobile.swp.choice.ChoiceManager;
import android.os.Bundle;
import android.os.Message;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;

import common.manager.activity.ActivityManager;

public class SplashActivity extends BaseActivity {
	private static final int		SPLASH_SHOWING_TIME		= 1000;
	
	private static final int		MSG_SHOW_NEXT_PAGE		= 100;
	ImageView m_imgBackground = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.layout_splash);
		
		findViews();
		
		doInitActions();
	}
	
	private void findViews()
	{
		m_imgBackground = (ImageView) findViewById(R.id.image_background);
	}
	
	private void doInitActions()
	{
		AlphaAnimation face_in_out_anim = new AlphaAnimation(0.1f, 1.0f);       
		face_in_out_anim.setDuration(SPLASH_SHOWING_TIME);     
		m_imgBackground.setAnimation(face_in_out_anim);
		face_in_out_anim.start();
		
		Message msg = Message.obtain();
		msg.what = MSG_SHOW_NEXT_PAGE;
		
		sendMessageDelayed(msg, SPLASH_SHOWING_TIME * 4 / 3);
	}
	
	private void gotoNextPage()
	{
		Bundle bundle = new Bundle();

		if( ChoiceManager.getInstance().isCompletedChoiceSelection() == false )
		{
			ChoiceManager.getInstance().setFirstStage();
			ActivityManager.changeActivity(this, SelectChoiceActivity.class, bundle, true, null );			
		}
		else
		{
//			   SWPDocumentCreator.getInstance().makeCurrentStateDoc("test");

			ActivityManager.changeActivity(this, SWPHomeActivity.class, bundle, true, null );			
		}
	}
	
	protected void processMessage(Message msg)
	{
		super.processMessage(msg);
		switch( msg.what )
		{
		case MSG_SHOW_NEXT_PAGE:
			gotoNextPage();
			break;

		}
	}
}
