package mobile.swp.activity;

import java.util.ArrayList;

import mobile.swp.R;
import mobile.swp.mvp.BasePresenter;
import mobile.swp.mvp.EnvironmentalScanPresenter;
import mobile.swp.mvp.EnvironmentalScanPresenterImpl;
import mobile.swp.mvp.EnvironmentalScanView;
import mobile.swp.mvp.NavigateBarPresenter;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.view.ViewPager.LayoutParams;
import android.text.TextPaint;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import common.design.layout.LayoutUtils;
import common.design.layout.ScreenAdapter;
import common.design.utils.ResourceUtils;
import common.manager.activity.ActivityManager;

public class EnvironmentalScanActivity extends NavigateBarActivity implements EnvironmentalScanView {	
	TextView		m_txtTitle 		= null;
	TextView		m_txtEnvironmentalTitle 		= null;
	TextView 		m_txtEnvironmentalSubTitle			= null;
	EnvironmentalScanPresenter presenter = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.layout_environmental_scan_create);
		
		createPresenter();
		addNavigateBar();
		findViews();		
		layoutControls();
		initData();
		initUIEvents();
	}
	
	private void createPresenter()
	{
		presenter = new EnvironmentalScanPresenterImpl(this);
	}
	private void findViews()
	{
		m_txtTitle = (TextView) findViewById(R.id.txt_title);
		m_txtEnvironmentalTitle = (TextView) findViewById(R.id.txt_first_title);
		m_txtEnvironmentalSubTitle = (TextView) findViewById(R.id.txt_second_title);
		
		View layFillContent = findViewById(R.id.lay_fill_content);
		
		for(int i=0; i<((ViewGroup)layFillContent).getChildCount(); ++i) 
		{
			View nextChild = ((ViewGroup)layFillContent).getChildAt(i);
			if( nextChild == null || nextChild instanceof RelativeLayout == false )
				continue;
			
			EditText editContent = (EditText)nextChild.findViewById(R.id.txt_file_description);
			if(editContent != null)
				editContent.setTag("fill" + i);
			
			ImageView imgMarker = (ImageView)nextChild.findViewById(R.id.img_marker_icon);
			if(imgMarker != null)
				imgMarker.setTag("mark" + i);
		}
	}
	
	
	private void layoutControls()
	{
		m_txtTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(50));
		LayoutUtils.setMargin(m_txtTitle, 0, 90, 0, 0, true);
		
		View layFillContent = findViewById(R.id.lay_fill_content);
		LayoutUtils.setMargin(layFillContent, 40, 97, 40, 0, true);
		
		m_txtEnvironmentalTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(50));		
		m_txtEnvironmentalSubTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(50));
		
		for(int i=0; i<((ViewGroup)layFillContent).getChildCount(); ++i) 
		{
			View nextChild = ((ViewGroup)layFillContent).getChildAt(i);
			if( nextChild == null )
				continue;
			
			LayoutUtils.setSize(nextChild, LayoutParams.MATCH_PARENT, 1360/9, true);
			if( i == 1 )
				LayoutUtils.setMargin(nextChild, 0, 3, 0, 0, true);
			
			if( nextChild instanceof RelativeLayout == false )
				continue;
			
			ImageView imgMarker = (ImageView)nextChild.findViewById(R.id.img_marker_icon);
			EditText editContent = (EditText)nextChild.findViewById(R.id.txt_file_description);
			
			LayoutUtils.setSize(imgMarker, 54, 54, true);
			LayoutUtils.setMargin(imgMarker, 22, 0, 40, 0, true);
			
			LayoutUtils.setSize(editContent, LayoutParams.MATCH_PARENT, 130, true);
			editContent.setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(34));
		}
	}
	
	private void initData()
	{
		TextPaint paint = new TextPaint(Paint.UNDERLINE_TEXT_FLAG);
		paint.setAntiAlias(true);
		m_txtTitle.setPaintFlags( paint.getFlags());
		m_txtTitle.setText(getString(R.string.home_environmental_scan).replace("\n", " "));
		
		m_txtEnvironmentalTitle.setBackgroundColor(Color.rgb(3, 90, 99));
		m_txtEnvironmentalTitle.setTextColor(Color.WHITE);

		m_txtEnvironmentalSubTitle.setBackgroundColor(Color.rgb(31, 136, 147));
		m_txtEnvironmentalSubTitle.setTextColor(Color.WHITE);
		
		View layFillContent = findViewById(R.id.lay_fill_content);
		
		for(int i=0; i<((ViewGroup)layFillContent).getChildCount(); ++i) 
		{
			View nextChild = ((ViewGroup)layFillContent).getChildAt(i);
			
			if( nextChild == null || nextChild instanceof RelativeLayout == false )
				continue;
			
			ImageView imgMarker = (ImageView)nextChild.findViewById(R.id.img_marker_icon);
			EditText editContent = (EditText)nextChild.findViewById(R.id.txt_file_description);
			
			Picasso.with(this).load(R.drawable.icon_risk_level)
			.placeholder(R.drawable.icon_risk_level)//.centerCrop()
			.into(imgMarker);
			
			editContent.setHint(R.string.environmental_fill_hint);
			editContent.setText("");
		}
		
		((BasePresenter)presenter).initData();
	}
		
	private void initUIEvents()
	{
		View layFillContent = findViewById(R.id.lay_fill_content);
		
		for(int i=0; i<((ViewGroup)layFillContent).getChildCount(); ++i) 
		{
			View nextChild = ((ViewGroup)layFillContent).getChildAt(i);
			
			if( nextChild == null || nextChild instanceof RelativeLayout == false )
				continue;
			
			ImageView imgMarker = (ImageView)nextChild.findViewById(R.id.img_marker_icon);
			
			ResourceUtils.addClickEffect(imgMarker);		
		}
	}
	
	@Override
	protected void gotoBackPage()
	{
		((NavigateBarPresenter)presenter).gotoPrevPage();
	}
	
	@Override
	protected void gotoNextPage()
	{
		((NavigateBarPresenter)presenter).gotoNextPage();
	}
	
	
	@Override
	protected void gotoMenuPage()
	{
		if( presenter instanceof NavigateBarPresenter )
			((NavigateBarPresenter)presenter).gotoMenuPage();
	}	

	// Setting Data
	@Override
	public void showFillContent(String title, String subtitle, ArrayList<String> data) {
		m_txtEnvironmentalTitle.setText(title);
		m_txtEnvironmentalSubTitle.setText(subtitle);
		
		View layFillContent = findViewById(R.id.lay_fill_content);
		
		int count = 0;
			
		for(int i=0; i< ((ViewGroup)layFillContent).getChildCount(); ++i) 
		{
			View nextChild = ((ViewGroup)layFillContent).getChildAt(i);
			
			if( nextChild == null || nextChild instanceof RelativeLayout == false )
				continue;
			
			EditText editContent = (EditText)nextChild.findViewById(R.id.txt_file_description);
			if(count > data.size() - 1)
				return;
			
			editContent.setText(data.get(count));
			count++;
		}
	}

	// Getting Data
	@Override
	public ArrayList<String> getEnvironmentalScanData() {
		ArrayList<String> content = new ArrayList<String>();
		
		View layFillContent = findViewById(R.id.lay_fill_content);
		
		for(int i=0; i<((ViewGroup)layFillContent).getChildCount(); ++i) 
		{
			View nextChild = ((ViewGroup)layFillContent).getChildAt(i);
			
			if( nextChild == null || nextChild instanceof RelativeLayout == false )
				continue;
			
			EditText editContent = (EditText)nextChild.findViewById(R.id.txt_file_description);
			content.add("" + editContent.getText().toString());
		}
		
		return content;
	}


	@Override
	public void gotoEstimatingRiskPage() {
		Bundle bundle = new Bundle();
		ActivityManager.changeActivity(this, EstimatingRiskActivity.class, bundle, false, null );		
	}


	@Override
	public void gotoFileListPage() {
		onFinishActivity();				
	}


	@Override
	public void changeSelfPage() {
		Bundle bundle = new Bundle();
		ActivityManager.changeActivity(this, getClass(), bundle, true, null );		
	}


	

}
