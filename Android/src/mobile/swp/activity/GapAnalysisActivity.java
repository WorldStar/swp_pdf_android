package mobile.swp.activity;

import java.util.ArrayList;
import java.util.List;

import mobile.swp.R;
import mobile.swp.adapters.MeasureTableAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import common.design.layout.LayoutUtils;
import common.design.layout.ScreenAdapter;

public class GapAnalysisActivity extends GapAnalysisBaseActivity {	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		m_nColCount = 4;
		super.onCreate(savedInstanceState, R.layout.layout_gap_0);
	}	
	
	@Override
	protected void layoutFragments(ViewGroup fragment, int num)
	{
		for(int j = 0; j < fragment.getChildCount(); j++ )
		{
			View view = fragment.getChildAt(j);
			if( view == null )
				continue;
			
			if( num == 0 )
			{
				if( view instanceof TextView )
				{
					LayoutUtils.setMargin(view, 57, 0, 0, 0, true);
					LayoutUtils.setSize(view, 144, LayoutParams.WRAP_CONTENT, true);
					((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(37));
				}
				
				if( view instanceof EditText )
				{
					LayoutUtils.setMargin(view, 0, 0, 48, 0, true);
					LayoutUtils.setSize(view, LayoutParams.FILL_PARENT, 105, true);				
					((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(42));
				}					
			}
			if( 1 < num && num < 6 )
			{
				if( view instanceof TextView && j == 0 )
				{
					LayoutUtils.setMargin(view, 40, 0, 0, 0, true);
					LayoutUtils.setSize(view, 304, LayoutParams.WRAP_CONTENT, true);
					((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(37));
				}
				
				if( view instanceof EditText )
				{
					LayoutUtils.setMargin(view, 0, 0, 18, 0, true);
					LayoutUtils.setSize(view, LayoutParams.FILL_PARENT, 105, true);				
					((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(42));
				}	
			}
			
			if( view instanceof EditText )
				LayoutUtils.setPadding(view, m_EditPadding, 0, m_EditPadding, 0, true);
		}
		
	}

	@Override
	protected List<JSONObject> getItemData(JSONObject data)
	{		
		List<JSONObject> tableData = new ArrayList<JSONObject>();

		for(int i = 2; i < 6; i++ )
		{
			String content = data.optString("value_" + i + "_1", "");
			JSONObject fileinfo = new JSONObject();
			try {
				fileinfo.put(MeasureTableAdapter.ITEM_NAME, content);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			tableData.add(fileinfo);
		}
		
		return tableData;
	}

	@Override
	public JSONObject getCurrentStateData() {
		JSONObject data = super.getCurrentStateData();
		
		List<JSONObject>  list = m_adapterMeasureTable.getData();
		JSONArray array = new JSONArray();
		for(int i = 0; i < list.size(); i++ )
			array.put(list.get(i).optString(MeasureTableAdapter.ITEM_NAME, ""));
		
		try {
			data.put("grid_data", array);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return data;
	}
	

}
