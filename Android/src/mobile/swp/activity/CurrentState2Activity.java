package mobile.swp.activity;

import mobile.swp.R;
import mobile.swp.component.combobox.ComboBoxView;
import mobile.swp.constant.Const;
import mobile.swp.mvp.CurrentStateView;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import common.design.layout.LayoutUtils;
import common.design.layout.ScreenAdapter;
import common.manager.activity.ActivityManager;

public class CurrentState2Activity extends CurrentStateBaseActivity implements CurrentStateView {	
	TextView			m_txtTitle 		= null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.layout_current_state_2);
	}	
	
	protected void layoutFragments(ViewGroup fragment, int num)
	{
		for(int j = 0; j < fragment.getChildCount(); j++ )
		{
			View view = fragment.getChildAt(j);
			if( view == null )
				continue;
			
			if( view instanceof TextView )
			{
				switch( num )
				{
				case 0:
					LayoutUtils.setSize(view, 232, LayoutParams.WRAP_CONTENT, true);
					break;				
				}
				
				LayoutUtils.setMargin(view, 48, 0, 0, 0, true);
				((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(37));
			}
			
			if( view instanceof EditText )
			{
				if( 1 < num && num < 7 )
				{
					LayoutUtils.setMargin(view, 48, 0, 48, 0, true);
					LayoutUtils.setSize(view, LayoutParams.FILL_PARENT, 115, true);
					((EditText) view).setHint((num - 1) + ". Please select Alige... .");
				}
				
			
				if( num == 7 )
				{
					LayoutUtils.setMargin(view, 48, 0, 48, 0, true);
					LayoutUtils.setSize(view, LayoutParams.FILL_PARENT, 384, true);
				}
				LayoutUtils.setPadding(view, m_EditPadding, 0, m_EditPadding, 0, true);
				((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(42));
			}			
			
			if( view instanceof ComboBoxView )
			{
				LayoutUtils.setMargin(view, 64, 0, 48, 0, true);
				LayoutUtils.setSize(view, LayoutParams.FILL_PARENT, 115, true);
				((ComboBoxView)view).setTextSize(ScreenAdapter.computeHeight(42));
				view.setOnClickListener(onClickButtonListener);		
				view.setTag("combo_1");
//				ResourceUtils.addClickEffect(view);
			}
		}
		
	}
	
	private View.OnClickListener onClickButtonListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			if( arg0 instanceof ComboBoxView )
				presenter.showListDialog((String)arg0.getTag(), ((ComboBoxView)arg0).getText());			
		}
	};


	@Override
	public void gotoPage(int pageNum) {
		Bundle bundle = new Bundle();
		bundle.putInt(Const.PAGE_NAME, pageNum);		
		ActivityManager.changeActivity(this, CurrentState3Activity.class, bundle, false, null );	
	}
	
}
