package mobile.swp.activity;

import mobile.swp.R;
import mobile.swp.constant.Const;
import mobile.swp.mvp.CurrentStateView;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import common.design.layout.LayoutUtils;
import common.design.layout.ScreenAdapter;
import common.manager.activity.ActivityManager;

public class CurrentState3Activity extends CurrentStateBaseActivity implements CurrentStateView {	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.layout_current_state_3);
	}	
	
	
	protected void layoutFragments(ViewGroup fragment, int num)
	{
		for(int j = 0; j < fragment.getChildCount(); j++ )
		{
			View view = fragment.getChildAt(j);
			if( view == null )
				continue;
			
			if( view instanceof TextView )
			{
				LayoutUtils.setMargin(view, 48, 0, 0, 0, true);
				((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(37));
			}
			
			if( view instanceof EditText )
			{
				LayoutUtils.setMargin(view, 48, 0, 48, 0, true);
				LayoutUtils.setSize(view, LayoutParams.FILL_PARENT, 294, true);
				
				((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(42));
			}			
		}
		
	}
	
	@Override
	public void gotoPage(int pageNum) {
		Bundle bundle = new Bundle();
		bundle.putInt(Const.PAGE_NAME, pageNum);		
		ActivityManager.changeActivity(this, CurrentState4Activity.class, bundle, false, null );			
	}	
	
}
