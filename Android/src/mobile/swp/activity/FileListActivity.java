package mobile.swp.activity;

import java.util.List;

import mobile.swp.R;
import mobile.swp.adapters.FileListAdapter;
import mobile.swp.constant.Const;
import mobile.swp.mvp.BasePresenter;
import mobile.swp.mvp.FileListPresenter;
import mobile.swp.mvp.FileListPresenterFactory;
import mobile.swp.mvp.FileListView;
import mobile.swp.mvp.NavigateBarPresenter;

import org.json.JSONObject;

import android.graphics.Paint;
import android.os.Bundle;
import android.text.TextPaint;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import common.design.layout.LayoutUtils;
import common.design.layout.ScreenAdapter;
import common.list.adapter.ItemCallBack;
import common.list.adapter.ItemResult;
import common.manager.activity.ActivityManager;

public class FileListActivity extends NavigateBarActivity implements FileListView {
	TextView				m_txtTitle = null;
	ListView				m_listFiles = null;
	TextView				m_emptyView = null;
	FileListAdapter 		m_adapterFileList = null;
	
	FileListPresenter presenter = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.layout_filelist);
		
		createPresenter();	
		
		addNavigateBar();
		findViews();		
		layoutControls();
		initData();
//		initUIEvents();
	}
	
	private void createPresenter()
	{
		int page = 0;
		Bundle bundle = getIntent().getExtras();
		if( bundle != null )
		{
			page = bundle.getInt(Const.PAGE_NAME, 0);
		}
		presenter = FileListPresenterFactory.getInstance().createFileListPresenter(this, page);
		Log.e("activity_stack", "create_filelist_" + page);
		
	}
	
	private void findViews()
	{
		m_txtTitle = (TextView) findViewById(R.id.txt_title);
		m_listFiles = (ListView) findViewById(R.id.list_File_List);
		m_emptyView = (TextView) findViewById(R.id.txt_empty_view);
	}
	
	private void layoutControls()
	{
		int miniHeight = ScreenAdapter.miliToPx(100);
		if( ScreenAdapter.computeHeight(164) > miniHeight )
		{
			LayoutUtils.setSize(m_btnNext, 200, 48, true);
		}
		else
		{
			float ratio = 0.6f;
			int buttonSizeX = (int)(ScreenAdapter.miliToPx(200) * ratio);
			int buttonSizeY = (int)(ScreenAdapter.miliToPx(48) * ratio);
			LayoutUtils.setSize(m_btnNext, buttonSizeX, buttonSizeY, false);
			LayoutUtils.setMargin(m_btnNext, 0, 0, (int)(ScreenAdapter.miliToPx(38) * ratio), 0, false);
		}
		
		m_btnNext.setBackgroundResource(R.drawable.file_create_btn_bg);

		m_txtTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(50));
		LayoutUtils.setMargin(m_txtTitle, 0, 45, 0, 0, true);
		
		LayoutUtils.setMargin(m_listFiles, 0, 50, 0, 0, true);
		
		m_emptyView.setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(50));
		LayoutUtils.setMargin(m_emptyView, 0, 50, 0, 0, true);
		LayoutUtils.setPadding(m_emptyView, 0, 0, 0, 500, true);
	}
	
	private void initData()
	{
		TextPaint paint = new TextPaint(Paint.UNDERLINE_TEXT_FLAG);
		paint.setAntiAlias(true);
		m_txtTitle.setPaintFlags( paint.getFlags());
		m_txtTitle.setText(getString(R.string.home_business_strategy).replace("\n", " "));
		
	}
	
	@Override
	public void showPageTitle(String title)
	{
		m_txtTitle.setText(title);
	}
	
	@Override
	public void showFileListData(List<JSONObject> filelist)
	{
		if( filelist.size() < 1 )
		{
			m_listFiles.setVisibility(View.GONE);
			m_emptyView.setVisibility(View.VISIBLE);
		}
		else
		{
			m_listFiles.setVisibility(View.VISIBLE);
			m_emptyView.setVisibility(View.GONE);

			m_adapterFileList = new FileListAdapter(this, filelist, R.layout.fragment_listitem_filelist, new ItemCallBack() {
				
				@Override
				public void doClick(ItemResult result) {
					presenter.openPDFFile(result.itemData);
				}
			});
			
			m_listFiles.setAdapter(m_adapterFileList);
		}
	
	}
	
	protected void gotoDirectOtherPage(JSONObject itemData) {
		
	}

	@Override
	protected void gotoMenuPage()
	{
		if( presenter instanceof NavigateBarPresenter )
			((NavigateBarPresenter)presenter).gotoMenuPage();
	}
	
	@Override
	protected void gotoNextPage()
	{
		if( presenter instanceof NavigateBarPresenter )
			((NavigateBarPresenter)presenter).gotoNextPage();
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
		
		((BasePresenter)presenter).initData();
	}

	@Override
	public void gotoBusinessResponsePage()
	{
		Bundle bundle = new Bundle();
		
		ActivityManager.changeActivity(this, BusinessResponseActivity.class, bundle, false, null );
	}
	
	@Override
	public void gotoEnvironmentalScanPage() {
		Bundle bundle = new Bundle();
		ActivityManager.changeActivity(this, EnvironmentalScanActivity.class, bundle, false, null );

	}

	@Override
	public void gotoCurrentStatePage() {
		Bundle bundle = new Bundle();
		bundle.putInt(Const.PAGE_NAME, 6);
		ActivityManager.changeActivity(this, CurrentState1Activity.class, bundle, false, null );		
	}

	@Override
	public void gotoFuturingPage() {
		Bundle bundle = new Bundle();
		bundle.putInt(Const.PAGE_NAME, 1);
		ActivityManager.changeActivity(this, Future1Activity.class, bundle, false, null );	
	}

	@Override
	public void gotoGapAnalysisPage() {
		Bundle bundle = new Bundle();
		bundle.putInt(Const.PAGE_NAME, 12);
		ActivityManager.changeActivity(this, GapAnalysisActivity.class, bundle, false, null );	
		
	}

	@Override
	public void gotoActionPlanningPage() {
		Bundle bundle = new Bundle();
		bundle.putInt(Const.PAGE_NAME, 13);
		ActivityManager.changeActivity(this, ActionPlanningActivity.class, bundle, false, null );	
	}

	@Override
	public void gotoMonitorReportPage() {
		Bundle bundle = new Bundle();
		bundle.putInt(Const.PAGE_NAME, 14);
		ActivityManager.changeActivity(this, MonitorReportActivity.class, bundle, false, null );	
	}

	@Override
	public void gotoPDFViewPage(String path) {
		Bundle bundle = new Bundle();
		bundle.putString(LibPDFViewActivity.PDF_FILE_PATH, path);
		ActivityManager.changeActivity(this, LibPDFViewActivity.class, bundle, false, null );	
	}	
	
	public void refreshFileList(int menuNum, boolean isTopFlag)
	{
		presenter = FileListPresenterFactory.getInstance().createFileListPresenter(this, menuNum - 1);
		Log.e("activity_stack", "refresh_filelist_" + menuNum);
		if( isTopFlag == true )
			((BasePresenter)presenter).initData();
	}
}
