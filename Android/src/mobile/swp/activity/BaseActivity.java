package mobile.swp.activity;

import mobile.swp.constant.Const;
import mobile.swp.mvp.BaseView;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;

import common.manager.activity.ActivityManager;

public class BaseActivity extends Activity implements BaseView {
	protected  ProgressDialog progressDialog = null;

	protected Handler mMessageHandle = new Handler(){
		@Override
		public void handleMessage(Message msg) {
			processMessage(msg);
		}		
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		ActivityManager.getInstance().pushActivity(BaseActivity.this);
		setBackgroundColor(Const.APP_BACK_COLOR);
		initProgress();
	}
	
	protected void setBackgroundColor( int color )
	{
		View view = getWindow().getDecorView();
	    view.setBackgroundColor(color);

	}
	
	public void sendMessage(Message msg) 
	{
		mMessageHandle.sendMessage(msg);
	}
	
	protected void sendMessageDelayed(Message msg, long delayMillis ) 
	{
		mMessageHandle.sendMessageDelayed(msg, delayMillis);
	}
		
	protected void onFinishActivity()
	{
		ActivityManager.getInstance().popActivity();	
	}
	@Override 
	public void onBackPressed( ) {	
		onFinishActivity();
	}
	

	protected void processMessage(Message msg)
	{		
		switch( msg.what )
		{
		
		}
	}

	@Override
	public void initProgress() {
       progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please wait...");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
	}

	@Override
	public void showProgress(String title, String message) {
		progressDialog.setTitle(title);
		progressDialog.setMessage(message);
		if(progressDialog.isShowing()){
			return;
		}
		progressDialog.show();
		
	}
	
	@Override
	public void changeProgress(String title, String message) {
		if( progressDialog.isShowing() == false )
			return;
		
		progressDialog.setTitle(title);
		progressDialog.setMessage(message);
		
	}
	@Override
	public void hideProgress() {
		progressDialog.dismiss();	
	}


}
