package mobile.swp.activity;

import mobile.swp.R;
import mobile.swp.mvp.BasePresenter;
import mobile.swp.mvp.EstimatingRiskPresenter;
import mobile.swp.mvp.EstimatingRiskPresenterImpl;
import mobile.swp.mvp.EstimatingRiskView;
import mobile.swp.mvp.NavigateBarPresenter;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.view.ViewPager.LayoutParams;
import android.text.TextPaint;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import common.design.layout.LayoutUtils;
import common.design.layout.ScreenAdapter;
import common.design.utils.ResourceUtils;
import common.manager.activity.ActivityManager;

public class EstimatingRiskActivity extends NavigateBarActivity implements EstimatingRiskView {	
	TextView		m_txtTitle 		= null;
	TextView		m_txtQuestion 		= null;
	EstimatingRiskPresenter	presenter = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.layout_estimating_risk);
		
		createPresenter();
		addNavigateBar();
		findViews();		
		layoutControls();
		initData();
		initUIEvents();
	}
	
	private void createPresenter()
	{
		presenter = new EstimatingRiskPresenterImpl(this);
	}
	private void findViews()
	{
		m_txtTitle = (TextView) findViewById(R.id.txt_title);
		m_txtQuestion = (TextView) findViewById(R.id.txt_question);
		
		View layRiskSelectPanel = findViewById(R.id.lay_risk_select_panel);		
		for(int i=0; i<((ViewGroup)layRiskSelectPanel).getChildCount(); ++i) 
		{
			View nextChild = ((ViewGroup)layRiskSelectPanel).getChildAt(i);
			if( nextChild == null || nextChild instanceof LinearLayout == false )
				continue;
			
			for(int j = 0; j < ((ViewGroup)nextChild).getChildCount(); ++j )
			{
				View view = ((ViewGroup)nextChild).getChildAt(j);
				if( view == null )
					continue;
				
				view.setTag("panel_" + i + j);
				
			}		
		}
	}
	
	private void layoutControls()
	{
		m_txtTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(50));
		LayoutUtils.setMargin(m_txtTitle, 0, 90, 0, 0, true);
		
		m_txtQuestion.setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(50));
		LayoutUtils.setSize(m_txtQuestion, LayoutParams.MATCH_PARENT, 250, true);
		LayoutUtils.setMargin(m_txtQuestion, 50, 80, 50, 0, true);

		
		View layRiskSelectPanel = findViewById(R.id.lay_risk_select_panel);
		LayoutUtils.setMargin(layRiskSelectPanel, 0, 25, 0, 0, true);
		LayoutUtils.setPadding(layRiskSelectPanel, 13, 20, 20, 15, true);
		
		int [][] panelBack = {
				{ R.drawable.panel_11, R.drawable.panel_12, R.drawable.panel_13 },
				{ R.drawable.panel_21, R.drawable.panel_21, R.drawable.panel_12 },
				{ R.drawable.panel_21, R.drawable.panel_21, R.drawable.panel_11 }
		};
				
		for(int i=0; i<((ViewGroup)layRiskSelectPanel).getChildCount(); ++i) 
		{
			View nextChild = ((ViewGroup)layRiskSelectPanel).getChildAt(i);
			if( nextChild == null || nextChild instanceof LinearLayout == false )
				continue;
			
			for(int j = 0; j < ((ViewGroup)nextChild).getChildCount(); ++j )
			{
				View view = ((ViewGroup)nextChild).getChildAt(j);
				if( view == null  )
					continue;
				
				LayoutUtils.setSize(view, 284, 252, true);	
				LayoutUtils.setPadding(view, 20, 20, 20, 20, true);
				view.setBackgroundResource(panelBack[i][j]);
				((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(40));
				
			}		
		}
		
		View risk_level_name_bg_vertical = findViewById(R.id.img_risk_level_name_vertical);
		View risk_level_name_bg_horizontal = findViewById(R.id.img_risk_level_name_horizontal);
		
		LayoutUtils.setSize(risk_level_name_bg_vertical, 46, 746, true);
		LayoutUtils.setMargin(risk_level_name_bg_vertical, 55, 690, 0, 0, true);
		
		LayoutUtils.setSize(risk_level_name_bg_horizontal, 840, 37, true);
		LayoutUtils.setMargin(risk_level_name_bg_horizontal, 115, 1460, 0, 0, true);
	}
	
	private void initData()
	{
		TextPaint paint = new TextPaint(Paint.UNDERLINE_TEXT_FLAG);
		paint.setAntiAlias(true);
		m_txtTitle.setPaintFlags( paint.getFlags());
		m_txtTitle.setText("Estimating Risk");
				
		m_btnNext.setBackgroundResource(R.drawable.save_arrow);
		m_txtQuestion.setText("Where on the grid does each\r\nenvironmental scan item ﬁt?");

		((BasePresenter)presenter).initData();
	}
	
	private void initUIEvents()
	{
		View layRiskSelectPanel = findViewById(R.id.lay_risk_select_panel);
		LayoutUtils.setMargin(layRiskSelectPanel, 0, 25, 0, 0, true);
		LayoutUtils.setPadding(layRiskSelectPanel, 12, 12, 12, 12, true);
				
		for(int i=0; i<((ViewGroup)layRiskSelectPanel).getChildCount(); ++i) 
		{
			View nextChild = ((ViewGroup)layRiskSelectPanel).getChildAt(i);
			if( nextChild == null || nextChild instanceof LinearLayout == false )
				continue;
			
			for(int j = 0; j < ((ViewGroup)nextChild).getChildCount(); ++j )
			{
				View view = ((ViewGroup)nextChild).getChildAt(j);
				if( view == null  )
					continue;
				
				ResourceUtils.addClickEffect(view);		
				view.setOnClickListener(onClickButtonListener);		
			}		
		}
		
	
	}
	
	private View.OnClickListener onClickButtonListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			presenter.showSelectChoiceDialog((String)arg0.getTag());
		}
	};


	@Override
	public void showSelectedChoiceInfo(String tag, String content) {
		View layRiskSelectPanel = findViewById(R.id.lay_risk_select_panel);		
		for(int i=0; i<((ViewGroup)layRiskSelectPanel).getChildCount(); ++i) 
		{
			View nextChild = ((ViewGroup)layRiskSelectPanel).getChildAt(i);
			if( nextChild == null || nextChild instanceof LinearLayout == false )
				continue;
			
			for(int j = 0; j < ((ViewGroup)nextChild).getChildCount(); ++j )
			{
				View view = ((ViewGroup)nextChild).getChildAt(j);
				if( view == null )
					continue;
				if( tag.equals("panel_" + i + j) )
				{
					((TextView)view).setText(content);
				}				
			}		
		}
		
	}
	
	@Override
	public void showSelectedChoiceInfo(String[][] content) {
		View layRiskSelectPanel = findViewById(R.id.lay_risk_select_panel);		
		for(int i=0; i<((ViewGroup)layRiskSelectPanel).getChildCount(); ++i) 
		{
			View nextChild = ((ViewGroup)layRiskSelectPanel).getChildAt(i);
			if( nextChild == null || nextChild instanceof LinearLayout == false )
				continue;
			
			for(int j = 0; j < ((ViewGroup)nextChild).getChildCount(); ++j )
			{
				View view = ((ViewGroup)nextChild).getChildAt(j);
				if( view == null )
					continue;
				((TextView)view).setText(content[i][j]);							
			}		
		}
	}
	
	@Override
	protected void gotoNextPage()
	{
		((NavigateBarPresenter)presenter).gotoNextPage();
	}

	@Override
	protected void gotoMenuPage()
	{
		if( presenter instanceof NavigateBarPresenter )
			((NavigateBarPresenter)presenter).gotoMenuPage();
	}	
	
	@Override
	public void gotoFileListPage() {
		ActivityManager.getInstance().popAllActivityExceptOne(FileListActivity.class);		
	}


}
