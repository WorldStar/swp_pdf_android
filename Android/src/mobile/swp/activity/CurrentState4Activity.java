package mobile.swp.activity;

import kankan.wheel.widget.DateWheel;
import mobile.swp.R;
import mobile.swp.component.combobox.ComboBoxView;
import mobile.swp.constant.Const;
import mobile.swp.mvp.CurrentStateView;
import android.os.Bundle;
import android.support.v4.view.ViewPager.LayoutParams;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import common.design.layout.LayoutUtils;
import common.design.layout.ScreenAdapter;
import common.manager.activity.ActivityManager;

public class CurrentState4Activity extends CurrentStateBaseActivity implements CurrentStateView {	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, R.layout.layout_current_state_4);
	}
			
	protected void layoutFragments(ViewGroup fragment, int num)
	{
		for(int j = 0; j < fragment.getChildCount(); j++ )
		{
			View view = fragment.getChildAt(j);
			if( view == null )
				continue;
			
			if( view instanceof TextView )
			{
				switch( num )
				{
				case 0:
				case 2:
					LayoutUtils.setSize(view, 232, LayoutParams.WRAP_CONTENT, true);
					break;				
				}
				
				LayoutUtils.setMargin(view, 48, 0, 0, 0, true);
				((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(37));
			}
			
			if( view instanceof EditText )
			{
				if( num == 0 )
				{
					LayoutUtils.setMargin(view, 64, 0, 48, 0, true);
					LayoutUtils.setSize(view, LayoutParams.FILL_PARENT, 115, true);
							
				}
				if( num == 1 )
				{
					LayoutUtils.setMargin(view, 48, 0, 48, 0, true);
					LayoutUtils.setSize(view, LayoutParams.FILL_PARENT, 115, true);
				}
				if( num == 3 )
				{
					LayoutUtils.setMargin(view, 15, 0, 0, 0, true);
					LayoutUtils.setSize(view, 100, 72, true);
				}
				
				if( num == 4 )
				{
					LayoutUtils.setMargin(view, 48, 0, 48, 0, true);
					LayoutUtils.setSize(view, LayoutParams.FILL_PARENT, 373, true);
				}
				((TextView)view).setTextSize(TypedValue.COMPLEX_UNIT_PX, ScreenAdapter.computeHeight(42));
				LayoutUtils.setPadding(view, 15, 0, 15, 0, true);
			}			
			
			if( view instanceof DateWheel )
			{
				LayoutUtils.setSize(view, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT, true);
				LayoutUtils.setMargin(view, 64, 0, 48, 0, true);
			}
			
			if( view instanceof ComboBoxView )
			{
				LayoutUtils.setMargin(view, 48, 0, 48, 0, true);
				LayoutUtils.setSize(view, LayoutParams.FILL_PARENT, 115, true);
				((ComboBoxView)view).setTextSize(ScreenAdapter.computeHeight(37));
				view.setOnClickListener(onClickButtonListener);		
				view.setTag("combo_1");
			}
		}
		
	}
	
	private View.OnClickListener onClickButtonListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			if( arg0 instanceof ComboBoxView )
				presenter.showListDialog((String)arg0.getTag(), ((ComboBoxView)arg0).getText());			
		}
	};

	@Override
	public void gotoPage(int pageNum) {
		Bundle bundle = new Bundle();
		bundle.putInt(Const.PAGE_NAME, pageNum);		
		ActivityManager.changeActivity(this, CurrentState5Activity.class, bundle, false, null );	
	}
}
