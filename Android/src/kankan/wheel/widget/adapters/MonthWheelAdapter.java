/*
 *  Copyright 2011 Yuri Kanivets
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package kankan.wheel.widget.adapters;

import android.content.Context;

/**
 * Numeric Wheel adapter.
 */
public class MonthWheelAdapter extends AbstractWheelTextAdapter {
	// Values
	private int minValue;
	private int maxValue;

	String [] month_name = {
		"January",
		"Feburary",
		"March",
		"April",
		"May",
		"June",
		"July",
		"Auguest",
		"September",
		"Octorber",
		"November",
		"December"
	};
	/**
	 * Constructor
	 * 
	 * @param context
	 *            the current context
	 */
	public MonthWheelAdapter(Context context) {
		this(context, 0, 11);
	}

	/**
	 * Constructor
	 * 
	 * @param context
	 *            the current context
	 * @param minValue
	 *            the wheel min value
	 * @param maxValue
	 *            the wheel max value
	 */
	public MonthWheelAdapter(Context context, int minValue, int maxValue) {
		this(context, minValue, maxValue, null);
	}

	/**
	 * Constructor
	 * 
	 * @param context
	 *            the current context
	 * @param minValue
	 *            the wheel min value
	 * @param maxValue
	 *            the wheel max value
	 * @param format
	 *            the format string
	 */
	public MonthWheelAdapter(Context context, int minValue, int maxValue,
			String format) {
		super(context);

		this.minValue = minValue;
		this.maxValue = maxValue;
	}

	@Override
    public CharSequence getItemText(int index) {
        if (index >= 0 && index < getItemsCount()) {
            int value = minValue + index;
            return month_name[value];            
        }
        return null;
    }

	@Override
	public int getItemsCount() {
		return maxValue - minValue + 1;
	}
}
