package kankan.wheel.widget;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import kankan.wheel.widget.adapters.MonthWheelAdapter;
import kankan.wheel.widget.adapters.NumericWheelAdapter;
import mobile.swp.R;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

public class DateWheel extends LinearLayout {
	private Calendar calendar = Calendar.getInstance(Locale.ENGLISH);
	
	private OnTimeChangedListener timeChangedListener = null;
	
	private Date	m_selDate = new Date();		
	
	WheelView m_Days = null;
	
	public DateWheel(Context context) {
		this(context, null);
	}

	public DateWheel(Context context, AttributeSet attrs) {
		super(context, attrs);

		setOrientation(VERTICAL);
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.date_wheel, this, true);
		
		int maxDates = getDateNumber(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH));
		final WheelView days = (WheelView) findViewById(R.id.day);
		final NumericWheelAdapter daysAdapter = new NumericWheelAdapter(context, 1, maxDates, "%02d");
		daysAdapter.setItemResource(R.layout.wheel_text_item);
		daysAdapter.setItemTextResource(R.id.text);
		days.setViewAdapter(daysAdapter);
		days.setCyclic(true);
		
		m_Days = days;
		
		final WheelView years = (WheelView) findViewById(R.id.year);
		NumericWheelAdapter yearsAdapter = new NumericWheelAdapter(context, 0, 3000, "%04d");
		yearsAdapter.setItemResource(R.layout.wheel_text_item);
		yearsAdapter.setItemTextResource(R.id.text);
		years.setViewAdapter(yearsAdapter);
		years.setCyclic(true);

		final WheelView month = (WheelView) findViewById(R.id.month);
		
		MonthWheelAdapter monthAdapter = new MonthWheelAdapter(context);
		monthAdapter.setItemResource(R.layout.wheel_text_item);
		monthAdapter.setItemTextResource(R.id.text);
		month.setViewAdapter(monthAdapter);
		month.setCyclic(true);
		
		days.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				m_selDate.setDate(newValue + 1);
			}
		});
	
		
		month.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				m_selDate.setMonth(newValue);
				limitDates(wheel, daysAdapter);
			}
		});		
		
		years.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				m_selDate.setYear(newValue);		
				limitDates(wheel, daysAdapter);
			}
		});
		
		days.setCurrentItem(calendar.get(Calendar.DATE) - 1);
		years.setCurrentItem(calendar.get(Calendar.YEAR));
		month.setCurrentItem(calendar.get(Calendar.MONTH));
		
	}

	private void limitDates(WheelView wheel, NumericWheelAdapter adpater)
	{
		int maxDates = getDateNumber(m_selDate.getYear(), m_selDate.getMonth());
		adpater.setRange(1, maxDates);
		if( m_selDate.getDate() > maxDates )
		{
			m_selDate.setDate(maxDates);
			m_Days.setCurrentItem(maxDates);
			m_Days.invalidateWheel(true);
		}
	}
	public static int getDateNumber(int year, int month){
		// Create a calendar object and set year and month
		Calendar mycal = new GregorianCalendar(year, month, 1);

		// Get the number of days in that month
		int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH); // 28
		
		return daysInMonth;
	}
	
	public void setOnTimeChangedListener(OnTimeChangedListener timeChangedListener) {
		this.timeChangedListener = timeChangedListener;
	}

	public interface OnTimeChangedListener {
		void onTimeChanged(long time);
	}
	
	public Date getDate()
	{
//		int year = m_selDate.getYear();
//		return new Date(m_selDate.getYear(), m_selDate.getMonth(), m_selDate.getDate());
		return m_selDate;
	}

	public String getDateStringForEnglish()
	{
		SimpleDateFormat format = new SimpleDateFormat("MMMM dd, yyyy",  Locale.ENGLISH);
		String content = format.format(m_selDate);
		return content.substring(0, content.length() - 4) + m_selDate.getYear();
	}
}
